/****************************************************************************
Copyright (C) Cambridge Silicon Radio Ltd. 2013

FILE NAME
    csr_i2s_audio_plugin.c
DESCRIPTION
    plugin implentation which routes audio output via an external i2s device
NOTES
*/

#include <stdio.h>
#include <audio.h>
#include <codec.h>
#include <stdlib.h>
#include <panic.h>
#include <print.h>
#include <file.h>
#include <stream.h> 
#include <kalimba.h>
#include <kalimba_standard_messages.h>
#include <message.h>
#include <Transform.h>
#include <string.h>
#include <i2c.h>
#include <pio.h>
#include <pio_common.h>

#include "csr_i2s_audio_plugin.h"
#include "alldsp.h"
#include "allcomm.h"

#define xDEBUG_I2S
#ifdef DEBUG_I2S
#  define I2S_PRINT(x) printf x
#else
#  define I2S_PRINT(x)
#endif


#define I2S_MASTER_MODE TRUE
#define MCLK_SCALING_FACTOR 64


/* I2S configuration data */
I2SConfiguration * I2S_config;


void CsrI2SInitialisePlugin(I2SConfiguration * config)
{
    I2S_config = config;
}


Sink CsrI2SAudioOutputConnect(uint32 rate, bool stereo, Source left_port, Source right_port)
{
    return AllConnect(rate, stereo, left_port, right_port);
}


void CsrI2SAudioOutputDisconnect(bool stereo)
{
    AllDisconnect();
}


void CsrI2SAudioOutputSetVolume(bool stereo, int16 left_vol, int16 right_vol, bool volume_in_dB)
{
/*
    if (volume_in_dB)
    {
        int32 volume = 0;
        I2S_PRINT(("I2S: Volume dB[%d] ", left_vol));
        volume = (int32)left_vol * CODEC_STEPS;
        left_vol = CODEC_STEPS - volume / (MIN_CODEC_GAIN_DB * DB_DSP_SCALING_FACTOR);
        I2S_PRINT(("steps[%d]\n", (int)left_vol));
    }
    else
        I2S_PRINT(("I2S: Volume [%d]\n", left_vol));
    AllSetVolume(left_vol);
*/
}


Sink CsrI2SAudioOutputConnectAdpcm(uint32 rate, bool stereo, Source left_port)
{
    return AllConnectAdpcm(rate, stereo, left_port);
}


uint16 CsrI2SMusicResamplingFrequency(void)
{
    return ALL_MUSIC_SAMPLING_RATE;
}


uint16 CsrI2SVoiceResamplingFrequency(void)
{
    return ALL_VOICE_SAMPLING_RATE;
}


void CsrI2SAudioInputConnect(uint32 rate, bool stereo, Sink left_port, Sink right_port )
{
    Panic();
}


static Sink ConfigureI2SAsSink(uint32 rate, audio_channel channel)
{
    Sink sink = StreamAudioSink(AUDIO_HARDWARE_I2S, AUDIO_INSTANCE_0, channel);

    PanicFalse(SinkConfigure(sink, STREAM_I2S_MASTER_MODE, I2S_MASTER_MODE));

    if (I2S_MASTER_MODE)
        PanicFalse(SinkConfigure(sink, STREAM_I2S_MASTER_CLOCK_RATE, (rate * MCLK_SCALING_FACTOR)));

    PanicFalse(SinkConfigure(sink, STREAM_I2S_SYNC_RATE, rate));
    PanicFalse(SinkConfigure(sink, STREAM_I2S_JSTFY_FORMAT, 0));
    PanicFalse(SinkConfigure(sink, STREAM_I2S_LFT_JSTFY_DLY, 1));
    PanicFalse(SinkConfigure(sink, STREAM_I2S_CHNL_PLRTY, 0));
    PanicFalse(SinkConfigure(sink, STREAM_I2S_BITS_PER_SAMPLE, 16));
    /* PanicFalse(SinkConfigure(lSink_A, STREAM_I2S_TX_START_SAMPLE, 1)); */

    return sink;
}


static Source ConfigureI2SAsSource(uint32 rate, audio_channel channel)
{
    Source src = StreamAudioSource(AUDIO_HARDWARE_I2S, AUDIO_INSTANCE_0, channel);
    PanicFalse(SourceConfigure(src, STREAM_I2S_MASTER_MODE, I2S_MASTER_MODE));
    PanicFalse(SourceConfigure(src, STREAM_I2S_SYNC_RATE, rate));
    PanicFalse(SourceConfigure(src, STREAM_I2S_JSTFY_FORMAT, 0));
    PanicFalse(SourceConfigure(src, STREAM_I2S_LFT_JSTFY_DLY, 1));
    PanicFalse(SourceConfigure(src, STREAM_I2S_CHNL_PLRTY, 0));
    PanicFalse(SourceConfigure(src, STREAM_I2S_BITS_PER_SAMPLE, 16));
    return src;
}


static void ConnectToDSP(uint32 rate, Source src_a, Source src_b)
{
    all->dsp_sink_a = ConfigureI2SAsSink(rate, AUDIO_CHANNEL_SLOT_1);

    /* NOTE: HFP indicates stereo but passes only src_a */
    if (src_b)
    {
        all->dsp_sink_b = ConfigureI2SAsSink(rate, AUDIO_CHANNEL_SLOT_0);
        PanicFalse(SinkSynchronise(all->dsp_sink_a, all->dsp_sink_b));
        if (IsSkullHdBull() && !all->a2dpStreaming)
        {
            Source temp = src_a;
            I2S_PRINT(("I2S: L/R swap (before DSP)\n"));
            src_a = src_b;
            src_b = temp;
        }
        PanicFalse(StreamConnect(src_a, all->dsp_sink_a));
        PanicFalse(StreamConnect(src_b, all->dsp_sink_b));
    }
    else
    {
        all->dsp_sink_b = NULL;
        PanicFalse(StreamConnect(src_a, all->dsp_sink_a));
    }
}


static void DisconnectFromDSP(void)
{
    if (all->dsp_sink_a)
    {
        StreamDisconnect(0, all->dsp_sink_a);
        SinkClose(all->dsp_sink_a);
        all->dsp_sink_a = NULL;
    }
    if (all->dsp_sink_b)
    {
        StreamDisconnect(0, all->dsp_sink_b);
        SinkClose(all->dsp_sink_b);
        all->dsp_sink_b = NULL;
    }
}


static void ConnectToSpeaker(uint32 rate, bool stereo, Source src_a, Source src_b)
{
    if (stereo)
    {
        all->speaker_sink_a = StreamAudioSink(AUDIO_HARDWARE_CODEC, AUDIO_INSTANCE_0, AUDIO_CHANNEL_A);
        all->speaker_sink_b = StreamAudioSink(AUDIO_HARDWARE_CODEC, AUDIO_INSTANCE_0, AUDIO_CHANNEL_B);
    }
    else
        all->speaker_sink_a = StreamAudioSink(AUDIO_HARDWARE_CODEC, AUDIO_INSTANCE_0, AUDIO_CHANNEL_A);

    PanicFalse(SinkConfigure(all->speaker_sink_a, STREAM_CODEC_OUTPUT_RATE, rate));
    #if 0
    if (all->productId == ALL_PRODUCT_BULL)    
        PanicFalse(SinkConfigure(all->speaker_sink_a, STREAM_CODEC_OUTPUT_GAIN, (CODEC_OUTPUT_GAIN_RANGE-10)));        /*zzzfix --- decrease the volume a little bit*/
    #endif
    if (stereo)
    {
        PanicFalse(SinkConfigure(all->speaker_sink_b, STREAM_CODEC_OUTPUT_RATE, rate));
        #if 0
        if (all->productId == ALL_PRODUCT_BULL)    
            PanicFalse(SinkConfigure(all->speaker_sink_b, STREAM_CODEC_OUTPUT_GAIN, (CODEC_OUTPUT_GAIN_RANGE-10)));    /*zzzfix --- decrease the volume a little bit*/
        #endif        
        PanicFalse(SinkSynchronise(all->speaker_sink_a, all->speaker_sink_b));
    }

    PanicFalse(StreamConnect(src_a, all->speaker_sink_a));
    if (stereo)
        PanicFalse(StreamConnect(src_b, all->speaker_sink_b));
}


static void ConnectDspToSpeaker(uint32 rate, bool stereo)
{
    Source src_a = NULL, src_b = NULL;

    if (stereo)
    {
        src_a = ConfigureI2SAsSource(rate, AUDIO_CHANNEL_A);
        src_b = ConfigureI2SAsSource(rate, AUDIO_CHANNEL_B);
        if (IsSkullHdBull())
        {
            Source temp = src_a;
            I2S_PRINT(("I2S: L/R swap (after DSP)\n"));
            src_a = src_b;
            src_b = temp;
        }
    }
    else
        src_a = ConfigureI2SAsSource(rate, AUDIO_CHANNEL_B);

    ConnectToSpeaker(rate, stereo, src_a, src_b);
}


static void DoDisconnect(void)
{
    AllResetDock();
    if (all->speaker_sink_a)
    {
        StreamDisconnect(0, all->speaker_sink_a);
        SinkClose(all->speaker_sink_a);
        all->speaker_sink_a = NULL;
    }

    if (all->speaker_sink_b)
    {
        StreamDisconnect(0, all->speaker_sink_b);
        SinkClose(all->speaker_sink_b);
        all->speaker_sink_b = NULL;
    }

    DisconnectFromDSP();
}


Sink AllConnect(uint32 rate, bool stereo, Source src_a, Source src_b)
{
    /* Source src; */
    
    uint32 maxRate = CsrI2SMusicResamplingFrequency();
    if (rate > maxRate)
        rate = maxRate;

    I2S_PRINT(("I2S: Connect (%lu %s)\n", rate, stereo ? "stereo" : "mono"));


    AllMute();
    DoDisconnect();
    ConnectToDSP(rate, src_a, src_b);
    ConnectDspToSpeaker(rate, stereo);
    
    /*following is to set the ADC input gain
        ADC input actually has 3 gains:
            pre-gain            --- this is controlled ??? where ???
            ADC analog gain     --- the next 2 gains are controlled together
            ADC digital gain

            how about the gain set in SinkCfg tools: line gain, level 0~31 ???

        the first 2 gains controlled like this in the table
        
        the 3rd value in function SourceConfigure(src, STREAM_CODEC_INPUT_GAIN, (CODEC_INPUT_GAIN_RANGE-21))
        
        idx     ADC analog gain     ADC digital gain        the left 2 gains added together
        22      7(  0dB)            7(+21.5dB)              +21.5dB
        21      7(  0dB)            6(+18.0dB)              +18.0dB
        20      7(  0dB)            5(+15.5dB)              +15.5dB
        19      7(  0dB)            4(+12.0dB)              +12.0dB
        18      7(  0dB)            3( +9.5dB)               +9.5dB
        17      7(  0dB)            2( +6.0dB)               +6.0dB
        16      7(  0dB)            1( +3.5dB)               +3.5dB
        15      7(  0dB)            0(    0dB)                  0dB
        14      6( -3dB)            0(    0dB)               -3.0dB
        13      5( -6dB)            0(    0dB)               -6.0dB
        12      4( -9dB)            0(    0dB)               -9.0dB
        11      3(-12dB)            0(    0dB)              -12.0dB
        10      2(-15dB)            0(    0dB)              -15.0dB
         9      1(-18dB)            0(    0dB)              -18.0dB
         8      0(-21dB)            0(    0dB)              -21.0dB
         7      0(-21dB)           15( -2.5dB)              -23.5dB
         6      0(-21dB)           14( -6.0dB)              -27.0dB
         5      0(-21dB)           13( -8.5dB)              -29.5dB
         4      0(-21dB)           12(-12.0dB)              -33.0dB
         3      0(-21dB)           11(-14.5dB)              -35.5dB
         2      0(-21dB)           10(-18.0dB)              -39.0dB
         1      0(-21dB)            9(-20.5dB)              -41.5dB
         0      0(-21dB)            8(-24.0dB)              -45.0dB
            
    */
#if 0    
    src = StreamAudioSource(AUDIO_HARDWARE_CODEC, AUDIO_INSTANCE_0, AUDIO_CHANNEL_A);
    /* SourceConfigure(src, STREAM_CODEC_MIC_INPUT_GAIN_ENABLE, 0);    */
    PanicFalse(SourceConfigure(src, STREAM_CODEC_INPUT_GAIN, (4)));
    src = StreamAudioSource(AUDIO_HARDWARE_CODEC, AUDIO_INSTANCE_0, AUDIO_CHANNEL_B);
    /* SourceConfigure(src, STREAM_CODEC_MIC_INPUT_GAIN_ENABLE, 0);    */
    PanicFalse(SourceConfigure(src, STREAM_CODEC_INPUT_GAIN, (4)));
#endif    
    
            
    AllUnmute(FALSE);
    
    return all->dsp_sink_a;
}


void AllDisconnect(void)
{
    /* We do lazy disconnect to keep the I2S clock running at all times */
    AllMute();
    I2S_PRINT(("I2S: Disconnect (not really)\n"));
}


Sink AllConnectAdpcm(uint32 rate, bool stereo, Source src_a)
{
    /* We don't (and we shouldn't) use ADPCM anymore */
    Panic();
    return NULL;
}
