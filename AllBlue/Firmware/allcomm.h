#ifndef AllBlue_allcomm_h
#define AllBlue_allcomm_h

#include <message.h>
#include <source.h>


#define xDEBUG_DFI_UPGRADE
#ifdef DEBUG_DFI_UPGRADE
#   define DFI_PRINT(x) printf x
#else
#   define DFI_PRINT(x)
#endif


#define _MASK(x) ((uint32)1 << x)
#define dly(d) { uint32 i = (uint32)d * 1000uL; while (i--) ; }

#define PIO_SDA 3
#define PIO_DC 11
#define PIO_3V3_SKULL_XS 12
#define PIO_MUTE 13
#define PIO_VDDCORE_SKULL_XS 14
#define PIO_SPI_CLK 21
#define PIO_NFC_SESSION 24
#define PIO_SPI_MISO 26
#define PIO_ARST 27

/* Skull HD specific: */
#define PIO_IR 1
#define PIO_I2S_SEL 8
#define PIO_FSEL 9
#define PIO_APPLE_CP_RESET 12
#define PIO_SPI_SEL 28
#define PIO_FMODE_HD 6

#define PIO_SCL PIO_DC

#define EN_3V3 TRUE
#define DIS_3V3 FALSE

#define PROGRESS_LED_HD LED_2

extern uint16 NowInUpdatingDspMfi;  /*zzzfix*/

#define InitCrc() 0x6363
void UpdateCrc(uint16* crc, uint8 ch);

void I2cStart(bool reqOrRsp);
void I2cStop(void);
void I2cTrx(int d);
int  I2cRcv(void);

void DSPWrite(const uint8* buf, size_t len);
void DSPRead(uint8* buf, size_t len);

void DfiStartUpgrade(void);

void ShowBatteryLevel(int level, bool blink_last);
void ShowProgress(int level);

#endif
