

#include <stdlib.h>
#include <stdio.h>

#include <pio.h>
#include <ps.h>
#include <led.h>
#include <boot.h>
#include <message.h>
#include <panic.h>
#include <sink.h>
#include <transform.h>
#include <source.h>
#include <pio_common.h>

#include "sink_private.h"
#include "sink_configmanager.h"
#include "sink_statemanager.h"
#include "sink_powermanager.h"
#include "sink_audio_prompts.h"
#include "sink_slc.h"
#include "sink_tones.h"
#include "sink_leds.h" /* for LedsIndicateError() */

#include "alldsp.h"
#include "allcomm.h"
#include "allnfc.h"

#include "../DFUWizard/common/globalversioninfo.h"


#define CONFIG_PRODUCT_SERIAL CONFIG_DSP(9)
#define CONFIG_DSP_VERSION CONFIG_DSP(24)
#define CONFIG_RESCUE_MODE CONFIG_DSP(25)
#define CONFIG_JINGLE_MODE CONFIG_DSP(26)

#define PSKEY_USB_BCD_DEVICE 0x007d
#define PSKEY_DEVICE_NAME 0x0108

#define MAX_DEVICE_NAME 48


/*** DEBUG MACROS ************************************************************/

#define xDEBUG_BATTERY_READINGS
#define xDEBUG_VOLUME
#define xDEBUG_AUDIO
#define xDEBUG_POWER
#define xFORCE_FMODE

#ifdef DEBUG_BATTERY_READINGS
#   define BAT_PRINT(x) printf x
#else
#   define BAT_PRINT(x)
#endif

#ifdef DEBUG_VOLUME
#   define VOLUME_PRINT(x) printf x
#else
#   define VOLUME_PRINT(x)
#endif

#ifdef DEBUG_AUDIO
#   define AUDIO_PRINT(x) printf x
#else
#   define AUDIO_PRINT(x)
#endif

#ifdef DEBUG_POWER
#   define POWER_PRINT(x) printf x
#else
#   define POWER_PRINT(x)
#endif

/*** /DEBUG MACROS ***********************************************************/


#define FIXED_MUSIC_VOLUME 15

#define ENABLE_DFI_UPGRADE 1
#define ENABLE_DEEP_SLEEP 0
#define ENABLE_JINGLES 1
#define SEND_RC_COMMANDS_TO_DSP 1

/* Use TonesGetToneVolume() for jingles: the range is 0..31 so we halve it */
#define FixedJingleVolume() (TonesGetToneVolume(FALSE) / 2)
#if ENABLE_JINGLES
#  define JinglesEnabled() ((bool)TonesGetToneVolume(FALSE))
#else
#  define JinglesEnabled() FALSE
#endif

#define VOLUME_RANGE 16
#define UNMUTE_DELAY 1000
/* #define HEARTBEAT_MS 200 */ /* zzzfix, make it slower, to ensure TWS */
#define HEARTBEAT_MS 250       /* zzzfix, make it slower, to ensure TWS */
#define SLEEP_HEARTBEAT_SECS 2

#define BATTERY_VOLTAGE_MIN 6000
#define BATTERY_VOLTAGE_MAX 8500
#define BATTERY_VOLTAGE_CRITICAL 6500
#define BATTERY_VOLTAGE_ADJUST 1173

/* was originally: then changed by zzzfix
#define WHITE_LED_BRIGHTNESS_BULL 0x0800
*/
#define WHITE_LED_BRIGHTNESS_BULL 0x0600
#define BLUE_LED_BRIGHTNESS_BULL  0x0800

#define BLUE_LED_BRIGHTNESS_BULL_OFFSET  0x02
                                               /*defind added by zzzfix 20150530*/
#define WHITE_LED_BRIGHTNESS_BULL_OFFSET 0x02
                                               /*defind added by zzzfix 20150530*/

#if defined(FORCE_DIGITAL_OUTPUT_SAMPLE_RATE_MUSIC) && FORCE_DIGITAL_OUTPUT_SAMPLE_RATE_MUSIC != 44100
#   error "FORCE_DIGITAL_OUTPUT_SAMPLE_RATE_MUSIC should be set to 44100 (rebuild the library after setting)"
#endif



/* time to delay the power-on jingle */
#define DSP_STARTUP_SECS_OTHER 5
#define DSP_STARTUP_SECS_HD 4


/*added by zzzfix 20150530 to have better breath*/
#include "Ml_BreathTable.h"
/*
const uint16 WhiteLedBreathPwmArrayForBull[32];
const uint16 BlueLedBreathPwmArrayForBull[32];    these 2 constant tables are included in Ml_BreathTable.h
*/

typedef enum
{
    /* Message Ids for AllManagementTask */
    ALL_MGMT_REBOOT,
    ALL_MGMT_DSP_ON,
    ALL_MGMT_DSP_ON_1,
    ALL_MGMT_DSP_ON_2,
    ALL_MGMT_POWER_OFF,
    ALL_MGMT_BATTERY_LEDS_OFF,
    ALL_MGMT_UNMUTE,
    ALL_MGMT_BATTERY_STATUS_CHANGED,
    ALL_MGMT_SELECT_I2S
} PowerTaskIds;


enum
{
    DSP_FLAG_CHARGING_COMPLETE    = 0x01,
    DSP_FLAG_CHARGING             = 0x02,
    DSP_FLAG_LOW_VOLTAGE          = 0x04,
    DSP_FLAG_SIGNAL_TIMEOUT       = 0x08,
    DSP_FLAG_SIGNAL_PRESENT       = 0x10,
    DSP_FLAG_DOCK_STREAMING       = 0x40
};


AllData* all = NULL;

static int NoDCPlugged_BatLowJingleControlCnt = 0;  /*zzzfix*/

/* GLOBALS */
static int heartbeats = 0; /* used locally in HeartbeatTask() */
static uint8 dsp_opcode = 0; /* used locally in CommunicateWithDSPv2() */

static void AllManagement(Task t, MessageId id, Message payload);
static TaskData AllManagementTask = { AllManagement };
static void IndicatorLEDs(Task t, MessageId id, Message payload);
static TaskData IndicatorLEDsTask = { IndicatorLEDs };
static void HeartbeatTask(Task t, MessageId id, Message payload);
static TaskData HeartbeatTaskData = { HeartbeatTask };
static void SleepHeartbeatTask(Task t, MessageId id, Message payload);
static TaskData SleepHeartbeatTaskData = { SleepHeartbeatTask };
static void NoDspHeartbeatTask(Task t, MessageId id, Message payload);
static TaskData NoDspHeartbeatTaskData = { NoDspHeartbeatTask };
static void PlayJingleTask(Task t, MessageId id, Message payload);
static TaskData PlayJingleTaskData = { PlayJingleTask };



/* v2 opcodes in direction DSP -> CSR */
enum
{
    CSR_OPCODE_NONE = 0,
    CSR_OPCODE_MFI_VERSION_LOW = 1,
    CSR_OPCODE_MFI_VERSION_MID = 2,
    CSR_OPCODE_MFI_VERSION_HIGH = 3,
    CSR_OPCODE_MFI_STATUS = 4,
    CSR_OPCODE_DSP_VERSION_LOW = 5,
    CSR_OPCODE_DSP_VERSION_MID = 6,
    CSR_OPCODE_DSP_VERSION_HIGH = 7,
    CSR_OPCODE_OUTDATA = 8
};


/* v2 opcodes in direction CSR -> DSP */
enum
{
    CSR_OPCODE_VOLUME = 1,
    CSR_OPCODE_BATTERY_LEVEL = 2,
    CSR_OPCODE_BATTERY_TEMPERATURE = 3,
    CSR_OPCODE_ITHING_COMMAND = 0xc0,
    CSR_OPCODE_ITHING_SERIAL = 0xe0,
    CSR_OPCODE_ITHING_VERSION = 0xd0
};


/*** DFI Version PSKeys ******************************************************/


static void RetrieveDspVersionInfo(void)
{
    uint16 rescueMode = 0;
    
    all->rescueMode = FALSE;
    all->JingleIsOn = TRUE;
	if (PsRetrieve(CONFIG_JINGLE_MODE, &all->JingleIsOn, sizeof(all->JingleIsOn)) != sizeof(all->JingleIsOn))
        all->JingleIsOn = TRUE;
	if (PsRetrieve(CONFIG_RESCUE_MODE, &rescueMode, sizeof(rescueMode)) == sizeof(rescueMode))
        all->rescueMode = TRUE;
	if (PsRetrieve(CONFIG_DSP_VERSION, &all->dspVersion, sizeof(all->dspVersion)) != sizeof(all->dspVersion))
        memset(&all->dspVersion, 0, sizeof(all->dspVersion));
}


void UpdateDspVersionInfo(void)
{
    PanicFalse(PsStore(CONFIG_DSP_VERSION, &all->dspVersion, sizeof(all->dspVersion)) == sizeof(all->dspVersion));
}
void UpdateJingleMode(void)
{
    PanicFalse(PsStore(CONFIG_JINGLE_MODE, &all->JingleIsOn, sizeof(all->JingleIsOn)) == sizeof(all->JingleIsOn));
}

/****************zzz jingle on/off flag   ************************************/
void AllToggleJingleOnOffFlag(void)
{
    all->JingleIsOn=1-all->JingleIsOn;
    UpdateJingleMode();
}
/****************zzz jingle on/off flag   ************************************/


/*****************************************************************************/


static void PlayJingleTask(Task t, MessageId id, Message payload)
{
    AUDIO_PRINT(("JINGLE: play? (startup=%d, poweron?=%d)\n", all->startup, id == EventUsrPowerOn));
    
    if(!all->JingleIsOn) return;
    
    if (id == EventUsrPowerOn)
        all->startup = ALL_APP_STARTUP_JINGLE_FINISHED;
    if (JinglesEnabled())
    {
        /* We postpone any jingles if the startup one hasn't been played yet */
        if (all->startup >= ALL_APP_STARTUP_JINGLE_FINISHED)
            AudioPromptPlayEvent(id);
        else
            MessageSendLater(t, id, NULL, 500);
    }
}


static void DfiStartUpgradeAndCancelLEDs(void)
{
    /* TODO: turn off the LEDs */
#if ENABLE_DFI_UPGRADE
     MessageCancelAll(&IndicatorLEDsTask, 0);
     DfiStartUpgrade();
#endif
}


void AllAbort(int code)
{
    LedsIndicateError(code);
}


void AllResetDock(void)
{
    AUDIO_PRINT(("DOCK: reset (startup=%d)\n", all->startup));
    if (IsSkullHdBull())
    {
        PioSet32(_MASK(PIO_I2S_SEL), FALSE); /*disconnect I2S*/
		PioSet32(_MASK(PIO_SPI_SEL), FALSE); /*disconnect SPI from second flash*/
        PioCommonSetPin(PIO_ARST, pio_drive, FALSE);
        MessageCancelAll(&AllManagementTask, 0);
        if (all->dsp == DSP_STATUS_ON && all->startup >= ALL_APP_STARTUP_JINGLE_POSTED)
        {
            PioCommonSetPin(PIO_ARST, pio_pull, TRUE);
            MessageSend(&AllManagementTask, ALL_MGMT_SELECT_I2S, NULL);
        }
    }
    if (all->startup == ALL_APP_POWERED_ON)
    {
        all->startup = ALL_APP_STARTUP_JINGLE_POSTED;
        MessageSendLater(&PlayJingleTaskData, EventUsrPowerOn, NULL,
            (IsSkullHdBull() ? DSP_STARTUP_SECS_HD : DSP_STARTUP_SECS_OTHER) * 1000);
    }
}

void AllDecoderTypeNotify(A2dpAudioDecoderType DecType)
{
    all->CurrentA2dpAudioDecoderUsed=DecType;
    return;
}

static void CommunicateWithDSPv1(void)
{
    {
        uint8 wbuf[7] = {0xff, 0xaa, 0, 0, 0, 0x55, 0x7f};
        wbuf[2] = all->volume | (all->dcOn * 0x80);
        wbuf[3] = (all->batteryLevel - BATTERY_VOLTAGE_MIN) / 10;
        wbuf[4] = all->batteryTemperature / 4;
        DSPWrite(wbuf, sizeof(wbuf));
    }

    {
        uint8 rbuf[6];
        DSPRead(rbuf, sizeof(rbuf));
        /* TODO: version number is ignored, maybe DSP shouldn't send it? */
        if (rbuf[1] == 0xa5 && rbuf[5] == 0xd9)
        {
            Version dfiVer = ExpectedDfiVersion(), mfiVer = ExpectedMfiVersion();
            if (rbuf[0] & 0x80)
                all->dspFlags = rbuf[0];
            all->dspRetryCount = 0;
            /*zzzfix, this is to: after EXE update, device on, no DC plugged, then DC plugged, but dsp version is old*/            
            if (ENABLE_DFI_UPGRADE && all->dcOn &&
                (memcmp(&dfiVer, &all->dspVersion.dfi, sizeof(Version)) != 0 ||
                    memcmp(&mfiVer, &all->dspVersion.mfi, sizeof(Version)) != 0))
            {            
                    DfiStartUpgradeAndCancelLEDs();
            }
            /*zzzfix, this is to: after EXE update, device on, no DC plugged, then DC plugged, but dsp version is old*/            
        }
        else /* bad packet? */
        {
            all->dspRetryCount++;
            if (all->dspRetryCount > 100 && all->dcOn)
            {
                all->dspRetryCount = 0;
                DFI_PRINT(("DFI: DSP timed out\n"));
#if 1
                if (all->dsp == DSP_STATUS_ON)          /*zzzfix --- found that after DC plugged, XS will do DSP update, 
                                                        seems like XS is power off, DSP is not resonding, while runs here,
                                                        will cause DSP update. So, added this condition check*/
                /*zzzfix --- for XS DC plug strange things: always updateDSP, no device on jingle on powering up ...*/                    
#endif                    
                    DfiStartUpgradeAndCancelLEDs();
            }
        }
    }
}

static void CommunicateWithDSPv2(void) /* Skull_HD and Bull only */
{
    uint8 wbuf[4] = {0xab, 0, 0, 0x59};
    uint8 rbuf[3];

    if (PioCommonGetPin(PIO_FMODE_HD))
    {
        DFI_PRINT(("Factory Mode!!\n"));
        return;
    }
#if defined FORCE_FMODE
    DFI_PRINT(("Forced Factory Mode!!\n"));
    return;
#endif

    if (all->serialSent < 8)
        dsp_opcode = CSR_OPCODE_ITHING_SERIAL;
    else if (all->aeroVersionSent < 8)
        dsp_opcode = CSR_OPCODE_ITHING_VERSION;

    if (all->remoteEventMask)
    {
        dsp_opcode=CSR_OPCODE_ITHING_COMMAND;
        all->serialSent = 0;
        all->aeroVersionSent = 0;
    }
    
        
    wbuf[1]=dsp_opcode;
    switch(dsp_opcode)
    {
        case CSR_OPCODE_VOLUME:
            wbuf[2] = all->volume | (all->dcOn * 0x80);
            dsp_opcode=CSR_OPCODE_ITHING_COMMAND;
            break;
        case CSR_OPCODE_BATTERY_LEVEL:
            dsp_opcode=CSR_OPCODE_BATTERY_TEMPERATURE;
            break;    
        case CSR_OPCODE_BATTERY_TEMPERATURE:
            dsp_opcode=CSR_OPCODE_VOLUME; 
            break;   
        case CSR_OPCODE_ITHING_COMMAND:
            if (all->remoteEventMask)   
            {
                wbuf[2] = all->remoteEventMask;
                if (!(all->dspFlags & DSP_FLAG_DOCK_STREAMING) && !(all->remoteEventMask & REMOTE_PLAY_PAUSE))
                    wbuf[2] = 0;
                if (!SEND_RC_COMMANDS_TO_DSP)
                    wbuf[2] = 0;
                DFI_PRINT(("Remote: %d\n", (uint8) all->remoteEventMask));
            }
            else
            {
                wbuf[1] = 0;
                wbuf[2] = 0;
            }
            dsp_opcode=CSR_OPCODE_VOLUME;
            break;
        case CSR_OPCODE_ITHING_SERIAL:
            wbuf[1] = CSR_OPCODE_ITHING_SERIAL + all->serialSent;
            wbuf[2] = all->serial[all->serialSent];
            dsp_opcode = CSR_OPCODE_VOLUME;
            all->serialSent++;
            all->remoteEventMask = 0;
            break;
        case CSR_OPCODE_ITHING_VERSION:
            wbuf[1] = CSR_OPCODE_ITHING_VERSION + all->aeroVersionSent;
            wbuf[2] = all->aeroVersion[all->aeroVersionSent];
            dsp_opcode = CSR_OPCODE_VOLUME;
            all->aeroVersionSent++;
            all->remoteEventMask = 0;
            break;
        default:
            dsp_opcode=CSR_OPCODE_VOLUME;
            break;
    }
    
    VOLUME_PRINT(("VOL: dsp_opcode=%d, wbuf[2]=0x%02x\n", wbuf[1], wbuf[2]));
    DSPWrite(wbuf, sizeof(wbuf));

    DSPRead(rbuf, sizeof(rbuf));
    if (rbuf[2] == 0xd9)
    {
        switch(rbuf[0])
        {
            /* TODO: version numbers are ignored, maybe DSP shouldn't send it? */
            case CSR_OPCODE_OUTDATA:
                all->dspFlags = rbuf[1];
            case CSR_OPCODE_NONE:
                break;
            case CSR_OPCODE_DSP_VERSION_LOW:
                /* dsp_ver.low=rbuf[1]; */
                break;
            case CSR_OPCODE_DSP_VERSION_MID:
                /* dsp_ver.mid=rbuf[1]; */
                break;
            case CSR_OPCODE_DSP_VERSION_HIGH:
                /* dsp_ver.high=rbuf[1]; */
                break;
            case CSR_OPCODE_MFI_VERSION_LOW:
                /* mfi_ver.low=rbuf[1]; */
                break;
            case CSR_OPCODE_MFI_VERSION_MID:
                /* mfi_ver.mid=rbuf[1]; */
                break;
            case CSR_OPCODE_MFI_VERSION_HIGH:
                /* mfi_ver.high=rbuf[1]; */
                break;
        }
        if (wbuf[1]==CSR_OPCODE_ITHING_COMMAND)
            all->remoteEventMask = 0; /* Confirmed delivery; clear the RC command mask */
        all->dspRetryCount = 0;
    }
    else /* bad packet */
    {
        DFI_PRINT(("WARNING: bad packet from DSP [%02x %02x %02x]\n", rbuf[0], rbuf[1], rbuf[2]));
        all->dspRetryCount++;
        /* On some HD boards the DSP may require quite some time to start 
           responding, hence 200 retries */
        if (all->dspRetryCount > 200 && all->dcOn)
        {
            all->dspRetryCount = 0;
            DFI_PRINT(("DFI: DSP timed out\n"));
            DfiStartUpgradeAndCancelLEDs();
        }
    }
}


static void CommunicateWithDSP(void)
{
    switch (all->dsp)
    {
    case DSP_STATUS_OFF:
        /* Shouldn't be called */
        break;

    case DSP_STATUS_INITIALIZING:
        break;

    case DSP_STATUS_UPGRADING_MAIN:
    case DSP_STATUS_UPGRADING_SECONDARY:
        Panic();
        break;

    case DSP_STATUS_ON_UNVERIFIED:
        all->dsp = DSP_STATUS_ON;
        {
            Version dfiVer = ExpectedDfiVersion(), mfiVer = ExpectedMfiVersion();
            RetrieveDspVersionInfo();   /*zzzfix --- for XS DC plug strange things: always updateDSP, no device on jingle on powering up ...*/
                                        /* adding RetrieveDspVersionInfo(); here is no helpful to the abve problem, but keep it here is also OK
                                        */
            if (ENABLE_DFI_UPGRADE && all->dcOn &&
                (memcmp(&dfiVer, &all->dspVersion.dfi, sizeof(Version)) != 0 ||
                    memcmp(&mfiVer, &all->dspVersion.mfi, sizeof(Version)) != 0))
            {
                DfiStartUpgradeAndCancelLEDs();
            }
            else
                AllResetDock();
        }
        break;

    case DSP_STATUS_ON:
        if (IsSkullHdBull())
            CommunicateWithDSPv2();
        else
            CommunicateWithDSPv1();
        break;
    }
}


static void SetBatteryStatus(BatteryStates newStatus)
{
    if (all->batteryStatus != newStatus)
    {
        all->batteryStatus = newStatus;
        MessageSend(&AllManagementTask, ALL_MGMT_BATTERY_STATUS_CHANGED, 0);
    }
}


static void SetBatteryLEDs(void)
{
    if (IsSkullXsTwist())
    {
        if (all->dcOn)
        {
            /* TODO: don't rely on DSP flags */
            if (all->dspFlags & DSP_FLAG_CHARGING_COMPLETE)
            {
                SetBatteryStatus(BATTERY_STATUS_FULL);
                ShowBatteryLevel(5, FALSE);
            }

            else if (all->dspFlags & DSP_FLAG_LOW_VOLTAGE)
            {
                SetBatteryStatus(BATTERY_STATUS_LOW);
                ShowBatteryLevel(0, TRUE);
            }

            else if (all->dspFlags & DSP_FLAG_CHARGING)
            {
                SetBatteryStatus(BATTERY_STATUS_CHARGING);

                if (all->batteryLevel < 7200)
                    ShowBatteryLevel(0, TRUE);

                else if (all->batteryLevel < 7400)
                    ShowBatteryLevel(1, TRUE);

                else if (all->batteryLevel < 7600)
                    ShowBatteryLevel(2, TRUE);

                else if (all->batteryLevel < 7800)
                    ShowBatteryLevel(3, TRUE);

                else if (all->batteryLevel < 8000)
                    ShowBatteryLevel(4, TRUE);

                else
                    ShowBatteryLevel(5, FALSE);
            }

            else
            {
                SetBatteryStatus(BATTERY_STATUS_OK);
                ShowBatteryLevel(0, FALSE);
                ShowProgress(1);
            }
        }

        else /* no DC */
        {
            /* TODO: trigger low battery event when DC is not connected */
            /*zzzfix*/
            if (all->batteryLevel < 6600)
            {
                /*zzzfix IndicatorLEDs process this --> blinking BatLed0*/
                /* ShowBatteryLevel(0, TRUE); */
                ShowBatteryLevel(1, FALSE);      /*still display the same as one upper level*/
				/*at this level, if to use: ShowBatteryLevel(0, TRUE); then BatLed0 could be off after you activate here, because
				it is in blinking*/
            }
            else if (all->batteryLevel < 7200)
                ShowBatteryLevel(1, FALSE);

            else if (all->batteryLevel < 7400)
                ShowBatteryLevel(2, FALSE);

            else if (all->batteryLevel < 7600)
                ShowBatteryLevel(3, FALSE);

            else if (all->batteryLevel < 7800)
                ShowBatteryLevel(4, FALSE);

            else
                ShowBatteryLevel(5, FALSE);
        }
    }
}

static void DisableDsp(void)
{
    PioCommonSetPin(PIO_ARST, pio_drive, FALSE);
    PioCommonSetPin(all->conf->pioTRST, pio_drive, FALSE);
    if (IsSkullXsTwist())
    {
        PioCommonSetPin(PIO_VDDCORE_SKULL_XS, pio_drive, FALSE);
        PioCommonSetPin(PIO_3V3_SKULL_XS, pio_drive, DIS_3V3);
    }
    /*zzzfix --- for XS DC plug strange1 things: always updateDSP, no device on jingle on powering up ...*/
    all->dsp = DSP_STATUS_OFF;  /*zzzfix: if not add this, after DC is unplugged, although DSP is powered off, 
                                but DSP status is still ON, and it will still keep in non-sleep heart beat*/
    /*zzzfix zzzfix zzzfix : this should be the critical fix the the XS strange1 issue */
                                /*zzz!!! is there effect to HD???*/
    DFI_PRINT(("DFI: DSP off/reset\n"));
}


static void EnableDsp(void)
{
    if (all->dsp == DSP_STATUS_OFF)
    {
        DisableDsp();
        all->dsp = DSP_STATUS_INITIALIZING;
        MessageSendLater(&AllManagementTask, ALL_MGMT_DSP_ON, 0, 10);
    }
}


static void IndicatorLEDs(Task t, MessageId id, Message payload)
{
    /* If powered off this task will shut itself down as we don't have any
       indication when not powered on */
    if (!all->startup)
        return;

#if 0
	/*this is to detect if aptx is enabled or not*/
    if(all->CurrentA2dpAudioDecoderUsed==DecoderType_APTX)
    {
        all->dimStage = (all->dimStage + 8) % 32;
    }else
    {
        all->dimStage = (all->dimStage + 1) % 32;
    }
#else
    all->dimStage = (all->dimStage + 1) % 32;
#endif	
        
    /*** WHITE LED ***/
    if (all->seconds <= 2)
    {
        /* First 2 seconds after power on */
        LedConfigure(all->conf->lineInLed, LED_DUTY_CYCLE, IsBull() ? WHITE_LED_BRIGHTNESS_BULL : 0x0fff);
        LedConfigure(all->conf->lineInLed, LED_ENABLE, TRUE);
    }
    else if (all->whiteCountdown)
    {
        LedConfigure(all->conf->lineInLed, LED_DUTY_CYCLE, IsBull() ? WHITE_LED_BRIGHTNESS_BULL : 0x0fff);
        LedConfigure(all->conf->lineInLed, LED_ENABLE, !all->wiredConnected);
        all->whiteCountdown--;
    }
    else if ((all->dspFlags & DSP_FLAG_SIGNAL_PRESENT) && analogAudioSinkMatch(theSink.routed_audio))
    {
#if 1 
        /*zzzfix no breath --- if set to #if 0, on the left*/
        /* Breath the white LED if wired audio is playing, i.e. DSP signal present and not A2DP */
        /*uint32 brightness = (all->dimStage > 16 ? 32 - all->dimStage : all->dimStage) * 256; */
        /* if (!IsBull()) */  /* this is original, zzzfix*/
        /* if (IsBull())
            brightness = brightness * WHITE_LED_BRIGHTNESS_BULL / 0x0fff; */
        
        /*zzzfix: on XS, the white LED breath works fine. on HD, it doesn't breath even analog signal is plugged and presented,
          because all->dspFlags never shows DSP_FLAG_SIGNAL_PRESENT, which seems to be wrong. DSP should report DSP_FLAG_SIGNAL_PRESENT when analog is
          plugged and played
        */
        
        if(IsBull())
        {
            /* zzzfix, original effect is bad, XS and SkullHD needs to be give longer PWM, because XS white LED is weaker than HD */
            LedConfigure(all->conf->lineInLed, LED_DUTY_CYCLE, WhiteLedBreathPwmArrayForBull[all->dimStage]);
            LedConfigure(all->conf->lineInLed, LED_ENABLE, TRUE);
        }else
        {
            /* zzzfix, original effect is bad, XS and SkullHD needs to be give longer PWM, because XS white LED is weaker than HD */
            LedConfigure(all->conf->lineInLed, LED_DUTY_CYCLE, 2*WhiteLedBreathPwmArrayForBull[all->dimStage]);
            LedConfigure(all->conf->lineInLed, LED_ENABLE, TRUE);
        }
        
#endif        
    }
    else if (all->wiredConnected)
    {
        LedConfigure(all->conf->lineInLed, LED_DUTY_CYCLE, IsBull() ? WHITE_LED_BRIGHTNESS_BULL : 0x0fff);
        LedConfigure(all->conf->lineInLed, LED_ENABLE, TRUE);
    }
    else
    {
        LedConfigure(all->conf->lineInLed, LED_ENABLE, FALSE);
    }


    /*** BLUE LED ***/
    if(IsBull())
    {
        /* for bull, this part added to fix BULL foot blue LED problem*/
        /* bull foot LED control is opposit from HD/XS  zzzfix 20150530 */
        if (all->a2dpStreaming)
        {
            /*
            uint32 brightness = (all->dimStage > 16 ? 32 - all->dimStage : all->dimStage) * 256;
            if (IsBull())
                brightness = 0x0fff - brightness * BLUE_LED_BRIGHTNESS_BULL / 0x0fff;
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, brightness);
            */
#if 1 
        /*zzzfix no breath --- if set to #if 0, on the left*/
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, BlueLedBreathPwmArrayForBull[all->dimStage]);
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, TRUE);
#endif
        }
        else if (all->a2dpConnected)
        {
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, BLUE_LED_BRIGHTNESS_BULL);
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, TRUE);
        }
        else
        {   /*blue LED off, actually PWM ON and PWM duty is full*/
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, TRUE);
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE,
                ((all->dimStage >> 3) & 1) ? (0x1000-BLUE_LED_BRIGHTNESS_BULL) : 0x0fff);   /*zzzfix*/
        }
    }else
    {
        /* for all others*/
        if (all->a2dpStreaming)
        {
#if 1 
        /*zzzfix no breath --- if set to #if 0, on the left*/
            /*
            uint32 brightness = (all->dimStage > 16 ? 32 - all->dimStage : all->dimStage) * 256;
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, brightness);
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, !IsBull());
            */
            
            /* zzzfix, original effect is bad, XS needs to be give longer PWM, because XS blue LED is weaker than SkullHD */
            if(IsSkullXsTwist())
                LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, 2*(0x1000-BlueLedBreathPwmArrayForBull[all->dimStage]));
            else
                LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, (0x1000-BlueLedBreathPwmArrayForBull[all->dimStage]));
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, !IsBull());
#endif            
        }
        else if (all->a2dpConnected)
        {
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE,
                IsBull() ? BLUE_LED_BRIGHTNESS_BULL : 0x0fff);
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, !IsBull());
        }
        else
        {
            LedConfigure(all->conf->bluetoothLed, LED_ENABLE, (all->dimStage >> 3) & 1);
            LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE,
                IsBull() ? BLUE_LED_BRIGHTNESS_BULL : 0x0fff);
        }
    }

#ifdef GREEN_LED_TWS_DEBUGGING
    switch (all->twsStatus)
    {
    case TWS_STATUS_OFF:
        SetGreenLed(FALSE);
        break;
    case TWS_STATUS_INQ:
        SetGreenLed((all->dimStage >> 4) & 1);
        break;
    case TWS_STATUS_DISC:
        SetGreenLed((all->dimStage >> 3) & 1);
        break;
    case TWS_STATUS_CONNECTED: /* currently unused */
        SetGreenLed(TRUE);
        break;
    }
#endif

    /*zzzfix --- blink RedLed (power off LED) when low battery at no DC plugged --- this is for XS, also handles jingle for battery low*/    
    if (IsSkullXsTwist())
    {    
        if (all->dcOn)
        {    
            if(all->startup != ALL_APP_SHUTDOWN_JINGLE_POSTED)
            {    
                /*zzzfix when dc plugged, and not powerdown triggered*/
                LedConfigure(all->conf->powerOffLed, LED_ENABLE, FALSE);
            }                
        }
        else
            if (all->batteryLevel < 6600)
            {
                /*zzzfix --- every 1 minutes, say "battery low" --- battery low state is around 1 minutes only*/
                NoDCPlugged_BatLowJingleControlCnt = (NoDCPlugged_BatLowJingleControlCnt + 1) % 300;
                if(NoDCPlugged_BatLowJingleControlCnt==150)
                    MessageSend(&PlayJingleTaskData, EventSysBatteryLow, 0);
                /*no need to clear NoDCPlugged_BatLowJingleControlCnt to 0, as long as it comes here for 0.5 min(first time)
                  or 1 min(not first time), say battery low */
                /*zzzfix --- every 8minutes, say "battery low"*/
                
                /*zzzfix --- should blink red LED*/                
                LedConfigure(all->conf->powerOffLed, LED_ENABLE, TRUE);
                LedConfigure(all->conf->powerOffLed, LED_DUTY_CYCLE,
                                    (NoDCPlugged_BatLowJingleControlCnt & 1) ? 0 : 0x0fff);                         
                /*zzzfix --- should blink red LED*/                
            }else
            {
                if(all->startup != ALL_APP_SHUTDOWN_JINGLE_POSTED)
                {    
                    /*zzzfix and not powerdown triggered*/
                    LedConfigure(all->conf->powerOffLed, LED_ENABLE, FALSE);
                }
            }
    }
    /*zzzfix --- blink BatLed0 when low battery at no DC plugged --- this is for XS, also handles jingle for battery low*/    
        

    
    /* zzzfix, try to do LED slower, ensure TWS, becasue TWS may have the max CPU load
               while IO operation seems to be slow  */
    /* MessageSendLater(&IndicatorLEDsTask, 0, NULL, 120); */
    MessageSendLater(&IndicatorLEDsTask, 0, NULL, 200);
}


static void CheckVoltageLevels(void)
{
    if (IsSkullXsTwist())
    {
        voltage_reading reading;
        if (PowerBatteryGetVoltage(&reading))
        {
            /* millivolts on AIO pin * divider (1.1xxx) (plus correction factor, possibly
               due to input impedance of CSR A/D) */
            all->batteryLevel = ((uint32)reading.voltage * BATTERY_VOLTAGE_ADJUST) / 100;
            if (all->batteryLevel < BATTERY_VOLTAGE_MIN)
                all->batteryLevel = BATTERY_VOLTAGE_MIN;
            if (all->batteryLevel > BATTERY_VOLTAGE_MAX)
                all->batteryLevel = BATTERY_VOLTAGE_MAX;
            if (all->batteryLevel < BATTERY_VOLTAGE_CRITICAL && !all->dcOn)
            {
                BAT_PRINT(("BAT: voltage=%ld POWER OFF\n", all->batteryLevel));
                powerManagerPowerOff();
            }
            BAT_PRINT(("BAT: voltage=%ld\n", all->batteryLevel));
        }
        if (PowerBatteryGetTemperature(&reading))
        {
            all->batteryTemperature = reading.voltage;
            BAT_PRINT(("BAT: temp reading=%d\n", reading.voltage));
            if (all->batteryTemperature > 1000)
                all->batteryTemperature = 1000;
        }
    }
}


static void CheckDC(void)
{
    if (IsSkullXsTwist())
    {
        bool dc;
        PioSetDir32(_MASK(PIO_DC), 0);
        dc = (PioGet32() & _MASK(PIO_DC)) != 0;
        PioSetDir32(_MASK(PIO_DC), _MASK(PIO_DC));
        PioSet32(_MASK(PIO_DC), 0);
        if (!dc && dc != all->dcOn) /* detect edge */
        {
            ShowBatteryLevel(0, FALSE);
            if (stateManagerGetState() == deviceLimbo)
                /* will call AllOkToSleep() to see if we can go to deep sleep */
                stateManagerUpdateLimboState();
        }
        all->dcOn = dc;
    }
    else
        all->dcOn = TRUE;
}


static void CheckWiredAudio(void)
{
    bool connected = all->startup && analogAudioConnected();
    if (connected != all->wiredConnected)
        AllWiredConnectedNotify(connected);
}


static void CheckDock(void)
{
    bool connected = all->startup && (all->dspFlags & DSP_FLAG_DOCK_STREAMING) != 0;
    if (connected != all->dockConnected)
        AllDockStreamingNotify(connected);
}


static void CheckDSPState(void)
{
    if (IsSkullHdBull())
    {
        if (all->startup && all->dsp == DSP_STATUS_OFF)
            EnableDsp();
        if (!all->startup && all->dsp == DSP_STATUS_ON)
            DisableDsp();
    }
    else
    {
        if (all->dcOn && all->dsp == DSP_STATUS_OFF)
            /* DSP needs to be running to charge the battery */
            EnableDsp();
        if (!all->dcOn && !all->startup && all->dsp == DSP_STATUS_ON)
             /* shut down DSP if power off and no DC input */
            DisableDsp();
    }
}


static void SleepHeartbeatTask(Task t, MessageId id, Message payload)
{
    /* Should be called when powered off and DSP is not running,
       so switch automatically if it's not the case: */
    if (all->dsp != DSP_STATUS_OFF)
    {
        MessageSendLater(&HeartbeatTaskData, 0, 0, HEARTBEAT_MS);
        POWER_PRINT(("POWER: Switching to full heartbeat\n"));
        /*
          zzz: on XS, if it is in sleep, charging LED will be displayed, but here
               in the sleep heart beat, can not display battery LED charging blinking,
               so the Heartbeat in non-sleep must be running.
               
               it is because: when in sleep you plug DC, CheckDSPState() will enable dsp running,
               and cause dsp status to be DSP_STATUS_ON_UNVERIFIED, then next time getting back here,
               will jump to HeartbeatTask, while all->startup is still OFF
        */
        return;
    }
    all->seconds += SLEEP_HEARTBEAT_SECS;
    CheckDC();
    /* CheckVoltageLevels(); */
    CheckDSPState();
    
    
    
    
#ifdef EnablePowerOnBlinkingAndSleepBlinking
    {
        static int k=0;
        /* for seeing it is running sleeping */
        LedConfigure(LED_0, LED_DUTY_CYCLE, 0x800);
        k++;
        if( (k&0x2) )
            LedConfigure(LED_0, LED_ENABLE, TRUE);
        else        
            LedConfigure(LED_0, LED_ENABLE, FALSE);
    }
#endif    
    
    MessageSendLater(t, 0, 0, SLEEP_HEARTBEAT_SECS * 1000);
}


static void NoDspHeartbeatTask(Task t, MessageId id, Message payload)
{
#if 0 /*zzzfix, is this part really necessary???*/   
    all->seconds++;
    CheckDC();
    CheckVoltageLevels();
    if (all->dcOn)
        SetBatteryLEDs();
    CheckWiredAudio();
    MessageSendLater(t, 0, 0, 1000);
#endif    
}


static void HeartbeatTask(Task t, MessageId id, Message payload)
{
    /* Should be called when powered on or DSP is running (because of charging),
       so switch automatically if it's not the case: */

    if (all->dsp == DSP_STATUS_OFF)
    {
        MessageSendLater(&SleepHeartbeatTaskData, 0, 0, SLEEP_HEARTBEAT_SECS * 1000);
        POWER_PRINT(("POWER: Switching to power saving heartbeat\n"));
        return;
    }

    heartbeats++;

    if (heartbeats % (1000 / HEARTBEAT_MS) == 0) /* roughly once per second */
    {
        all->seconds++;
        CheckDC();
        CheckVoltageLevels();
        if (all->dcOn)
            SetBatteryLEDs();
        CheckDSPState();
        
		/*this is to avoid line in played out at powering up, if line in is connected. Here means
		only to active line after powerOn complete, or 5s after after power on*/
        if((all->JinglePlayingIsFinished) /*&& (all->WorkingSeconds) > 5*/)  
            CheckWiredAudio();
        
        if (IsSkullHdBull())
            CheckDock();

        /* DSP signals a timeout */
        if (!all->sleepless && all->dsp == DSP_STATUS_ON && (all->dspFlags & DSP_FLAG_SIGNAL_TIMEOUT) &&
                !all->dcOn && all->startup && all->seconds > 300)
            powerManagerPowerOff();
    }

    CommunicateWithDSP();

    if (all->dsp == DSP_STATUS_UPGRADING_MAIN || all->dsp == DSP_STATUS_UPGRADING_SECONDARY)
        return;

    MessageSendLater(t, 0, 0, HEARTBEAT_MS);
}


static void AllManagement(Task t, MessageId id, Message payload)
{
    switch ((PowerTaskIds)id)
    {
    case ALL_MGMT_REBOOT:
        AllReboot();
        break;

    /* Three-stage DSP on procedure; called from EnableDsp() */
    case ALL_MGMT_DSP_ON:
        if (IsSkullXsTwist())
            PioCommonSetPin(PIO_3V3_SKULL_XS, pio_drive, EN_3V3);
        if (IsSkullHdBull())
        {
            int pioEecs = all->conf->dsp[DSP_SECONDARY].pioEECS;
            int pioMosi = all->conf->dsp[DSP_SECONDARY].pioMOSI;
            PioCommonSetPin(pioEecs, pio_drive, TRUE); /*CS pull-up*/
            PioCommonSetPin(PIO_APPLE_CP_RESET, pio_drive, TRUE);
            PioCommonSetPin(PIO_SPI_SEL, pio_drive, FALSE);
            PioCommonSetPin(PIO_I2S_SEL, pio_drive, FALSE);
            PioSetDir32(_MASK(pioEecs) | _MASK(pioMosi) | _MASK(PIO_SPI_MISO) | _MASK(PIO_SPI_CLK),
                        _MASK(pioEecs));
            PioSetMapPins32(all->conf->dsp[DSP_SECONDARY].dfiUsedPios, all->conf->usedPios); /*take control of PIO pins*/
            PioSet32(_MASK(pioEecs), _MASK(pioEecs)); /*CS pull-up*/
            PioSet32(_MASK(PIO_SPI_SEL), FALSE); /*disconnect SPI*/
        }
        MessageSendLater(&AllManagementTask, ALL_MGMT_DSP_ON_1, 0, 10);
        break;
    case ALL_MGMT_DSP_ON_1:
        if (IsSkullXsTwist())
            PioCommonSetPin(PIO_VDDCORE_SKULL_XS, pio_drive, TRUE);
        MessageSendLater(&AllManagementTask, ALL_MGMT_DSP_ON_2, 0, 10);
        break;
    case ALL_MGMT_DSP_ON_2:
        if (!IsSkullHdBull())
            PioCommonSetPin(PIO_ARST, pio_pull, FALSE);
        PioCommonSetPin(all->conf->pioTRST, pio_pull, FALSE);
        all->dsp = DSP_STATUS_ON_UNVERIFIED;
        all->dspRetryCount = 0;
        break;

    case ALL_MGMT_POWER_OFF:
        /* if (all->startup == ALL_APP_SHUTDOWN_JINGLE_POSTED && all->voicePromptPlaying) */
        if (all->startup == ALL_APP_SHUTDOWN_JINGLE_POSTED && !all->JinglePlayingIsFinished && all->JingleIsOn)
        {
			/* Delay power-off until the jingle is finished */
            MessageSendLater(&AllManagementTask, ALL_MGMT_POWER_OFF, 0, 500);
        }
        else
        {
            MessageCancelAll(&HeartbeatTaskData, 0);
            MessageCancelAll(&SleepHeartbeatTaskData, 0);
            MessageCancelAll(&NoDspHeartbeatTaskData, 0);

            /*
            DisableDsp();
            PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
            */
            /*zzzfix: to power down AMP first, then power down DSP*/
            PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
            DisableDsp();
            
            ShowBatteryLevel(0, FALSE);
            PioSetDir32(_MASK(PIO_SCL), _MASK(PIO_SCL));
            PioSetDir32(_MASK(PIO_SDA), _MASK(PIO_SDA));
            PioSet32(_MASK(PIO_SCL) | _MASK(PIO_SDA), 0);
            MessageSend(&AllManagementTask, ALL_MGMT_REBOOT, 0);
        }
        break;

    case ALL_MGMT_BATTERY_LEDS_OFF:
        ShowBatteryLevel(0, FALSE);
        break;

    case ALL_MGMT_UNMUTE:
        if (all->rescueMode)
            break;
        if(!NowInUpdatingDspMfi) PioCommonSetPin(all->conf->pioSD, pio_drive, TRUE); /*zzzfix --- this is to avoid the big noise during updating DSP/MFI's spi flash memory*/
        PioCommonSetPin(PIO_MUTE, pio_drive, FALSE);
        /* if (all->wiredConnected && !all->a2dpStreaming && !all->voicePromptPlaying) */
        if (all->wiredConnected && !all->a2dpStreaming && all->JinglePlayingIsFinished)
            wiredAudioUpdateVolume(same_volume);
        AUDIO_PRINT(("SND: On!\n"));
        break;

    case ALL_MGMT_BATTERY_STATUS_CHANGED:
        switch (all->batteryStatus)
        {
        case BATTERY_STATUS_LOW:
            /* not needed here, now in LED blinking heart beat, there is the trigger for battery low jingle, 2015.07.17 */
            /*
            if (all->startup)
                MessageSend(&PlayJingleTaskData, EventSysBatteryLow, 0);
            */
            break;
        case BATTERY_STATUS_FULL:
            /*zzzfix --- after discuss with Dale, decided to remove this jingle, 
              but this Jingle AAC Bin data is not removed, also the PS jingle settings
              are not changed. Later, when code memory size is not enough, do it.
              2015 June.21st*/
            /*            
            if (all->startup)
                MessageSend(&PlayJingleTaskData, EventSysChargeComplete, 0);
            */    
            break;
        default: ;
        }
        break;

    case ALL_MGMT_SELECT_I2S:
        /* HD only; called within 3 seconds after boot up */
        PioCommonSetPin(PIO_I2S_SEL, pio_drive, TRUE);
        PioCommonSetPin(PIO_SPI_SEL, pio_drive, FALSE); 
        DFI_PRINT(("I2S Selected\n"));
        break;
    }
}


static void SetDspVolume(uint16 volume)
{
    all->volume = volume;
}


void AllPowerOn(void)
{
    /*zzzfix --- for XS DC plug strange things: always updateDSP, no device on jingle on powering up ...*/
    /*zzz: maybe for some reason, "if (all->startup)return;" makes power on not really power on ... ???*/
    if (all->startup)
        return;
    all->startup = ALL_APP_POWERED_ON;
    all->seconds = 0;
    PioCommonSetPin(all->conf->pioSD, pio_drive, TRUE);
    SetDspVolume(FIXED_MUSIC_VOLUME);
    EnableDsp();
    MessageSend(&IndicatorLEDsTask, 0, NULL);
    AllResetDock();
}


void AllPowerOff(void)
{
    wiredAudioDisconnect(); /* without this the app crashes somewhere in lib code */
    disconnectAllA2dpAvrcp(TRUE); /* otherwise the power-off jingle is skipped */
    LedConfigure(all->conf->powerOffLed, LED_ENABLE, TRUE); /* will be switched off during startup */
    LedConfigure(all->conf->lineInLed, LED_ENABLE, FALSE);

    /* if it is Bull, blue LED pwm should be ON, and duty cycle should be full  zzzfix 20150530*/
    if(IsBull())
        LedConfigure(all->conf->bluetoothLed, LED_DUTY_CYCLE, 0xfff);
    /* if it is not Bull, LED off, PWM stop, signal level is low */
    LedConfigure(all->conf->bluetoothLed, LED_ENABLE, IsBull());
    
    MessageCancelAll(&IndicatorLEDsTask, 0);
    all->startup = ALL_APP_SHUTDOWN_JINGLE_POSTED;
    MessageSendLater(&PlayJingleTaskData, EventUsrPowerOff, 0, 1000);
    MessageSendLater(&AllManagementTask, ALL_MGMT_POWER_OFF, 0, 2000);
}


void AllMute(void)
{
    /* if (!all->voicePromptPlaying) */
    if (all->JinglePlayingIsFinished)
    {
        AUDIO_PRINT(("SND: Off\n"));
        MessageCancelAll(&AllManagementTask, ALL_MGMT_UNMUTE);
        PioCommonSetPin(PIO_MUTE, pio_drive, TRUE);
        all->IsAlreadyUnMuted = FALSE;
    }
}


void AllUnmute(bool now)
{
    if(!NowInUpdatingDspMfi) PioCommonSetPin(all->conf->pioSD, pio_drive, TRUE); /*zzzfix --- this is to avoid the big noise during updating DSP/MFI's spi flash memory*/

    if(!(all->IsAlreadyUnMuted))
    {   
        all->IsAlreadyUnMuted = TRUE;
    
        if (now)
            MessageSend(&AllManagementTask, ALL_MGMT_UNMUTE, 0);
        else
        {
            PioCommonSetPin(PIO_MUTE, pio_drive, TRUE);
            MessageCancelAll(&AllManagementTask, ALL_MGMT_UNMUTE);
            MessageSendLater(&AllManagementTask, ALL_MGMT_UNMUTE, 0, UNMUTE_DELAY);
            AUDIO_PRINT(("SND: On...\n"));
        }
    }
}


void AllShowBatteryLevel()
{
    if (IsSkullHdBull() || all->dcOn)
        return;
    MessageCancelAll(&AllManagementTask, ALL_MGMT_BATTERY_LEDS_OFF);
    CheckVoltageLevels();
    SetBatteryLEDs();
    MessageSendLater(&AllManagementTask, ALL_MGMT_BATTERY_LEDS_OFF, 0, 5000);
}


void AllReboot()
{
    configManagerDefragCheck();
    BootSetMode(BootGetMode());
}


void AllA2dpConnectedNotify(bool connected, TwsConnectionType tws)
{
    /* TODO: the tws parameter is wrong, it doesn't distinguish master/slave */
	if (tws == TWS_NONE)
    {
		all->a2dpConnected = connected;
    }
    else
    {   /*if it is TWS connected, should go into paring */
        
        /*if any peer_device[1,or 2] is 1,means that the host(mobile deivces) connected to this bluetooth device
          ,so it can't enter pairing mode in this states*/
        if(((theSink.a2dp_link_data->peer_device[0] == 1) || (theSink.a2dp_link_data->peer_device[1] == 1))
            && (deviceManagerNumConnectedDevs() == MAX_A2DP_CONNECTIONS))
        {
            /*now in TWS, and connected device is cell phone and TWS other side, it is the cellphone wants to establish A2DP, so no need to getting in to paring, */
        }  
        else
        {
            /*now in TWS, and connected device is TWS other side only, here means TWS just connected, so need to getting in to paring, */
             MessageCancelAll (&theSink.task, EventUsrEnterPairing);
             MessageSendLater (&theSink.task, EventUsrEnterPairing , 0 , 1600 ); /*make sure getting into paring mode after TWS connected */
        }   
    }
    if (connected)
        MessageSend(&PlayJingleTaskData, EventSysA2dpConnected, 0);
}


void AllA2dpStreamingNotify(bool streaming)
{
    all->a2dpStreaming = streaming;
    AUDIO_PRINT(("A2DP STM: %s\n", streaming ? "on" : "off"));
}


void AllWiredConnectedNotify(bool connected)
{
    /* This is detected by us in CheckWiredAudio() above, because the app
       otherwise doesn't handle the wired plug events */
    all->wiredConnected = connected;
    if (connected)
    {
        AUDIO_PRINT(("WIRED: on\n"));
        /* if (!all->a2dpStreaming && !all->voicePromptPlaying) */
        if (!all->a2dpStreaming && all->JinglePlayingIsFinished)
            audioHandleRouting(audio_source_ANALOG);
    }
    else
    {
        wiredAudioDisconnect();
        AUDIO_PRINT(("WIRED: off\n"));
    }
}


void AllDockStreamingNotify(bool connected)
{
    /* This is detected by us in CheckDock() above */
    all->dockConnected = connected;
    AUDIO_PRINT(("DOCK: %s\n", connected ? "on" : "off"));
    if (connected)
    {
        if (stateManagerIsConnected())
        {
            sinkDisconnectAllSlc();
            disconnectAllA2dpAvrcp(FALSE);
        }
    }
}


void AllVoicePromptNotify(bool playing)
{
    all->voicePromptPlaying = playing;
    /* SetDspVolume(playing ? FixedJingleVolume() : FIXED_MUSIC_VOLUME); */
    SetDspVolume(FIXED_MUSIC_VOLUME);
    /* zzzfix 20150530, really 20dB down??? ---yes! even 20dB down seems too loud for HD,
	   for XS, 12dB down is OK ...
	   for HD, 20dB down is OK ...
       Following is from Hovik,
Please note that in the coming release the gain in the volume files
will be reduced by 20dB. This is because previously we were using two
levels for the master volume: 16 for music and 8 for the jingles, but
this caused problems due to slowness of the processor (sudden very
loud jingles) and we decided to reduce the gain in the files
themselves and use one master volume value for everything. In fact
files with reduced gain compress better and take slightly less space.
   */
    
    AUDIO_PRINT(("JINGLE: %s\n", playing ? "on" : "off"));
}


void AllTwsNotify(TwsStatus status)
{
	all->twsStatus = status;
	switch (status)
	{
	case TWS_STATUS_OFF:
		MessageSend(&PlayJingleTaskData, EventUsrPeerSessionEnd, 0);
		break;
	case TWS_STATUS_CONNECTED: /* currently unused */
		break;
	case TWS_STATUS_DISC:
		MessageSend(&PlayJingleTaskData, EventUsrPeerSessionConnDisc, 0);
		break;
	case TWS_STATUS_INQ:
		MessageSend(&PlayJingleTaskData, EventUsrPeerSessionInquire, 0);
		break;
	}
}


static RemoteEvent TranslateToRemoteEventMask(sinkEvents_t event)
{
    switch (event)
    {
    case EventUsrAvrcpSkipForward:      return REMOTE_NEXT;
    case EventUsrAvrcpSkipBackward:     return REMOTE_PREV;
    case EventUsrAvrcpPlayPause:        return REMOTE_PLAY_PAUSE;
    case EventUsrBassBoostEnableDisableToggle:
                                        return REMOTE_BASS_BOOST;
    case EventUsrVolumeUp:              return REMOTE_VOLUME_UP;
    case EventUsrVolumeDown:            return REMOTE_VOLUME_DOWN;
    case EventUsrMuteToggle:            return REMOTE_MUTE;
    default:                            return REMOTE_NONE;
    }
}


void AllSinkEventNotify(sinkEvents_t event)
{
    if (IsSkullHdBull())
    {
        RemoteEvent eventMask = TranslateToRemoteEventMask(event);
        if (eventMask)
        {
            /* printf("Event: %04x  Remote: %d\n", event, eventMask); */
            all->remoteEventMask |= eventMask;
        }
    }
}


bool AllOkToSleep(void)
{
#if ENABLE_DEEP_SLEEP
    return a ll && !all->dcOn;
#else
    return FALSE;   /*zzztemp: deep sleep is disabled, XS will never go into real physical power down */
#endif
}


void AllSleepNotify()
{
	all->startup = ALL_APP_POWERED_OFF;
    PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
    MessageCancelAll(&HeartbeatTaskData, 0);
    MessageCancelAll(&SleepHeartbeatTaskData, 0);
    MessageCancelAll(&NoDspHeartbeatTaskData, 0);
    DisableDsp();
    if (IsSkullXsTwist())
        PioSetDir32(_MASK(PIO_DC), 0);
}


uint32 AllDeviceClass()
{
    return AUDIO_MAJOR_SERV_CLASS | AV_COD_RENDER | AV_MAJOR_DEVICE_CLASS | AV_MINOR_SPEAKER;
}


void AllVolumeNotify(uint16 volume)
{
    /* Here we receive the volume in the range 0..16, where 0 means mute */
    if (volume >= 15 || volume == 0)
        all->whiteCountdown = 10;
    all->dockVolume = volume;
    VOLUME_PRINT(("Volume: %d\n", volume));
}


static void InitData(void)
{
    if (all)
        return;
    all = PanicUnlessMalloc(sizeof(AllData));
    memset(all, 0, sizeof(AllData));
    all->dsp = DSP_STATUS_OFF;
    all->productId = ALL_PRODUCT_UNKNOWN;
    all->volume = VOLUME_RANGE;
    all->batteryLevel = BATTERY_VOLTAGE_MIN;
    all->batteryStatus = BATTERY_STATUS_OK;

    all->serialSent = (uint8)sizeof(all->serial) - 1;
    sprintf(all->serial, "%02x%06lx", theSink.local_bd_addr.uap, theSink.local_bd_addr.lap);
    all->aeroVersionSent = (uint8)sizeof(all->aeroVersion) - 1;
    sprintf(all->aeroVersion, "%8s", VERSION_APP_FULL_STR);
    NowInUpdatingDspMfi=0;  /*zzzfix*/
    NoDCPlugged_BatLowJingleControlCnt = 0; /*zzzfix*/
    all->batteryLevel=6601;  /* zzzfix, no to blink red at powering up */
    all->CurrentA2dpAudioDecoderUsed=DecoderType_NotSure;
    all->IsAlreadyUnMuted = FALSE;
    all->JinglePlayingIsFinished = FALSE;   /*at the beginning, device on is always playing, so jingleplaying is NOT finished at the beginnning*/
}


static char* GetLocalDeviceName(char deviceName[MAX_DEVICE_NAME + 1])
{
    uint16 deviceNamePacked[MAX_DEVICE_NAME / 2];
    size_t deviceNamePackedLen = 0;
    if ((deviceNamePackedLen = PsFullRetrieve(PSKEY_DEVICE_NAME, deviceNamePacked, sizeof(deviceNamePacked) - 1)) > 0)
    {
        const uint16* pw = deviceNamePacked;
        char* pc = deviceName;
        while (deviceNamePackedLen--)
        {
            *pc++ = *pw & 0xff;
            *pc++ = *pw++ >> 8;
        }
        *pc = 0;
    }
    else
        strcpy(deviceName, "<Unnamed>");
    return deviceName;
}



/* ************************************************************************* */
/* CONFIGURATION *********************************************************** */
/* ************************************************************************* */


#define PIO_SD_OTHERS 15
#define PIO_SD_SKULL_HD 22
#define PIO_TRST_OTHERS 28
#define PIO_TRST_SKULL_HD 4
#define PIO_SPI_MOSI 25
#define PIO_MFI_MOSI 14

#define PIO_EECS 23
#define PIO_MFI_EECS 5


#define DFI_PIO_OTHERS (_MASK(PIO_ARST)|_MASK(PIO_EECS)|_MASK(PIO_SPI_MOSI)|_MASK(PIO_SPI_MISO)|_MASK(PIO_SPI_CLK)|_MASK(PIO_SD_OTHERS)|_MASK(PIO_NFC_SESSION)|_MASK(PIO_VDDCORE_SKULL_XS)|_MASK(PIO_TRST_OTHERS))
#define DFI_PIO_SKULL_HD (_MASK(PIO_ARST)|_MASK(PIO_EECS)|_MASK(PIO_SPI_MOSI)|_MASK(PIO_SPI_MISO)|_MASK(PIO_SPI_CLK)|_MASK(PIO_SD_SKULL_HD)|_MASK(PIO_NFC_SESSION)|_MASK(PIO_SPI_SEL))
#define MFIDFI_PIO (_MASK(PIO_ARST)|_MASK(PIO_SPI_SEL)|_MASK(PIO_MFI_MOSI)|_MASK(PIO_SPI_MISO)|_MASK(PIO_SPI_CLK)|_MASK(PIO_SD_SKULL_HD)|_MASK(PIO_NFC_SESSION))
#define USED_PIO_OTHERS (_MASK(PIO_ARST)|_MASK(PIO_TRST_OTHERS))
#define USED_PIO_SKULL_HD (_MASK(PIO_ARST)|_MASK(PIO_SPI_SEL)|_MASK(PIO_SD_SKULL_HD))


static const ProductConfig skullHdBullConfig =
{
    USED_PIO_SKULL_HD,      /* usedPios */
    PIO_SD_SKULL_HD,        /* pioSD */
    PIO_TRST_SKULL_HD,      /* pioTRST */
    LED_1,                  /* powerOffLed = red */
    LED_0,                  /* bluetoothLed = blue */
    LED_2,                  /* lineInLed = white */
    {                       /* dsp */
        {                       /* dsp[DSP_MAIN] */
            DFI_PIO_SKULL_HD,       /* dfiUsedPios */
            PIO_SPI_MOSI,           /* pioMOSI */
            PIO_EECS,               /* pioEECS */
            "dsp.dfi"               /* dfiFileName */
        },
        {                       /* dsp[DSP_SECONDARY] */
            MFIDFI_PIO,             /* dfiUsedPios */
            PIO_MFI_MOSI,           /* pioMOSI */
            PIO_MFI_EECS,           /* pioEECS */
            "dsp2.dfi"              /* dfiFileName */
        }
    }
};


static const ProductConfig twistConfig =
{
    USED_PIO_OTHERS,        /* usedPios */
    PIO_SD_OTHERS,          /* pioSD */
    PIO_TRST_OTHERS,        /* pioTRST */
    LED_0,                  /* powerOffLed = red */
    LED_1,                  /* bluetoothLed = blue */
    LED_2,                  /* lineInLed = white */
    {                       /* dsp */
        {                       /* dsp[DSP_MAIN] */
            DFI_PIO_OTHERS,         /* dfiUsedPios */
            PIO_SPI_MOSI,           /* pioMOSI */
            PIO_EECS,               /* pioEECS */
            "dsp.dfi"               /* dfiFileName */
        }
    }
};


/* Other devices: currently XS_1, XS_2 and Twist */
static const ProductConfig otherConfig =
{
    USED_PIO_OTHERS,        /* usedPios */
    PIO_SD_OTHERS,          /* pioSD */
    PIO_TRST_OTHERS,        /* pioTRST */
    LED_1,                  /* powerOffLed = red */
    LED_0,                  /* bluetoothLed = blue */
    LED_2,                  /* lineInLed = white */
    {                       /* dsp */
        {                       /* dsp[DSP_MAIN] */
            DFI_PIO_OTHERS,         /* dfiUsedPios */
            PIO_SPI_MOSI,           /* pioMOSI */
            PIO_EECS,               /* pioEECS */
            "dsp.dfi"               /* dfiFileName */
        }
    }
};


Version ExpectedDfiVersion()
{
    const static Version xs1 = {3, 7, 17};
    const static Version xs2 = {3, 7, 13};
    const static Version hd = {3, 7, 19};
    const static Version bull = {3, 7, 21};
    const static Version twist = {3, 7, 17};
    const static Version other = {0, 0, 0};
    if (all->productId == ALL_PRODUCT_SKULL_XS_1)
        return xs1;
    if (all->productId == ALL_PRODUCT_TWIST)
        return twist;
    if (all->productId == ALL_PRODUCT_SKULL_XS_2)
        return xs2;
    if (all->productId == ALL_PRODUCT_SKULL_HD)
        return hd;
    if (all->productId == ALL_PRODUCT_BULL)
        return bull;
    Panic();
    return other;
}


Version ExpectedMfiVersion()
{
    const static Version hd = {1, 2, 4};
    const static Version bull = {1, 2, 4};
    const static Version other = {0, 0, 0};
    if (all->productId == ALL_PRODUCT_SKULL_HD)
        return hd;
    if (all->productId == ALL_PRODUCT_BULL)
        return bull;
    return other;
}


void AllConfigInit()
{
    InitData();

    {
        uint16 deviceId = 0;
        if (!PsFullRetrieve(PSKEY_USB_BCD_DEVICE, &deviceId, sizeof(deviceId)))
            deviceId = ALL_PRODUCT_SKULL_XS_1;
        all->productId = deviceId & 0x1fff;
        all->sleepless = (deviceId & 0x2000) != 0;
    }

    all->conf = IsSkullHdBull() ? &skullHdBullConfig :
        (IsTwist() ? &twistConfig : &otherConfig);

    RetrieveDspVersionInfo();

    /* Connect dummy source to keep the I2S clock going */
    AllConnect(ALL_MUSIC_SAMPLING_RATE, TRUE,
        StreamKalimbaSource(0 /* AUDIO_OUT_FROM_DSP_LEFT */),
        StreamKalimbaSource(1 /* AUDIO_OUT_FROM_DSP_RIGHT */));
    AllMute();

    PioSetMapPins32(all->conf->usedPios, all->conf->usedPios); /* take control of PIO pins */
    PioSetMapPins32(_MASK(all->conf->pioSD), _MASK(all->conf->pioSD));
    PioSetDir32(all->conf->usedPios, FALSE); /* set all used PIOs as inputs (high-Z) */
    PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
    PioCommonSetPin(PIO_NFC_SESSION, pio_pull, TRUE);
    PioCommonSetPin(PIO_MUTE, pio_drive, FALSE);

    DisableDsp();

    {
        char buf[MAX_DEVICE_NAME + 1];
        if (IsSkullXsTwist())
            PioCommonSetPin(PIO_3V3_SKULL_XS, pio_drive, EN_3V3);
        AllNFCInit(GetLocalDeviceName(buf), &theSink.local_bd_addr);
        if (IsSkullXsTwist())
            PioCommonSetPin(PIO_3V3_SKULL_XS, pio_drive, DIS_3V3);
    }

    CheckDC();
    MessageSend(&HeartbeatTaskData, 0, 0);
}



/*

test result for EXE update, and DSP update, DC plugged/unplugged  
  
ON XS:
    1. When doing EXE update, DC is plugged, XS is ON
               after EXE update finishes, DSP updating immediately starts --- --- this is pass!
               
    2. When doing EXE update, DC is plugged, XS is OFF
               after EXE update finishes, DSP updating immediately starts --- --- this is pass!

    3. When doing EXE update, DC is unplugged, XS is ON
               after EXE update finishes, plug DC, DSP updating immediately starts --- --- this is pass!
               after EXE update finishes, turn off XS, plug DC, DSP updating immediately starts --- --- this is pass!

    4. When doing EXE update, DC is unplugged, XS is OFF
               after EXE update finishes, plug DC, DSP updating immediately starts --- --- this is pass!
               after EXE update finishes, turn on XS, plug DC, DSP updating immediately starts --- --- this is pass!

               
When XS power is OFF, plug DC, unplug DC, will not impact power on. --- --- tested and this is pass!               
               
               
               
*/



