
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pio.h>
#include <led.h>
#include <file.h>
#include <panic.h>
#include <message.h>

#include "pio_common.h"
#include "alldsp.h"
#include "allcomm.h"


#define xDEBUG_I2C
#ifdef DEBUG_I2C
#   define I2C_PRINT(x) printf x
#else
#   define I2C_PRINT(x)
#endif


/*
 *   ISO 11785 CRC
 */

#define InitCrc() 0x6363


void UpdateCrc(uint16* crc, uint8 ch)
{
    ch = (ch ^ *crc) & 0xff;
    ch = (ch ^ (ch << 4)) & 0xff;
    *crc = (*crc >> 8) ^ (ch << 8) ^ (ch << 3) ^ (ch >> 4);
}


/*
 *   I2C low level routines
 */


static void SetSDA(int f)
{
    PioSetDir32(_MASK(PIO_SDA), _MASK(PIO_SDA));
    PioSet32(_MASK(PIO_SDA), f ? _MASK(PIO_SDA) : 0);
}


static int GetSDA(void)
{
    PioSetDir32(_MASK(PIO_SDA), 0);
    return (PioGet32() & _MASK(PIO_SDA)) ? 1 : 0;
}


static void SetSCL(int f)
{
    PioSetDir32(_MASK(PIO_SCL), _MASK(PIO_SCL));
    PioSet32(_MASK(PIO_SCL), f ? _MASK(PIO_SCL) : 0);
}


void I2cStart(bool reqOrRsp)
{
    dly(200);
    I2C_PRINT((reqOrRsp ? "REQ: " : "RSP: "));
    SetSDA(1);
    SetSCL(1);
    SetSDA(0);
    SetSCL(0);
}


void I2cStop(void)
{
    SetSDA(0);
    SetSCL(1);
    SetSDA(1);
    I2C_PRINT(("\n"));
    dly(200);
}


static void TrxBit(int b)
{
    SetSDA(b);
    SetSCL(1);
    SetSCL(0);
}


static int RcvBit(void)
{
    int b;
    GetSDA(); /* important! should be before raising the clock, doesn't work otherwise */
    SetSCL(1);
    b = GetSDA();
    SetSCL(0);
    return b;
}


void I2cTrx(int d)
{
    int ack;
    int i;
    I2C_PRINT(("%02x", d & 0xff));
    for (i = 8; i; i--)
    {
        TrxBit(d & 0x80);
        d <<= 1;
    }
    SetSDA(1);
    ack = RcvBit();
    dly(10);
    I2C_PRINT(("/%d ", ack));
}


int I2cRcv(void)
{
    int i, d = 0;
    SetSDA(1);
    for (i = 0; i < 8; i++)
    {
        d <<= 1;
        /* NOTE: SCL clock stretching is not implemented as we are likely
           slower than the other guy */
        d |= RcvBit();
    }
    TrxBit(0); /* ack */
    SetSDA(1);
    I2C_PRINT(("%02x ", d));
    dly(10);
    return d;
}



/*
 *   LEDs
 */


#define _BAT_LED_COUNT 5
#define _PIO_BAT_0 1
#define _PIO_BAT_1 2
#define _PIO_BAT_2 9
#define _PIO_BAT_3 8
#define _PIO_BAT_4 5


static const int LED_TO_PIN[_BAT_LED_COUNT] =
    { _PIO_BAT_0, _PIO_BAT_1, _PIO_BAT_2, _PIO_BAT_3, _PIO_BAT_4 };

void ShowBatteryLevel(int level, bool blink_last)
{
    if (IsSkullXsTwist())
    {
        int i;
        for (i = 0; i < _BAT_LED_COUNT; i++)
            if (blink_last && i == level)
                PioCommonSetPin(LED_TO_PIN[i], pio_drive, all->seconds & 1);
            else
                PioCommonSetPin(LED_TO_PIN[i], pio_drive, !(i < level));
    }
}


void ShowProgress(int level)
{
    if (IsSkullXsTwist())
    {
        int i;
        for (i = 0; i < _BAT_LED_COUNT; i++)
            PioCommonSetPin(LED_TO_PIN[_BAT_LED_COUNT - i - 1], pio_drive, !(i < level));
    }
    else if (IsSkullHdBull())
    {
        LedConfigure(PROGRESS_LED_HD, LED_DUTY_CYCLE, 0x0800);
        LedConfigure(PROGRESS_LED_HD, LED_ENABLE, level % 2);
    }
}



/*
 *   DFI update
 */


#define DFI_LOOP_DELAY 10
#define DFI_MANUF_CODE_RETRY_DELAY 100
#define DFI_ERASE_COMPLETE_DELAY 100
#define FLASH_ERASE_TIMEOUT 10000 /* ms worst case chip erase time */
#define DFI_MAX_SECTOR_RETRIES 3

#define IGNORE_READ_CRC (IsSkullXsTwist())

#define FLASH_PAGE_SIZE 256
#define FLASH_PAGES_PER_SECTOR 16

#define FLASH_CMD_ERASE 0xc7
#define FLASH_CMD_ERASE_SECTOR 0x20
#define FLASH_CMD_WREN 0x06
#define FLASH_CMD_WRITE 0x02
#define FLASH_CMD_READ 0x03
#define FLASH_CMD_READ_SR 0x05
#define FLASH_CMD_READ_MAN_CODE 0x90


enum
{
    DFI_OK,
    DFI_FLASH_MANUF_CODE_FAILED,
    DFI_DSP_TIMED_OUT,
    DFI_FLASH_VERIFY_FAILED
};


#define delay(n) { uint32 i = (uint32)n * 100; while (i--); }


void DSPWrite(const uint8* p, size_t len)
{
    PioSetDir32(_MASK(PIO_SCL), _MASK(PIO_SCL));
    PioSetDir32(_MASK(PIO_SDA), _MASK(PIO_SDA));
    for (; len; len--, p++)
    {
        int i = 0;
        for (i = 0; i < 8; i++)
        {
            if (*p & (1 << i))
                PioSet32(_MASK(PIO_SCL) | _MASK(PIO_SDA), _MASK(PIO_SCL) | _MASK(PIO_SDA));
            else
                PioSet32(_MASK(PIO_SCL) | _MASK(PIO_SDA), _MASK(PIO_SCL));
            PioSet32(_MASK(PIO_SCL), FALSE);
        }
    }
}


void DSPRead(uint8* p, size_t len)
{
    PioSetDir32(_MASK(PIO_SDA), 0);
    for (; len; len--, p++)
    {
        int i;
        *p = 0;
        for (i = 0; i < 8; i++)
        {
            PioSet32(_MASK(PIO_SCL), _MASK(PIO_SCL));
            PioSet32(_MASK(PIO_SCL), 0);
            if (PioGet32() & _MASK(PIO_SDA))
                *p |= (1 << i);
        }
    }
    PioSetDir32(_MASK(PIO_SDA), _MASK(PIO_SDA));
}


static int SPIWrite(uint32 pioMosiMask, int data)
{
    int i;
    for (i = 7; i >= 0; i--)
    {
        if (data & (1 << i))
            PioSet32(_MASK(PIO_SPI_CLK) | pioMosiMask, pioMosiMask);
        else
            PioSet32(_MASK(PIO_SPI_CLK) | pioMosiMask, 0);
        PioSet32(_MASK(PIO_SPI_CLK), _MASK(PIO_SPI_CLK));
    }
    return data;
}


static int SPIRead(void)
{
    int i, data = 0;
    for (i = 7; i >= 0; i--)
    {
        PioSet32(_MASK(PIO_SPI_CLK), FALSE);
        PioSet32(_MASK(PIO_SPI_CLK), _MASK(PIO_SPI_CLK));
        if (PioGet32() & _MASK(PIO_SPI_MISO))
            data |= (1 << i);
    }
    return data;
}


static FILE_INDEX DfiFileIndex(DspIndex index)
{
    const char* n = all->conf->dsp[index].dfiFileName;
    return FileFind(FILE_ROOT, n, strlen(n));
}


static Source LoadDfiAsSource(DspIndex index)
{
    Source source = StreamFileSource(DfiFileIndex(index));
    PanicNull(source);
    return source;
}


#define CHUNK_SIZE 0xc00 /* From SourceSize() documentation */
#if CHUNK_SIZE < FLASH_PAGE_SIZE
#   error Something is terribly wrong with CHUNK_SIZE and FLASH_PAGE_SIZE
#endif

static uint16 SkipDfiToOffset(Source dfi_source, uint32 start)
{
    uint16 size;
    /* SourceDrop() can only skip at most CHUNK_SIZE bytes, thus the loop: */
    for (; start >= CHUNK_SIZE; start -= CHUNK_SIZE)
        SourceDrop(dfi_source, CHUNK_SIZE);
    SourceDrop(dfi_source, start);
    size = SourceSize(dfi_source);
    if (size > FLASH_PAGE_SIZE)
        size = FLASH_PAGE_SIZE;
    return size;
}


static void FlashCommandWithRsp(DspIndex index, int cmd, uint8* rsp, size_t rsplen)
{
    uint32 pioMosiMask = _MASK(all->conf->dsp[index].pioMOSI);
    uint32 pioEecsMask = _MASK(all->conf->dsp[index].pioEECS);
    PioSet32(pioEecsMask, FALSE); /* select flash */
    SPIWrite(pioMosiMask, cmd);
    while (rsplen--)
        *rsp++ = SPIRead();
    PioSet32(pioEecsMask, pioEecsMask); /* de-select flash */
}


static void FlashAddrCommandBegin(DspIndex index, int cmd, uint32 address)
{
    uint32 pioMosiMask = _MASK(all->conf->dsp[index].pioMOSI);
    uint32 pioEecsMask = _MASK(all->conf->dsp[index].pioEECS);
    PioSet32(pioEecsMask, FALSE); /* select flash */
    SPIWrite(pioMosiMask, cmd);
    SPIWrite(pioMosiMask, (uint8)(address >> 16));
    SPIWrite(pioMosiMask, (uint8)(address >> 8));
    SPIWrite(pioMosiMask, (uint8)address);
}


static void FlashAddrCommandEnd(DspIndex index)
{
    uint32 pioEecsMask = _MASK(all->conf->dsp[index].pioEECS);
    PioSet32(pioEecsMask, pioEecsMask); /* de-select flash */
}


static void DfiEnableMaster(DspIndex index)
{
    uint32 usedPios = all->conf->dsp[index].dfiUsedPios;
    int pioMosi = all->conf->dsp[index].pioMOSI;
    uint32 pioEecsMask = _MASK(all->conf->dsp[index].pioEECS);
    int pioTrst = all->conf->pioTRST;
    uint32 pioTrstMask = _MASK(pioTrst);
    uint32 pioMosiMask = _MASK(pioMosi);
    DFI_PRINT(("DFI: flash enable master\n"));

    PioSetMapPins32(usedPios, usedPios); /* take control of PIO pins */
    PioSetDir32(usedPios, FALSE); /*set all used PIOs as inputs (high-Z)*/

    if (index == DSP_MAIN)
    {
        PioCommonSetPin(PIO_ARST, pio_drive, FALSE); /*set DSP in reset*/
        PioCommonSetPin(pioTrst, pio_drive, FALSE); /*set DSP in reset*/
        if (IsSkullXsTwist())
            PioCommonSetPin(PIO_3V3_SKULL_XS, pio_drive, EN_3V3);
        else
        {
            PioSet32(_MASK(PIO_I2S_SEL), FALSE); /*disconnect I2S*/
            PioSet32(_MASK(PIO_SPI_SEL), FALSE); /*disconnect SPI from second flash*/
        }
        PioSetDir32(pioMosiMask | pioEecsMask | _MASK(PIO_SPI_CLK) | _MASK(PIO_SPI_MISO),
                    pioMosiMask | pioEecsMask | _MASK(PIO_SPI_CLK)); /* set port directions for SPI master */
        PioSet32(pioEecsMask | _MASK(PIO_SPI_CLK),
                 pioEecsMask | _MASK(PIO_SPI_CLK)); /*force SPI mode 3, CLK and EE_CS normally high*/
    }

    else
    {
        PioCommonSetPin(pioMosi, pio_drive, TRUE);
        PioSetDir32(_MASK(PIO_ARST) | pioTrstMask | _MASK(PIO_I2S_SEL) | _MASK(PIO_SPI_SEL) | pioEecsMask | pioMosiMask | _MASK(PIO_SPI_CLK) | _MASK(PIO_SPI_MISO),
                    _MASK(PIO_ARST) | pioTrstMask | _MASK(PIO_I2S_SEL) | _MASK(PIO_SPI_SEL) | pioEecsMask | pioMosiMask | _MASK(PIO_SPI_CLK)); /*set port directions for SPI master*/
        PioSetDir32(_MASK(PIO_ARST) | pioTrstMask,
                    _MASK(PIO_ARST) | pioTrstMask); /*set port directions for SPI master*/
        PioSet32(pioTrstMask, FALSE); /*set DSP in reset*/
        PioCommonSetPin(pioTrst, pio_drive, FALSE); /*set DSP in reset*/
        PioSet32(_MASK(PIO_ARST), FALSE); /*set DSP in reset*/
        PioCommonSetPin(PIO_ARST, pio_drive, FALSE); /*set DSP in reset*/
        PioSet32(_MASK(PIO_I2S_SEL), FALSE); /*disconnect I2S*/
        PioSet32(_MASK(PIO_SPI_SEL), _MASK(PIO_SPI_SEL)); /*connect SPI*/
        PioSetMapPins32(pioMosiMask, pioMosiMask); /*take control of PIO pins*/
        PioSet32(pioMosiMask, pioMosiMask); /*set data line high*/
        PioSet32(pioEecsMask | _MASK(PIO_SPI_CLK),
                 pioEecsMask | _MASK(PIO_SPI_CLK)); /*force SPI mode 3, CLK and EE_CS normally high*/
    }
}


static void DfiFlashDisable(DspIndex index)
{
    int pioMosi = all->conf->dsp[index].pioMOSI;
    uint32 pioEecsMask = _MASK(all->conf->dsp[index].pioEECS);
    int pioTrst = all->conf->pioTRST;
    uint32 pioMosiMask = _MASK(pioMosi);
    DFI_PRINT(("DFI: flash disable\n"));
    PioSetDir32(pioMosiMask | pioEecsMask | _MASK(PIO_SPI_MISO) | _MASK(PIO_SPI_CLK), pioEecsMask);
    PioSetMapPins32(all->conf->dsp[index].dfiUsedPios, all->conf->usedPios); /*take control of PIO pins*/
    if (index == DSP_MAIN)
    {
        PioSet32(pioEecsMask, pioEecsMask); /*CS pull-up*/
        PioCommonSetPin(PIO_ARST, pio_pull, TRUE); /*release reset*/
        PioCommonSetPin(pioTrst, pio_pull, TRUE); /*release reset*/
    }
    else
    {
        PioSetDir32(_MASK(PIO_ARST), FALSE); /*release DSP from reset*/
        PioSetDir32(_MASK(pioTrst), FALSE); /*release DSP from reset*/
        PioSet32(pioEecsMask, pioEecsMask); /*CS pull-up*/
        PioSet32(_MASK(PIO_SPI_SEL), FALSE); /*disconnect SPI*/
    }
}


static bool DfiCheckManufCode(DspIndex index)
{
    uint8 data[5];
    FlashCommandWithRsp(index, FLASH_CMD_READ_MAN_CODE, data, sizeof(data));
    DFI_PRINT(("Flash Manufacturer Code [%d] = 0x%02x%02x\n", index, data[3], data[4]));
    return ((data[3] == 0x1c || data[3] == 0x18) && (data[4] == 0x13 || data[4] == 0x08));
}


static void DfiSetWren(DspIndex index)
{
    DFI_PRINT(("DFI: flash set write enable\n"));
    FlashCommandWithRsp(index, FLASH_CMD_WREN, NULL, 0);
}


static void DfiEraseSector(DspIndex index)
{
    DFI_PRINT(("DFI: flash erase sector at page %d\n", all->flashPage));
    FlashAddrCommandBegin(index, FLASH_CMD_ERASE_SECTOR, (uint32)all->flashPage * FLASH_PAGE_SIZE);
    FlashAddrCommandEnd(index);
}


static bool DfiEraseComplete(DspIndex index)
{
    uint8 statusReg;
    FlashCommandWithRsp(index, FLASH_CMD_READ_SR, &statusReg, 1);
    DFI_PRINT(("DFI: flash read status register: 0x%02x\n", statusReg));
    return statusReg == 0;
}


static void DfiWritePage(DspIndex index)
{
    uint32 start = (uint32)all->flashPage * FLASH_PAGE_SIZE;
    uint32 pioMosiMask = _MASK(all->conf->dsp[index].pioMOSI);
    Source dfi_source = LoadDfiAsSource(index);

    all->pageSize = SkipDfiToOffset(dfi_source, start);

    DFI_PRINT(("DFI: flash write page %d (%d)\n", all->flashPage, all->pageSize));

    if (all->pageSize)
    {
        const uint8* dfi_data = SourceMap(dfi_source);
        uint16 i;
        all->pageCrc = InitCrc();
        FlashAddrCommandBegin(index, FLASH_CMD_WRITE, start);
        DFI_PRINT(("WR >>> "));
        for (i = 0; i < all->pageSize; i++)
        {
            uint8 ch = dfi_data[i];
            if (i < 64) DFI_PRINT(("%02x ", ch));
            UpdateCrc(&all->pageCrc, ch);
            SPIWrite(pioMosiMask, ch);
        }
        DFI_PRINT(("\n"));
        FlashAddrCommandEnd(index);
    }

    SourceClose(dfi_source);
}


static bool DfiVerifyPage(DspIndex index)
{
    DFI_PRINT(("DFI: flash verify page %d (%d)\n", all->flashPage, all->pageSize));

    if (all->pageSize)
    {
        uint16 i;
        uint16 crc = InitCrc();
        FlashAddrCommandBegin(index, FLASH_CMD_READ, (uint32)all->flashPage * FLASH_PAGE_SIZE);
        DFI_PRINT(("RD <<< "));
        for (i = 0; i < all->pageSize; i++)
        {
            uint8 ch = SPIRead();
            if (i < 64) DFI_PRINT(("%02x ", ch));
            UpdateCrc(&crc, ch);
        }
        DFI_PRINT(("\n"));
        FlashAddrCommandEnd(index);
        return IGNORE_READ_CRC || crc == all->pageCrc;
    }
    return TRUE;
}


typedef enum
{
    DFI_START_UPGRADE,
    DFI_CHECK_MANUF_CODE,
    DFI_ERASE_SECTOR,
    DFI_WAIT_ERASE_SECTOR,
    DFI_WRITE_PAGE,
    DFI_VERIFY_PAGE,
    DFI_FINISH_UPGRADE
} DfiUpgradeStage;

uint16 NowInUpdatingDspMfi=0;  /*zzzfix --- to avoid noise during updating/writing DSP/MFI spi flash*/
static void DfiTask(Task t, MessageId id, Message payload)
{
    DspIndex index = all->dsp == DSP_STATUS_UPGRADING_SECONDARY ? DSP_SECONDARY : DSP_MAIN;

    switch ((DfiUpgradeStage)id)
    {
        case DFI_START_UPGRADE:
            NowInUpdatingDspMfi=1;  /*zzzfix*/        
            /*zzzfix --- power down AMP IC*/ PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
            all->runningTime = 0;
            all->flashPage = 0;
            ShowProgress(1);
            DfiEnableMaster(index);
            /*zzzfix --- power down AMP IC*/ PioCommonSetPin(all->conf->pioSD, pio_drive, FALSE);
            MessageSendLater(t, DFI_CHECK_MANUF_CODE, 0, DFI_LOOP_DELAY);
            break;

        case DFI_CHECK_MANUF_CODE:
            if (DfiCheckManufCode(index))
            {
                all->sectorRetryCount = 0;
                MessageSendLater(t, DFI_ERASE_SECTOR, 0, DFI_LOOP_DELAY);
            }
            else
            {
                all->runningTime += DFI_MANUF_CODE_RETRY_DELAY;
                if (all->runningTime > 5 * DFI_MANUF_CODE_RETRY_DELAY)
                    AllAbort(1);
                MessageSendLater(t, DFI_CHECK_MANUF_CODE, 0, DFI_MANUF_CODE_RETRY_DELAY);
            }
            break;

        case DFI_ERASE_SECTOR:
            DfiSetWren(index);
            DfiEraseSector(index);
            all->runningTime = 0;
            MessageSendLater(t, DFI_WAIT_ERASE_SECTOR, 0, DFI_LOOP_DELAY);
            break;

        case DFI_WAIT_ERASE_SECTOR:
            all->runningTime += DFI_LOOP_DELAY;
            ShowProgress(1 + ((all->runningTime % 100) > 50));
            if (all->runningTime > FLASH_ERASE_TIMEOUT || DfiEraseComplete(index))
                /* A longer delay after SR indicates erase is complete */
                MessageSendLater(t, DFI_WRITE_PAGE, 0, DFI_ERASE_COMPLETE_DELAY);
            else
                MessageSendLater(t, DFI_WAIT_ERASE_SECTOR, 0, DFI_LOOP_DELAY);
            break;

        case DFI_WRITE_PAGE:
            all->runningTime += DFI_LOOP_DELAY;
            ShowProgress(2 + ((all->runningTime % 50) > 25));
            DfiSetWren(index);
            DfiWritePage(index);
            MessageSendLater(t, DFI_VERIFY_PAGE, 0, DFI_LOOP_DELAY);
            break;

        case DFI_VERIFY_PAGE:
            all->runningTime += DFI_LOOP_DELAY;
            ShowProgress(2 + ((all->runningTime % 50) > 25));
            if (!DfiVerifyPage(index))
            {
                /* Page failed: restart the sector, or otherwise just abort */
                if (all->sectorRetryCount++ >= DFI_MAX_SECTOR_RETRIES)
                {
                    DFI_PRINT(("DFI: sector (page %d) reached max retries, aborting\n", all->flashPage));
                    AllAbort(2);
                }
                else
                {
                    all->flashPage = (all->flashPage / FLASH_PAGES_PER_SECTOR) * FLASH_PAGES_PER_SECTOR;
                    MessageSendLater(t, DFI_ERASE_SECTOR, 0, DFI_LOOP_DELAY);
                }
            }
            else if (all->pageSize < FLASH_PAGE_SIZE)
            {
                /* Finita la musica, passata la fiesta */
                ShowProgress(5);
                MessageSendLater(t, DFI_FINISH_UPGRADE, 0, 1000);
            }
            else
            {
                /* Next page: if at the beginning of a sector, erase it */
                all->flashPage++;
                if ((all->flashPage % FLASH_PAGES_PER_SECTOR) == 0)
                {
                    all->sectorRetryCount = 0;
                    MessageSendLater(t, DFI_ERASE_SECTOR, 0, DFI_LOOP_DELAY);
                }
                else
                    MessageSendLater(t, DFI_WRITE_PAGE, 0, DFI_LOOP_DELAY);
            }
            break;

        case DFI_FINISH_UPGRADE:
            DfiFlashDisable(index);
            /*zzzfix --- power up AMP IC*/
            /* PioCommonSetPin(all->conf->pioSD, pio_drive, TRUE); --> not
               necessary, it is going to reboot soon*/ 
            
            if (index == DSP_MAIN && all->conf->dsp[DSP_SECONDARY].dfiFileName)
            {
                all->dsp = DSP_STATUS_UPGRADING_SECONDARY;
                MessageSendLater(t, DFI_START_UPGRADE, 0, DFI_LOOP_DELAY);
            }
            else
            {
                all->dspVersion.dfi = ExpectedDfiVersion();
                all->dspVersion.mfi = ExpectedMfiVersion();
                UpdateDspVersionInfo();
                ShowProgress(0);
                NowInUpdatingDspMfi=0;  /*zzzfix*/        
                AllReboot();
            }
            break;
    }
}


static TaskData DfiTaskData = { DfiTask };


void DfiStartUpgrade()
{
    all->dsp = DSP_STATUS_UPGRADING_MAIN;
    ShowBatteryLevel(0, FALSE);
    MessageSend(&DfiTaskData, DFI_START_UPGRADE, 0);
}

