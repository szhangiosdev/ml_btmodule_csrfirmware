
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <csrtypes.h>
#include <panic.h>
#include <pio.h>
#include <pio_common.h>

#include "allcomm.h"
#include "allnfc.h"


#define xDEBUG_NFC
#ifdef DEBUG_NFC
#   define NFC_PRINT(x) printf x
#else
#   define NFC_PRINT(x)
#endif


#define FORCE_NFC_UPDATE FALSE


/*
 *   NFC low level routines
 */


static uint16 InitReq(void)
{
    I2cStart(TRUE);
    I2cTrx(0xac);
    return InitCrc();
}


static void SendReqChunk(uint16* crc, const uint8* data, size_t len)
{
    while (len--)
    {
        UpdateCrc(crc, *data);
        I2cTrx(*data++);
    }
}


static void SendReq8(uint16* crc, uint8 ch)
{
    I2cTrx(ch);
    UpdateCrc(crc, ch);
}


static void SendReq16(uint16* crc, uint16 w)
{
    SendReq8(crc, w >> 8);
    SendReq8(crc, w);
}


static void DoneReq(uint16* crc)
{
    if (crc)
    {
        I2cTrx(*crc & 0xff);
        I2cTrx(*crc >> 8);
    }
    I2cStop();
}


static uint16 InitRsp(void)
{
    I2cStart(FALSE);
    I2cTrx(0xad);
    return InitCrc();
}


static void ReceiveRspChunk(uint16* crc, uint8* data, size_t len)
{
    while (len--)
    {
        int ch = I2cRcv();
        if (data)
            *data++ = ch;
        UpdateCrc(crc, ch);
    }
}


static uint8 ReceiveRsp8(uint16* crc)
{
    int ch = I2cRcv();
    UpdateCrc(crc, ch);
    return ch;
}


static bool DoneRsp(uint16* crc)
{
    uint8 rspCrc[2];
    if (crc)
    {
        rspCrc[0] = I2cRcv();
        rspCrc[1] = I2cRcv();
    }
    I2cStop();
    return !crc || *crc == (rspCrc[0] | (rspCrc[1] << 8));
}



/*
 *   NFC higher-level
 */


typedef enum
{
    NFC_RSP_ERROR = -1,
    NFC_RSP_OK = 0
    /* 1 and above - time waiting extension requested */
} NfcResponse;


static void SendReq(const uint8* data, size_t len, bool withCrc)
{
    uint16 crc = InitReq();
    SendReqChunk(&crc, data, len);
    DoneReq(withCrc ? &crc : NULL);
}


static NfcResponse ReceiveRsp(uint8* data, size_t len)
{
    uint16 crc = InitRsp();
    uint8 pcb = ReceiveRsp8(&crc);
    if (pcb == 0xf2) /* frame waiting extension */
    {
        uint8 tws = ReceiveRsp8(&crc);
        if (!DoneRsp(&crc))
            return NFC_RSP_ERROR;
        return tws;
    }
    else if (pcb == 0xc2) /* deselect OK */
    {
        return DoneRsp(&crc) ? NFC_RSP_OK : NFC_RSP_ERROR;
    }
    else
    {
        ReceiveRspChunk(&crc, data, len);
        return DoneRsp(&crc) ? NFC_RSP_OK : NFC_RSP_ERROR;
    }
}


static uint8 BlockNum(void)
{
    static uint8 b = 0x03;
    b ^= 1;
    return b;
}


static void SendBlock(const uint8* data, size_t len)
{
    uint16 crc = InitReq();
    SendReq8(&crc, BlockNum());
    SendReqChunk(&crc, data, len);
    DoneReq(&crc);
}


#define SYSTEM_FILE 0xE101
#define CC_FILE 0xE103
#define NDEF_FILE 0x0001


static void NFCInitSession(void)
{
    /* Force I2C session, kill RF sessions if any */
    {
        const uint8 killRfCmd[] = {0x52};
        SendReq(killRfCmd, sizeof(killRfCmd), FALSE);
    }
    /* Select NFC T4 application */
    {
        const uint8 ndefAppSelectCmd[] = {0x00, 0xA4, 0x04, 0x00, 0x07, 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01, 0x00};
        SendBlock(ndefAppSelectCmd, sizeof(ndefAppSelectCmd));
        ReceiveRsp(NULL, 2);
    }
}


static bool SelectFile(unsigned fileId)
{
    const uint8 selectFileCmd[] = {0x00, 0xA4, 0x00, 0x0C, 0x02};
    uint16 crc = InitReq();
    uint8 rsp[2];
    SendReq8(&crc, BlockNum());
    SendReqChunk(&crc, selectFileCmd, sizeof(selectFileCmd));
    SendReq16(&crc, fileId);
    DoneReq(&crc);
    return ReceiveRsp(rsp, sizeof(rsp)) == NFC_RSP_OK && rsp[0] == 0x90;
}


static bool DeselectFile(void)
{
    uint16 crc = InitReq();
    SendReq8(&crc, 0xc2);
    DoneReq(&crc);
    return ReceiveRsp(NULL, 0) == NFC_RSP_OK;
}


/* NOTE: caller is responsible for freeing data */

static bool ReceiveFile(unsigned fileId, uint8** data, size_t* fileLen)
{
    /* File should be selected already */

    /* Read file length */
    {
        const uint8 readFileLenCmd[] = {0x00, 0xB0, 0x00, 0x00, 0x02};
        uint8 rsp[4];
        SendBlock(readFileLenCmd, sizeof(readFileLenCmd));
        if (ReceiveRsp(rsp, sizeof(rsp)) != NFC_RSP_OK || rsp[2] != 0x90)
            return FALSE;
        *fileLen = (rsp[0] << 8) | rsp[1];
        if (*fileLen == 0)
            return TRUE;
    }
    /* Read file */
    if (*fileLen <= 0xf6)
    {
        const uint8 readFileCmd[] = {0x00, 0xB0};
        uint16 crc = InitReq();
        SendReq8(&crc, BlockNum());
        SendReqChunk(&crc, readFileCmd, sizeof(readFileCmd));
        SendReq16(&crc, fileId == NDEF_FILE ? 2 : 0); /* file offset */
        SendReq8(&crc, *fileLen);
        DoneReq(&crc);
        *data = PanicUnlessMalloc(*fileLen);
        crc = InitRsp();
        ReceiveRsp8(&crc); /* block 0x02 or 0x03 */
        ReceiveRspChunk(&crc, *data, *fileLen);
        ReceiveRsp8(&crc); /* result code 0x90 */
        ReceiveRsp8(&crc); /* result code 0x00 */
        if (!DoneRsp(&crc))
        {
            free(*data);
            *data = NULL;
            return FALSE;
        }
    }
    else
    {
        NFC_PRINT(("ERROR: file too long (%d)\n", *fileLen));
        *fileLen = 0;
        return TRUE;
    }
    return TRUE;
}


static bool CheckAccessRights(void)
{
    /* TODO: actually check access rights. For now reading CC is required
       before reading NDEF, somehow it fails if we don't. */
    uint8* data = NULL;
    size_t len = 0;
    if (!SelectFile(CC_FILE))
        return FALSE;
    if (!ReceiveFile(CC_FILE, &data, &len))
        return FALSE;
    free(data);
    return TRUE;
}


static bool UpdateBinary(size_t offs, const uint8* data, size_t len)
{
    const uint8 updateBinaryCmd[] = {0x00, 0xD6};
    uint8 rsp[2];
    uint16 crc = InitReq();
    int result;
    SendReq8(&crc, BlockNum());
    SendReqChunk(&crc, updateBinaryCmd, sizeof(updateBinaryCmd));
    SendReq16(&crc, offs);
    SendReq8(&crc, len);
    SendReqChunk(&crc, data, len);
    DoneReq(&crc);
    dly(10000);
    result = ReceiveRsp(rsp, sizeof(rsp));
    dly(10000);
    if (result > 0) /* TFW extension */
    {
        uint16 crc = InitReq();
        SendReq8(&crc, 0xf2);
        SendReq8(&crc, result & 0xff);
        DoneReq(&crc);
        dly(10000);
        result = ReceiveRsp(rsp, sizeof(rsp));
        dly(10000);
    }
    if (result != NFC_RSP_OK || rsp[0] != 0x90)
        return FALSE;
    return TRUE;
}


static bool WriteFile(const uint8* data, size_t len)
{
    /* File should be selected already */

    uint8 lenData[2];

    lenData[0] = 0;
    lenData[1] = 0;
    if (!UpdateBinary(0, lenData, 2))
        return FALSE;

    if (!UpdateBinary(2, data, len))
        return FALSE;

    lenData[0] = len >> 8;
    lenData[1] = len & 0xff;
    if (!UpdateBinary(0, lenData, 2))
        return FALSE;

    return TRUE;
}


/* Caller is responsible for freeing the buffer */

static uint8* BuildNdef(const char* localName, const bdaddr* localAddr, size_t* ndefLen)
{
    const uint8 ndef1[] =
    {
        0xD2,           /* MB=1b, ME=1b, CF=0b, SR=1b, IL=0b, TNF=010b */
        0x20,           /* Record Type Length: 32 octets (record type name below) */
        0x15,           /* Payload length */
        0x61, 0x70, 0x70, 0x6C, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x76, 0x6E, 0x64, 0x2E,
        0x62, 0x6C, 0x75, 0x65, 0x74, 0x6F, 0x6F, 0x74, 0x68, 0x2E, 0x65, 0x70, 0x2E, 0x6F, 0x6F, 0x62,
                        /* Record Type Name: application/vnd.bluetooth.ep.oob */
        0x15, 0x00,     /* OOB optional data length: 33 octets */
        0x01, 0xff, 0x00, 0x5b, 0x02, 0x00, /* BT MAC address */
        0x01,           /* EIR Data Length */
        0x09            /* EIR Data Type: Complete Local Name */
    };
    const uint8 ndef2[] =
    {
        0x04,           /* EIR Data Length: 4 octets */
        0x0D,           /* EIR Data Type: Class of device */
        0x04, 0x04, 0x20,  /* Class of Device:
                             0x20, Service Class=Audio
                             0x04, Major Device Class=Audio/Video
                             0x04, Minor Device Class=Wearable */
        /* Headset Device *: */
        0x05,           /* EIR Data Length: 5 octets */
        0x03,           /* EIR Data type: 16-bit Service Class UUID list (complete) */
        0x1E, 0x11, 0x0B, 0x11
                        /* 16-bit Service Class UUID list (complete); 0x111E - HFP-HF, 0x011B - A2DP-SNK */
    };
    const size_t mimeLen = 32; /* application/vnd.bluetooth.ep.oob */

    uint8* ndef;
    size_t nameLen = strlen(localName);

    *ndefLen = sizeof(ndef1) + nameLen + sizeof(ndef2);
    ndef = PanicUnlessMalloc(*ndefLen);
    memcpy(ndef, ndef1, sizeof(ndef1));
    memcpy(ndef + sizeof(ndef1), localName, nameLen);
    memcpy(ndef + sizeof(ndef1) + nameLen, ndef2, sizeof(ndef2));

    ndef[2] += nameLen;
    ndef[mimeLen + 3] += nameLen;
    ndef[mimeLen + 5] = localAddr->lap & 0xff;
    ndef[mimeLen + 6] = (localAddr->lap >> 8) & 0xff;
    ndef[mimeLen + 7] = (localAddr->lap >> 16) & 0xff;
    ndef[mimeLen + 8] = localAddr->uap;
    ndef[mimeLen + 9] = localAddr->nap & 0xff;
    ndef[mimeLen + 10] = (localAddr->nap >> 8) & 0xff;
    ndef[mimeLen + 11] += nameLen;

    return ndef;
}


bool AllNFCInit(const char* localName, const bdaddr* localAddr)
{
    NFC_PRINT(("NAME: \"%s\"\n", localName));
    NFC_PRINT(("BDADDR: %04x:%02x:%06lx\n", localAddr->nap, localAddr->uap, localAddr->lap));

    ShowProgress(1);

    NFC_PRINT(("NFC: Init I2C session\n"));
    NFCInitSession();

    NFC_PRINT(("NFC: Read CC file\n"));
    if (!CheckAccessRights())
        return FALSE;

    {
        size_t oldNdefLen = 0;
        uint8* oldNdef = NULL;
        size_t ndefLen = 0;
        uint8* ndef = NULL;
        bool needUpdate = FALSE;

        NFC_PRINT(("NFC: Select NDEF\n"));
        if (!SelectFile(NDEF_FILE))
            return FALSE;

        ShowProgress(2);

        NFC_PRINT(("NFC: Read NDEF\n"));
        if (!ReceiveFile(NDEF_FILE, &oldNdef, &oldNdefLen))
            return FALSE;

        NFC_PRINT(("NFC: Compare NDEF\n"));
        ndef = BuildNdef(localName, localAddr, &ndefLen);
        needUpdate = FORCE_NFC_UPDATE || ndefLen != oldNdefLen || memcmp(ndef, oldNdef, ndefLen) != 0;
        free(oldNdef);

        if (needUpdate)
        {
            ShowProgress(3);

            NFC_PRINT(("NFC: Select NDEF\n"));
            if (!SelectFile(NDEF_FILE))
                return FALSE;

            NFC_PRINT(("NFC: Write NDEF\n"));
            if (!WriteFile(ndef, ndefLen))
            {
                free(ndef);
                return FALSE;
            }

            free(ndef);

            NFC_PRINT(("NFC: Deselect\n"));
            if (!DeselectFile())
                return FALSE;

        }
    }

    ShowProgress(0);
    DeselectFile(); /*zzzfix, without this, HD nfc doesn't work. This command seems to put NFC chip to a know state, otherwise, the NFC chip
                      status could be something like RF disabled, so that causes NFC doesn't work*/
    NFC_PRINT(("--------- NFC Complete\n"));
    return TRUE;
}

