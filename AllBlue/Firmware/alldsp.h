#ifndef _ALLDSP_H_
#define _ALLDSP_H_

#include <library.h>
#include <led.h>
#include <stream.h>
#include "sink_events.h"

/* #define EnablePowerOnBlinkingAndSleepBlinking */
					/*this define must be off for production release*/

#define ALL_MUSIC_SAMPLING_RATE 44100
#define ALL_VOICE_SAMPLING_RATE 8000


#define ALL_PRODUCT_SKULL_XS_1  0x1001
#define ALL_PRODUCT_TWIST       0x1002
#define ALL_PRODUCT_SKULL_HD    0x1003
#define ALL_PRODUCT_BULL        0x1004
#define ALL_PRODUCT_TOUR        0x1005
#define ALL_PRODUCT_SKULL_XS_2  0x1006
#define ALL_PRODUCT_UNKNOWN     -1


typedef enum
{
    DSP_STATUS_OFF,
    DSP_STATUS_INITIALIZING,
    DSP_STATUS_ON_UNVERIFIED,
    DSP_STATUS_UPGRADING_MAIN,
    DSP_STATUS_UPGRADING_SECONDARY,
    DSP_STATUS_ON
} DspStatus;


typedef enum
{
    BATTERY_STATUS_OK,
    BATTERY_STATUS_LOW,
    BATTERY_STATUS_CHARGING,
    BATTERY_STATUS_FULL
} BatteryStates;


typedef enum
{
    TWS_STATUS_OFF,
    TWS_STATUS_INQ, /* master */
    TWS_STATUS_DISC, /* slave */
    TWS_STATUS_CONNECTED
} TwsStatus;


typedef enum
{
    TWS_NONE,
    TWS_MASTER,
    TWS_SLAVE
} TwsConnectionType;

typedef enum
{
    DecoderType_SBC,
    DecoderType_AAC,
    DecoderType_APTX,
    DecoderType_NotSure
} A2dpAudioDecoderType;

typedef enum
{
    REMOTE_NONE         = 0x00,
    REMOTE_NEXT         = 0x01,
    REMOTE_PREV         = 0x02,
    REMOTE_PLAY_PAUSE   = 0x04,
    REMOTE_BASS_BOOST   = 0x08,
    REMOTE_VOLUME_UP    = 0x10,
    REMOTE_VOLUME_DOWN  = 0x20,
    REMOTE_MUTE         = 0x40,
	REMOTE_MENU			= 0x80	/* Currently unused */
} RemoteEvent;


typedef enum
{
    ALL_APP_POWERED_OFF,
    ALL_APP_POWERED_ON,                 /* use if (all->startup) {...} to see if the device is on */
    ALL_APP_STARTUP_JINGLE_POSTED,      /* to ensure correct sequence of AllResetDock() on HD/Bull */
    ALL_APP_STARTUP_JINGLE_FINISHED,    /* to ensure the startup jingle is always played first */
    ALL_APP_SHUTDOWN_JINGLE_POSTED      /* to ensure device is not rebooted until this jingle finishes */
} AllAppStartup;


typedef enum
    { DSP_MAIN, DSP_SECONDARY } DspIndex;


typedef struct
    { uint8 high, mid, low; } Version;


typedef struct
{
    Version dfi;
    Version mfi;
} DspVersionInfo;


typedef struct
{
    uint32 dfiUsedPios;
    int pioMOSI;
    int pioEECS;
    const char* dfiFileName;
} DspConfig;


typedef struct
{
    uint32 usedPios;
    int pioSD;
    int pioTRST;
    led_id powerOffLed;
    led_id bluetoothLed;
    led_id lineInLed;
    DspConfig dsp[2];       /* [DspIndex] */
} ProductConfig;


typedef struct
{
    uint16 productId;   /* device BCD & 0x1fff */
    bool sleepless;     /* "sleepless" mode, i.e. never time out on no signal; device BCD & 0x2000 */
    bool rescueMode;    /* temporary image to bring the device to adequate state */
    const ProductConfig* conf;
    DspVersionInfo dspVersion;
    uint16 CurrentA2dpAudioDecoderUsed;
    uint16 JingleIsOn;
    
    char serial[9];         /* BD addr LAP in hex */
    uint8 serialSent;
    char aeroVersion[9];    /* AeroUpdate version string */
    uint8 aeroVersionSent;

    unsigned flashPage;     /* DFI update vars: */
    uint16 pageCrc;
    uint16 pageSize;
    unsigned runningTime;
    unsigned sectorRetryCount;

    AllAppStartup startup;  /* powered on, startup jingle started etc. */
    DspStatus dsp;
    uint32 seconds;         /* seconds since boot up; restarted on power on */
    uint16 dimStage;        /* 60ms ticks, used for LED "breathing" */
    uint16 whiteCountdown;  /* used to indicate max or min volume */
    int volume;
    int dockVolume;         /* to be sent to DSP only when streaming via dock */
    uint32 batteryLevel;
    uint32 batteryTemperature;
    BatteryStates batteryStatus; /* used for triggering battery events and playing jingles */
    bool dcOn;
    int dspRetryCount;
    int dspFlags;
    TwsStatus twsStatus;

    bool JinglePlayingIsFinished;
    bool IsAlreadyUnMuted;
    bool a2dpConnected;
    bool a2dpStreaming;
    bool wiredConnected;
    bool dockConnected; /* Skull HD */
	RemoteEvent remoteEventMask; /* Skull HD */
    bool voicePromptPlaying;

    Sink dsp_sink_a, dsp_sink_b;
    Sink speaker_sink_a, speaker_sink_b;
} AllData;


extern AllData* all;

#define IsSkullHdBull()     (all->productId == ALL_PRODUCT_SKULL_HD || all->productId == ALL_PRODUCT_BULL)
#define IsBull()            (all->productId == ALL_PRODUCT_BULL)
#define IsTwist()           (all->productId == ALL_PRODUCT_BULL)
#define IsSkullXsTwist()    (all->productId == ALL_PRODUCT_SKULL_XS_1 || all->productId == ALL_PRODUCT_SKULL_XS_2 || all->productId == ALL_PRODUCT_TWIST)

Version ExpectedDfiVersion(void);
Version ExpectedMfiVersion(void);
void UpdateDspVersionInfo(void);
void UpdateJingleMode(void);

void AllConfigInit(void);
uint32 AllDeviceClass(void);

void AllPowerOn(void);
void AllPowerOff(void);
void AllVolumeNotify(uint16 volume); /* 0 for mute, 1..16 for A2DP levels */
void AllMute(void);
void AllUnmute(bool now);
void AllShowBatteryLevel(void);
void AllReboot(void);

void AllA2dpConnectedNotify(bool connected, TwsConnectionType tws);
void AllA2dpStreamingNotify(bool streaming);
void AllWiredConnectedNotify(bool connected);
void AllDockStreamingNotify(bool connected);
void AllResetDock(void); /* Skull HD only */
void AllVoicePromptNotify(bool playing);
void AllTwsNotify(TwsStatus status);
void AllSinkEventNotify(sinkEvents_t event);
bool AllOkToSleep(void);
void AllSleepNotify(void);
void AllDecoderTypeNotify(A2dpAudioDecoderType DecType);

Sink AllConnect(uint32 rate, bool stereo, Source src_a, Source src_b);
void AllDisconnect(void);
Sink AllConnectAdpcm(uint32 rate, bool stereo, Source src_a);

void AllAbort(int);
void AllToggleJingleOnOffFlag(void);

#endif /* _ALLDSP_H_ */

