#ifndef _ALLNFC_H_
#define _ALLNFC_H_

#include <bdaddr.h>

bool AllNFCInit(const char* localName, const bdaddr* localAddr);

#endif
