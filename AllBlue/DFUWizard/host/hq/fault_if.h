/*
 * fault_if.h - defines an enum of the possible fault reasons
 * 
 * Fault codes, with description strings are in the xmacro file "fault_codes.h". See
 * this file for full explanation of usage.
 */
 
#ifndef _FAULT_IF_H_
#define _FAULT_IF_H_

#define FAULT_IFACE_X(enum, hexv, sdesc, ldesc) enum = hexv
#define FAULT_IFACE_SEP  ,
enum faultid_enum{

#ifdef HAVE_HYDRA_ARCHITECTURE
#ifdef NOT_YET_HYDRA_FAULT_XMACROS
/* Header fragment containing the body of an enum of common hydra fault
codes. It is missing the enum enclosures at the start and end. It is
included here at the beginning because there is a comma after the final
entry, which prevents it from being included at the end of the enum */
#include "hydra_faultids.h"
#else
#error "Don't know how to turn Hydra faults into Xmacros"
#endif /* NOT_YET_HYDRA_FAULT_XMACROS */
#endif /* HAVE_HYDRA_ARCHITECTURE */

#include "fault_codes.h"

};
#undef FAULT_IFACE_X
#undef FAULT_IFACE_SEP

typedef enum faultid_enum faultid; 

#endif /* _FAULT_IF_H_ */


