/****************************************************************************
FILE
    ram_reserve_types.h  -  type declarations for ram_reserve

DESCRIPTION
    Holds any type declarations required for ram_reserve. Used by modules
    that need these but not the entire BCCMD PDU definition.
*/

#ifndef __RAM_RESERVE_TYPES_H__
#define __RAM_RESERVE_TYPES_H__


/* Allowed values for 'type' in BCCMDPDU_ALLOCATE/RECLAIM_RAM_RESERVE */
typedef enum {
    RAM_RESERVE_ALLOC_TYPE_TOKEN = 1
} RAM_RESERVE_ALLOC_TYPES;


#endif
