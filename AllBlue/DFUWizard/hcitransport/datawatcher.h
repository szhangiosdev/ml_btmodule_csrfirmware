/**********************************************************************
 *
 *  datawatcher.h
 *
 *  Copyright (C) Cambridge Silicon Radio Ltd
 *
 *  Contains the class specification for the DataWatcher class
 *
 ***********************************************************************/
#ifndef DATAWATCHER_INC
#define DATAWATCHER_INC

//List of valid transport channels
typedef enum
{
    TRANS_CHANNEL_UNDEFINED,
    //No current need for 'control' endpoint definition
    TRANS_CHANNEL_USB_BULK,
    TRANS_CHANNEL_USB_ISOCH,
    TRANS_CHANNEL_USB_INTERRUPT
} TransChannelEnum;

class DataWatcher
{
public:
    virtual void onSend ( const uint8* , size_t ) = 0;
    virtual void onRecv ( const uint8* , size_t , TransChannelEnum ) = 0; //TransChannelEnum param added for B-101514 to allow passing of usb endpoint
    virtual ~DataWatcher(){}
    virtual void operator= (const DataWatcher&){}
    // no need for a copy constructor
};

#endif //DATAWATCHER_INC
