//******************************************************************************
//
//  ptaptransportlinux.cpp
//
//  Copyright � Cambridge Silicon Radio Ltd.
//
//  Linux-specific code for dealing with the Windows Hydra driver.
//
//******************************************************************************

#ifdef WIN32
#else //WIN32

#include <cassert>
#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "csrhci/name_store.h"
#include "engine/enginefw_interface.h"
#include "ptaptransport.h"

#define ASSERT assert

///
/// Constructor.
///
PtapTransportLinux::PtapTransportLinux(
    const PtapConfiguration& aConf,
    const CallBacks& aCallBacks)
    :
    PtapTransport(aConf, aCallBacks),
    mPtapDeviceFileDescr(0)
{
    FUNCTION_DEBUG_SENTRY;
}

///
/// Destructor.
///
PtapTransportLinux::~PtapTransportLinux()
{
    FUNCTION_DEBUG_SENTRY;
}

///
/// Open the Linux packet tap interface.
///
bool PtapTransportLinux::openPacketTapInterface()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;
    std::string device = "/dev/ptap";

    mPtapDeviceFileDescr = open(device.c_str(), O_RDWR);

    if (mPtapDeviceFileDescr == -1)
    {
        printf("Failed to open %s\n", device.c_str());
        ok = false;
    }

    return ok;
}

///
/// Close the Linux packet tap interface.
///
bool PtapTransportLinux::closePacketTapInterface()
{
    FUNCTION_DEBUG_SENTRY;

    //stop reading thread
    Threadable::Stop();
    // check if reading thread really stopped
    bool stopped = Threadable::WaitForStop(500);

    if (!stopped)
    {
        Threadable::Kill(500);
    }

    close(mPtapDeviceFileDescr);
    
    return true;
}

///
/// The reading thread for processing responses.
///
int PtapTransportLinux::ThreadFunc()
{
    FUNCTION_DEBUG_SENTRY;

    unsigned char buffer[8192];
    int r = 0;
    fd_set readfds;

    do
    {
        FD_ZERO(&readfds);
        FD_SET(mPtapDeviceFileDescr, &readfds);

        // block until something available to read
        r = select(mPtapDeviceFileDescr + 1, &readfds, NULL, NULL, NULL);

        if(r < 0)
        {
            printf("Failed select on device file (%s)\n", strerror(errno));;
            return 1;
        }

        r = read(mPtapDeviceFileDescr, buffer, sizeof(buffer));

        if(r < 0)
        {
            printf("Failed reading device file (%s)\n", strerror(errno));
            return 1;
        }

        processRx(buffer, r);

    } while (Threadable::KeepGoing());

    return 0;
}

///
/// Write a message to the packet tap interface.
///
size_t PtapTransportLinux::writeToPtap(
    uint8* apBuffer,
    size_t aLength)
{
    FUNCTION_DEBUG_SENTRY;

#ifdef DEBUG_PTAP
    dumpBuffer(apBuffer, aLength, false);
#endif //DEBUG_PTAP

    size_t sentBytes = write(mPtapDeviceFileDescr, apBuffer, aLength);
    return  sentBytes;
}

#endif //WIN32
