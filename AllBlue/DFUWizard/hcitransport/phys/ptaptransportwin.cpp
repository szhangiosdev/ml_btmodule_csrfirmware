//******************************************************************************
//
//  ptaptransportwin.cpp
//
//  Copyright � Cambridge Silicon Radio Ltd.
//
//  Windows-specific code for dealing with the Windows Hydra driver.
//
//******************************************************************************

#ifdef WIN32

#include <windows.h>

#include <cassert>
#include <cstring>
#include <initguid.h>
#if !defined _WINCE && !defined _WIN32_WCE
#include <setupapi.h>
#endif //!defined _WINCE && !defined _WIN32_WCE
#include <stdio.h>

#include "csrhci/name_store.h"
#include "engine/enginefw_interface.h"
#include "ptaptransport.h"

// The packet tap device interface GUID {28B3FADA-ECCC-449D-95EE-77CCD059F5BF}.
DEFINE_GUID(GUID_DEVINTERFACE_PACKET_TAP, 0x28B3FADA, 0xECCC, 0x449D, 0x95, 0xEE, 0x77, 0xCC, 0xD0, 0x59, 0xF5, 0xBF);

///
/// Constructor.
///
PtapTransportWin::PtapTransportWin(
    const PtapConfiguration& aConf,
    const CallBacks& aCallBacks)
    :
    PtapTransport(aConf, aCallBacks),
    mInterface(L"ptap"),
    mFile(INVALID_HANDLE_VALUE)
{
    FUNCTION_DEBUG_SENTRY;

    // Create the read stop event.
    mReadStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

///
/// Destructor.
///
PtapTransportWin::~PtapTransportWin()
{
    FUNCTION_DEBUG_SENTRY;

    // Dispose of the read stop event.
    CloseHandle(mReadStopEvent);
}

///
/// Open the Windows packet tap interface.
///
bool PtapTransportWin::openPacketTapInterface()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;

    if (mFile != INVALID_HANDLE_VALUE)
    {
        printf("Packet tap interface is already open");
        return true;
    }

    // Determine the name of the packet tap device interface.
    std::wstring device;
    bool found = findInterface(device, mInterface);
    if (!found)
    {
        printf("Could not find device interface \"%S\"\n", mInterface.c_str());
        ok = false;
    }

    else
    {
        // Open the packet tap interface.
        mFile = CreateFile(device.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        if (mFile == INVALID_HANDLE_VALUE)
        {
            printf("Failed to open device interface \"%S\" (0x%08X)\n", mInterface.c_str(), GetLastError());
            ok = false;
        }
    }

    return ok;
}

///
/// Close the Windows packet tap interface.
///
bool PtapTransportWin::closePacketTapInterface()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;
    BOOL result;

    if (mFile == INVALID_HANDLE_VALUE)
    {
        printf("Packet tap interface is not open\n");
        return true;
    }

    // Stop the reading thread.
    Threadable::Stop();

    result = SetEvent(mReadStopEvent);
    if (result == 0)
    {
        printf("SetEvent failed (0x%08X)\n", GetLastError());
    }

    ok = WaitForStop(INFINITE);
    if (!ok)
    {
        printf("Problem waiting for the reading thread to stop\n");
    }

    // Close the packet tap interface.
    result = CloseHandle(mFile);
    if (result == 0)
    {
        printf("CloseHandle failed (0x%08X)\n", GetLastError());
    }

    mFile = INVALID_HANDLE_VALUE;

    return ok;
}

///
/// The reading thread for processing responses.
///
int PtapTransportWin::ThreadFunc()
{
    FUNCTION_DEBUG_SENTRY;

    /* The size of this buffer must be more than the maximum possible
     * packet size that can be read (so a packet is read with a single
     * read). The UCAPS logger interface queues less than 8KiB Rx buffers. */
    uint8 pBuffer[QPAIR_TOHOST_BUFFER_SIZE];

    // Create the read complete event.
    HANDLE hReadComplete = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (hReadComplete == NULL)
    {
        printf("CreateEvent failed (0x%08X)\n", GetLastError());
        onTransportFailure();
    }

    else
    {
        BOOL result;

        OVERLAPPED ol = {0, };
        ol.hEvent = hReadComplete;

        HANDLE events[2];
        events[0] = mReadStopEvent;
        events[1] = ol.hEvent;

        while (KeepGoing())
        {
            DWORD bytesRead;

            // Initiate the read.
            result = ReadFile(mFile, pBuffer, sizeof(pBuffer), NULL, &ol);
            if (result == 0 && GetLastError() != ERROR_IO_PENDING)
            {
                printf("ReadFile failed (0x%08X)\n", GetLastError());
                onTransportFailure();
                break;
            }

            // Wait on the read complete signal or read stop signal.
            DWORD dw = WaitForMultipleObjects(2, events, FALSE, INFINITE);
            if (dw == WAIT_FAILED)
            {
                printf("WaitForMultipleObjects failed (0x%08X)\n", GetLastError());
                onTransportFailure();
                break;
            }

#if !defined _WINCE && !defined _WIN32_WCE
            // Was read stop signalled?
            if (!KeepGoing())
            {
                result = CancelIo(mFile);
                if (result == 0)
                {
                    printf("CancelIo failed (0x%08X)\n", GetLastError());
                    onTransportFailure();
                    break;
                }
            }

            result = GetOverlappedResult(mFile, &ol, &bytesRead, TRUE);
#endif //!defined _WINCE && !defined _WIN32_WCE

            if (!KeepGoing())
            {
                break;
            }

            if (result == 0)
            {
                printf("GetOverlappedResult failed (0x%08X)\n", GetLastError());
                onTransportFailure();
                break;
            }

            // Reset the read complete event.
            result = ResetEvent(ol.hEvent);
            if (result == 0)
            {
                printf("ResetEvent failed (0x%08X)\n", GetLastError());
                onTransportFailure();
                break;
            }

            // Process the data read.
            bool ok = processRx(pBuffer, bytesRead);
            if (!ok)
            {
                printf("PtapTransportWin::processRx failed\n");
                onTransportFailure();
                break;
            }
        }

        // Dispose of the read complete event.
        result = CloseHandle(hReadComplete);
        if (result == 0)
        {
            printf("CloseHandle failed (0x%08X)\n", GetLastError());
        }
    }

    return 0;
}

///
/// Find the packet tap interface.
///
bool PtapTransportWin::findInterface(
    std::wstring& aDevice,
    const std::wstring& aInterface)
{
    FUNCTION_DEBUG_SENTRY;

#if !defined _WINCE && !defined _WIN32_WCE
    bool found = false;

    //
    // Open a handle to the device information set of all
    // present devices that expose the packet tap interface.
    //
    HDEVINFO hardwareDeviceInfo = SetupDiGetClassDevs(
        (LPGUID)&GUID_DEVINTERFACE_PACKET_TAP,
        NULL,
        NULL,
        DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);

    if (hardwareDeviceInfo == INVALID_HANDLE_VALUE)
    {
        printf("SetupDiGetClassDevs failed (0x%08X)\n", GetLastError());
    }

    else
    {
        BOOL result;

        SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
        deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

        SP_DEVICE_INTERFACE_DETAIL_DATA* pDeviceInterfaceDetailData = NULL;

        for (int index = 0; ; index++)
        {
            //
            // Enumerate each device interface in the device information set in turn.
            //
            result = SetupDiEnumDeviceInterfaces(
                hardwareDeviceInfo,
                NULL,
                (LPGUID)&GUID_DEVINTERFACE_PACKET_TAP,
                index,
                &deviceInterfaceData);

            if (result == 0 && GetLastError() == ERROR_NO_MORE_ITEMS)
            {
                break;
            }

            else if (result == 0)
            {
                printf("SetupDiEnumDeviceInterfaces failed (0x%08X)\n", GetLastError());
                break;
            }

            else
            {
                DWORD requiredLength;

                //
                // Determine the required length of the device interface detail data memory.
                //
                result = SetupDiGetDeviceInterfaceDetail(
                    hardwareDeviceInfo,
                    &deviceInterfaceData,
                    NULL,
                    0,
                    &requiredLength,
                    NULL);

                if (result != 0 || GetLastError() != ERROR_INSUFFICIENT_BUFFER)
                {
                    printf("SetupDiGetDeviceInterfaceDetail failed (0x%08X)\n", GetLastError());
                    break;
                }

                else
                {
                    //
                    // Deallocate any existing device interface detail data memory.
                    //
                    if (pDeviceInterfaceDetailData != NULL)
                    {
                        free(pDeviceInterfaceDetailData);
                        pDeviceInterfaceDetailData = NULL;
                    }

                    //
                    // Allocate the device interface detail data memory.
                    //
                    pDeviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(requiredLength);

                    if (pDeviceInterfaceDetailData == NULL)
                    {
                        printf("Failed to allocate %d bytes for device interface detail data\n", requiredLength);
                        break;
                    }

                    else
                    {
                        pDeviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

                        //
                        // Populate the device interface detail data memory.
                        //
                        result = SetupDiGetDeviceInterfaceDetail(
                            hardwareDeviceInfo,
                            &deviceInterfaceData,
                            pDeviceInterfaceDetailData,
                            requiredLength,
                            NULL,
                            NULL);

                        if (result == 0)
                        {
                            printf("SetupDiGetDeviceInterfaceDetail failed (0x%08X)\n", GetLastError());
                            break;
                        }

                        else
                        {
                            //
                            // Extract the interface name.
                            //
                            aDevice = pDeviceInterfaceDetailData->DevicePath;

                            if (aDevice.rfind(aInterface) == aDevice.length() - aInterface.length())
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Deallocate any device interface detail data memory.
        if (pDeviceInterfaceDetailData != NULL)
        {
            free(pDeviceInterfaceDetailData);
            pDeviceInterfaceDetailData = NULL;
        }

        // Dispose of the device information set.
        result = SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
        if (result == 0)
        {
            printf("SetupDiDestroyDeviceInfoList failed (0x%08X)\n", GetLastError());
        }
    }

    return found;

#else //!defined _WINCE && !defined _WIN32_WCE
    printf("findInterface not supported in WinCE\n");
    return false;

#endif //!defined _WINCE && !defined _WIN32_WCE
}

///
/// Write a message to the packet tap interface.
///
size_t PtapTransportWin::writeToPtap(
    uint8* apBuffer,
    size_t aLength)
{
    FUNCTION_DEBUG_SENTRY;

    DWORD bytesWritten = 0;

    if (mFile == INVALID_HANDLE_VALUE)
    {
        printf("Packet tap interface is not open\n");
        return 0;
    }

    // Create the write complete event.
    HANDLE hWriteComplete = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (hWriteComplete == NULL)
    {
        printf("CreateEvent failed (0x%08X)\n", GetLastError());
    }

    else
    {
        BOOL result;

        OVERLAPPED ol = {0, };
        ol.hEvent = hWriteComplete;

#ifdef DEBUG_PTAP
        dumpBuffer(apBuffer, aLength, false);
#endif //DEBUG_PTAP

        // Initiate the write.
        result = WriteFile(mFile, apBuffer, (DWORD)aLength, &bytesWritten, &ol);
        if (result == 0 && GetLastError() != ERROR_IO_PENDING)
        {
            printf("WriteFile failed (0x%08X)\n", GetLastError());
        }

        else
        {
#if !defined _WINCE && !defined _WIN32_WCE
            result = GetOverlappedResult(mFile, &ol, &bytesWritten, TRUE);
#endif //!defined _WINCE && !defined _WIN32_WCE
            if (result == 0 || bytesWritten != aLength)
            {
                printf("GetOverlappedResult failed (0x%08X)\n", GetLastError());
            }
        }

        // Dispose of the write complete event.
        result = CloseHandle(hWriteComplete);
        if (result == 0)
        {
            printf("CloseHandle failed (0x%08X)\n", GetLastError());
        }
    }

    return bytesWritten;
}

#else //WIN32
#endif //WIN32
