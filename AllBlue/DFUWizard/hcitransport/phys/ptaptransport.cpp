//******************************************************************************
//
//  ptaptransport.cpp
//
//  Copyright � Cambridge Silicon Radio Ltd.
//
//  Transport using the packet tap interface of the Hydra driver for Hydra chips.
//
//  Refer to CS-211901 for the Packet Tap Specifiction.
//
//  Note that all fields in the packet tap protocol are little-endian.
//
//******************************************************************************

#ifdef WIN32
#include <windows.h>
#endif //WIN32

#include <cassert>
#include <cstring>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>

#include "csrhci/name_store.h"
#include "engine/enginefw_interface.h"
#include "ptaptransport.h"

///
/// Constructor.
///
PtapTransport::PtapTransport(
    const PtapConfiguration& aConfig,
    const CallBacks& aRecv)
    :
    BlueCoreTransportImplementation("PtapTransport", aConfig.useCSRExtensions(), aRecv),
    mRequestedHifId(aConfig.getSelectedCuratorHifId()),
    mCuratorServiceExists(false),
    mBluetoothServiceExists(false),
    mTransportReady(false),
    mRxStatusCode(PTAP_STATUS_SUCCESS)
{
    FUNCTION_DEBUG_SENTRY;
}

///
/// Destructor.
///
PtapTransport::~PtapTransport()
{
    FUNCTION_DEBUG_SENTRY;

    stop();
}

///
/// Start the packet tap interface of the Hydra chip driver.
///
bool PtapTransport::start()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok;

    // Presume the transport will fail to start.
    mTransportReady = false;

    // Open the packet tap interface.
    ok = openPacketTapInterface();

    if (!ok)
    {
        printf("Failed to open packet tap interface\n");
    }
    else
    {
        // Start a reading thread to receive and process responses.
        ok = Threadable::Start();

        if (!ok)
        {
            printf("Failed to start reading thread\n");
            //// DISPOSE OF mFile
        }
        else
        {
            // Map the Bluetooth service to the configured host interface.
            ok = mapBluetoothServiceToHif();

            if (!ok)
            {
                printf("Failed to map Bluetooth service to configured host interface\n");
                //// DISPOSE OF Thread
                //// DISPOSE OF mFile
            }
            else
            {
                printf("Bluetooth service (sId:%04X) is mapped\n", mBluetoothServiceInfo.sId);

                // Attach to queue pairs.
                ok = attachToQueuePairs();

                if (!ok)
                {
                    printf("Failed to attach to queue pairs\n");
                    //// DISPOSE OF mapping
                    //// DISPOSE OF Thread
                    //// DISPOSE OF mFile
                }
                else
                {
                    printf("Attached to queue pairs successfully\n");
                    mTransportReady = true;
                }
            }
        }
    }

    return ok;
}

///
/// Stop the packet tap interface of the Hydra chip driver.
///
void PtapTransport::stop()
{
    FUNCTION_DEBUG_SENTRY;

    if (mTransportReady)
    {
        mTransportReady = false;

        // Detach from queue pairs.
        bool ok = detachFromQueuePairs();

        if (!ok)
        {
            printf("Problem detaching from queue pairs\n");
        }
        else
        {
            printf("Detached from queue pairs successfully\n");
        }

        // Clear down.
        mHifList.clear();
        mServiceList.clear();
        mCuratorServiceExists = false;
        mBluetoothServiceExists = false;

        // Close the packet tap interface.
        ok = closePacketTapInterface();

        if (!ok)
        {
            printf("Problem closing packet tap interface\n");
        }
    }
}

///
/// Returns whether the packet tap interface is ready for service.
///
bool PtapTransport::ready(
    uint32 aTimeout)
{
    return mTransportReady;
}

///
/// Sends a PDU to the DUT.
///
void PtapTransport::sendpdu(
    const PDU& aPayload)
{
    FUNCTION_DEBUG_SENTRY;

    uint8 pBuffer[MAX_PTAP_PACKET_SIZE];

    // Prepend the HCI / ACL / SCO / Vendor message with a PTAP_DATA_DOWN header with appropriate class, SID and CONID fields.
    // For HCI (CONID: 0x0000), ACL (CONID: 0x1000), SCO (CONID: 0x2000) and Vendor (CONID: 0x3000) messages.
    pack2(PTAP_DATA_DOWN, pBuffer + 0);
    pack2(SERVICE_CLASS_BLUETOOTH, pBuffer + 2);
    pack2(mBluetoothServiceInfo.sId, pBuffer + 4);

    // Determine the channel for the PDU.
    ConnectionId conId;
    size_t headerLength;
    PDU::bc_channel channel = aPayload.channel();

    switch (channel)
    {
    case PDU::hciCommand:
        conId = HCI_CMD;
        pack2(conId, pBuffer + 6);
        headerLength = 8;
        break;

    case PDU::hciACL:
        conId = HCI_ACL;
        pack2(conId, pBuffer + 6);
        headerLength = 8;
        break;

    case PDU::hciSCO:
        conId = HCI_SCO;
        pack2(conId, pBuffer + 6);
        headerLength = 8;
        break;

    default: // Vendor-specific.
        conId = VENDOR;
        pack2(conId, pBuffer + 6);
        pack1(channel, pBuffer + 8);
        headerLength = 9;
        break;
    }

    // Size the PDU.
    size_t payloadLength = aPayload.size();
    size_t bytesToSend = headerLength + payloadLength;

    if (bytesToSend > MAX_PTAP_PACKET_SIZE)
    {
        printf("PDU data size too large\n");
        onTransportFailure();
    }

    // Append the payload.
    pack(aPayload.data(), pBuffer + headerLength, payloadLength);

    // Write the message to the packet tap interface.
    size_t bytesSent = writeToPtap(pBuffer, bytesToSend);

    if (bytesSent != bytesToSend)
    {
        printf("Failed to send all PDU data\n");
        onTransportFailure();
    }
    else
    {
        // Pass to watcher.
        Watched::sent(pBuffer, bytesSent);
    }
}

///
/// Set the DataWatcher which records and prints all sent and received data / packets.
///
void PtapTransport::setDataWatch(
    DataWatcher* aW)
{
    FUNCTION_DEBUG_SENTRY;

    Watched::set(aW);
}

///
/// onTransportFailure.
///
void PtapTransport::onTransportFailure()
{
    FUNCTION_DEBUG_SENTRY;

    onLinkFail(ptap_transport_fail);
}

///
/// onEvent.
///
void PtapTransport::onEvent(
    uint8* aPdu,
    uint32 aLength)
{
    FUNCTION_DEBUG_SENTRY;

    recvData(PDU::hciCommand, aPdu, aLength);
}

///
/// onACL.
///
void PtapTransport::onACL(
    uint8* aPdu,
    uint32 aLength)
{
    FUNCTION_DEBUG_SENTRY;

    recvData(PDU::hciACL, aPdu, aLength);
}

///
/// onSCO.
///
void PtapTransport::onSCO(
    uint8* aPdu,
    uint32 aLength)
{
    FUNCTION_DEBUG_SENTRY;

    recvData(PDU::hciSCO, aPdu, aLength);
}

///
/// onVendor.
///
void PtapTransport::onVendor(
    uint8 aChannel,
    uint8* aPdu,
    uint32 aLength)
{
    FUNCTION_DEBUG_SENTRY;

    recvData(PDU::bc_channel(aChannel), aPdu, aLength);
}


///
/// Map the Bluetooth service to a host interface.
///
bool PtapTransport::mapBluetoothServiceToHif()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = false;
    uint8 pBuffer[MAX_PTAP_PACKET_SIZE];
    size_t length;

    // Query for host interfaces.
    makeQueryHifsPdu(pBuffer, length);
    ok = writeToPtapCheckResp(pBuffer, length);

    if (!ok)
    {
        printf("Failed querying for host interfaces\n");
    }
    else
    {
        // Query for the Bluetooth service.
        ok = queryBluetoothService();

        if (!ok)
        {
            printf("Failed querying for Bluetooth service\n");
        }

        if (ok)
        {
            // Report the Curator and Bluetooth services found.
            printf("Found Curator service (sId:%04X) on requested host interface (hifId:%04X)\n", mCuratorServiceInfo.sId, mRequestedHifId);
            printf("Found Bluetooth service (sId:%04X) associated with Curator service (sId:%04X)\n", mBluetoothServiceInfo.sId, mBluetoothServiceInfo.curatorId);

            // If the Bluetooth service is available, make it unavailable.
            if ((mBluetoothServiceInfo.state & SERVICE_AVAILABILITY) == SERVICE_AVAILABLE)
            {
                MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) was available", mBluetoothServiceInfo.sId);

                // Make the Bluetooth service unavailable.
                makeServiceUnavailablePdu(mBluetoothServiceInfo.sId, pBuffer, length);
                ok = writeToPtapCheckResp(pBuffer, length);

                if (!ok)
                {
                    printf("Failed making Bluetooth service (sId:%04X) unavailable\n", mBluetoothServiceInfo.sId);
                }
                else
                {
                    // Query for the Bluetooth service.
                    ok = queryBluetoothService();

                    if (!ok)
                    {
                        printf("Failed querying for Bluetooth service\n");
                    }
                    else if ((mBluetoothServiceInfo.state & SERVICE_AVAILABILITY) == SERVICE_UNAVAILABLE)
                    {
                        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) successfully became unavailable", mBluetoothServiceInfo.sId);
                    }
                    else
                    {
                        printf("Bluetooth service (sId:%04X) failed to become unavailable\n", mBluetoothServiceInfo.sId);
                        ok = false;
                    }
                }
            }
            else
            {
                printf("Bluetooth service (sId:%04X) was already unavailable\n", mBluetoothServiceInfo.sId);
            }
        }

        if (ok)
        {
            // If the Bluetooth service is inactive, start it.
            if ((mBluetoothServiceInfo.state & SERVICE_STATE) == SERVICE_STATE_INACTIVE)
            {
                MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) was inactive", mBluetoothServiceInfo.sId);

                // Start the Bluetooth service.
                makeServiceStartPdu(mBluetoothServiceInfo.sId, pBuffer, length);
                ok = writeToPtapCheckResp(pBuffer, length);

                if (!ok)
                {
                    printf("Failed starting Bluetooth service (sId:%04X)\n", mBluetoothServiceInfo.sId);
                }
                else
                {
                    // Query for the Bluetooth service.
                    ok = queryBluetoothService();

                    if (!ok)
                    {
                        printf("Failed querying for Bluetooth service\n");
                    }
                    else if ((mBluetoothServiceInfo.state & SERVICE_STATE) == SERVICE_STATE_STARTED)
                    {
                        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) sucessfully started", mBluetoothServiceInfo.sId);
                    }
                    else
                    {
                        printf("Bluetooth service (sId:%04X) failed to start\n", mBluetoothServiceInfo.sId);
                        ok = false;
                    }
                }
            }
            else
            {
                printf("Bluetooth service (sId:%04X) was already started\n", mBluetoothServiceInfo.sId);
            }
        }

        if (ok)
        {
            // If the Bluetooth service is not mapped, map it.
            if ((mBluetoothServiceInfo.state & SERVICE_STATE) != SERVICE_STATE_MAPPED)
            {
                MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) was unmapped", mBluetoothServiceInfo.sId);

                // Map the Bluetooth service to the configured host interface.
                makeServiceMapPdu(mBluetoothServiceInfo.sId, pBuffer, length);
                ok = writeToPtapCheckResp(pBuffer, length);

                if (!ok)
                {
                    printf("Failed mapping Bluetooth service (sId:%04X)\n", mBluetoothServiceInfo.sId);
                }
                else
                {
                    // Query for the Bluetooth service.
                    ok = queryBluetoothService();

                    if (!ok)
                    {
                        printf("Failed querying for Bluetooth service\n");
                    }
                    else if ((mBluetoothServiceInfo.state & SERVICE_STATE) == SERVICE_STATE_MAPPED)
                    {
                        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Bluetooth service (sId:%04X) successfuly mapped", mBluetoothServiceInfo.sId);
                    }
                    else
                    {
                        printf("Bluetooth service (sId:%04X) failed to map\n", mBluetoothServiceInfo.sId);
                        ok = false;
                    }
                }
            }
            else
            {
                printf("Bluetooth service (sId:%04X) was already mapped\n", mBluetoothServiceInfo.sId);
            }
        }
    }

    return ok;
}

///
/// Query for the Bluetooth service.
///
bool PtapTransport::queryBluetoothService()
{
    FUNCTION_DEBUG_SENTRY;

    bool ok;
    uint8 pBuffer[MAX_PTAP_PACKET_SIZE];
    size_t length = 0;

    // Query for services.
    makeQueryServicesPdu(pBuffer, length);
    ok = writeToPtapCheckResp(pBuffer, length);

    if (!ok)
    {
        printf("Failed querying for services\n");
    }
    else if (!mCuratorServiceExists)
    {
        printf("Curator service could not be found on requested host interface (hifId:%04X)\n", mRequestedHifId);
        ok = false;
    }
    else if (!mBluetoothServiceExists)
    {
        printf("Bluetooth service could not be found\n");
        ok = false;
    }

    return ok;
}

///
/// Find the HifType of the host interface with the given hifId.
///
PtapTransport::HifType PtapTransport::getHifType(
    uint16 aHifId)
{
    FUNCTION_DEBUG_SENTRY;

    HifType hifType = HIF_TYPE_RESERVED;
    std::vector<HifInfo>::iterator it;

    for (it = mHifList.begin(); it < mHifList.end(); it++)
    {
        HifInfo hifInfo = *it;

        if (hifInfo.hifId == aHifId)
        {
            hifType = hifInfo.type;
            break;
        }
    }

    return hifType;
}

///
/// Sets up the four Bluetooth channels (Rx/Tx queue pairs).
/// In the Hydra driver each queue pair is identified by the service id (sId)
/// and the connection id (conId).
/// For the Bluetooth service there are four connection ids corresponding to
/// the four Bluetooth channels: HCI command, ACL, SCO, Vendor.
///
bool PtapTransport::attachToQueuePairs()
{
    FUNCTION_DEBUG_SENTRY;

    uint8 pBuffer[MAX_PTAP_PACKET_SIZE];
    size_t length;
    bool allOk = true;

    const ConnectionId CONNECTION_IDS[] =
    {
        HCI_CMD,
        HCI_ACL,
        HCI_SCO,
        VENDOR
    };

    for (int index = 0; index < 4; ++index)
    {
        // Attach to the queue pair.
        makeQueuePairAttachPdu(mBluetoothServiceInfo.sId, CONNECTION_IDS[index], pBuffer, length);
        bool ok = writeToPtapCheckResp(pBuffer, length);

        if (!ok)
        {
            printf("Failed attaching to queue pair (conId:%04X)\n", CONNECTION_IDS[index]);
            allOk = false;
        }
        else
        {
            MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Attached to queue pair (conId:%04X)", CONNECTION_IDS[index]);
        }
    }

    return allOk;
}

///
/// Clears down the four Bluetooth channels (Rx/Tx queue pairs).
///
bool PtapTransport::detachFromQueuePairs()
{
    FUNCTION_DEBUG_SENTRY;

    uint8 pBuffer[MAX_PTAP_PACKET_SIZE];
    size_t length;
    bool allOk = true;

    const ConnectionId CONNECTION_IDS[] =
    {
        HCI_CMD,
        HCI_ACL,
        HCI_SCO,
        VENDOR
    };

    for (int index = 0; index < 4; ++index)
    {
        // Detach from the queue pair.
        makeQueuePairDetachPdu(mBluetoothServiceInfo.sId, CONNECTION_IDS[index], pBuffer, length);
        bool ok = writeToPtapCheckResp(pBuffer, length);

        if (!ok)
        {
            printf("Failed detaching from queue pair (conId:%04X)\n", CONNECTION_IDS[index]);
            allOk = false;
        }
        else
        {
            MSG_HANDLER_NOTIFY_DEBUG(DEBUG_BASIC, "Detached from queue pair (conId:%04X)", CONNECTION_IDS[index]);
        }
    }

    return allOk;
}

///
/// Initialise a PTAP_QUERY_HIFS pdu.
///
void PtapTransport::makeQueryHifsPdu(
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_QUERY_HIFS, apBuffer + 0);
    aLength = 2;
}

///
/// Initialise a PTAP_QUERY_SERVICES pdu.
///
void PtapTransport::makeQueryServicesPdu(
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_QUERY_SERVICES, apBuffer + 0);
    aLength = 2;
}

///
/// Initialise a PTAP_SERVICE_UNAIVAILABLE pdu.
///
void PtapTransport::makeServiceUnavailablePdu(
    uint16 aServiceId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_SERVICE_UNAVAILABLE, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    aLength = 4;
}

///
/// Initialise a PTAP_SERVICE_START pdu.
///
void PtapTransport::makeServiceStartPdu(
    uint16 aServiceId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_SERVICE_START, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    aLength = 4;
}

///
/// Initialise a PTAP_SERVICE_MAP pdu.
///
void PtapTransport::makeServiceMapPdu(
    uint16 aServiceId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_SERVICE_MAP, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    aLength = 4;
}

///
/// Initialise a PTAP_SERVICE_UNMAP pdu.
///
void PtapTransport::makeServiceUnmapPdu(
    uint16 aServiceId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_SERVICE_UNMAP, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    aLength = 4;
}

///
/// Initialise a PTAP_QUEUE_PAIR_ATTACH pdu.
///
void PtapTransport::makeQueuePairAttachPdu(
    uint16 aServiceId,
    uint16 aConId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_QUEUE_PAIR_ATTACH, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    pack2(aConId, apBuffer + 4);
    pack2(QPAIR_TOHOST_BUFFER_COUNT, apBuffer + 6);
    pack4(QPAIR_TOHOST_BUFFER_SIZE, apBuffer + 8);
    aLength = 12;
}

///
/// Initialise a PTAP_QUEUE_PAIR_DETACH pdu.
///
void PtapTransport::makeQueuePairDetachPdu(
    uint16 aServiceId,
    uint16 aConId,
    uint8* apBuffer,
    size_t& aLength)
{
    pack2(PTAP_QUEUE_PAIR_DETACH, apBuffer + 0);
    pack2(aServiceId, apBuffer + 2);
    pack2(aConId, apBuffer + 4);
    aLength = 6;
}

///
/// Packing / unpacking inline methods.
///
inline void PtapTransport::pack1(
    uint8 aSource,
    uint8* apDestination)
{
    *apDestination = aSource;
}

inline void PtapTransport::unpack1(
    const uint8* apSource,
    uint8* apDestination)
{
    *apDestination = *apSource;
};

inline void PtapTransport::pack2(
    uint16 aSource,
    uint8* apDestination)
{
    *(apDestination + 0) = (uint8)((aSource & 0x00FF) >> 0);
    *(apDestination + 1) = (uint8)((aSource & 0xFF00) >> 8);
}

inline void PtapTransport::unpack2(
    const uint8* apSource,
    uint16* apDestination)
{
    *apDestination = (uint16)(
        *(apSource + 1) << 8 |
        *(apSource + 0) << 0);
};

inline void PtapTransport::unpack2(
    const uint8* apSource,
    int* apDestination)
{
    *apDestination = (int)(
        *(apSource + 1) << 8 |
        *(apSource + 0) << 0);
};

inline void PtapTransport::pack4(
    uint32 aSource,
    uint8* apDestination)
{
    *(apDestination + 0) = (uint8)((aSource & 0x000000FF) >> 0);
    *(apDestination + 1) = (uint8)((aSource & 0x0000FF00) >> 8);
    *(apDestination + 2) = (uint8)((aSource & 0x00FF0000) >> 16);
    *(apDestination + 3) = (uint8)((aSource & 0xFF000000) >> 24);
}

inline void PtapTransport::unpack4(
    const uint8* apSource,
    uint32* apDestination)
{
    *apDestination = (uint32)(
        *(apSource + 3) << 24 |
        *(apSource + 2) << 16 |
        *(apSource + 1) << 8 |
        *(apSource + 0) << 0);
};

inline void PtapTransport::pack(
    const uint8* apSource,
    void* apDestination,
    size_t aCount)
{
    memcpy(apDestination, apSource, aCount);
};

inline void PtapTransport::unpack(
    const uint8* apSource,
    void* apDestination,
    size_t aCount)
{
    memcpy(apDestination, apSource, aCount);
};

///
/// Write a message to the packet tap interface and wait for a response.
///
bool PtapTransport::writeToPtapCheckResp(
    uint8* apBuffer,
    size_t aLength)
{
    FUNCTION_DEBUG_SENTRY;

    bool ok;

    // Write the message.
    size_t sentBytes = writeToPtap(apBuffer, aLength);

    // Wait for a response.
    ok = waitForRx(PTAP_RX_TIMEOUT_MS);

    if (!ok)
    {
        printf("PtapTransport::waitForRx() failed\n");
    }
    else if (sentBytes != aLength)
    {
        printf("Failed to write the requested amount of data to packet tap interface\n");
        ok = false;
    }
    else if (mRxStatusCode != PTAP_STATUS_SUCCESS)
    {
        printf("Non-success response status code (%04X) from packet tap interface\n", mRxStatusCode);
        ok = false;
    }

    return ok;
}

///
/// Block until a response has been received.
///
bool PtapTransport::waitForRx(
    unsigned int aMilliSecs)
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = mRxCompleteSignal.wait(aMilliSecs);
    mRxCompleteSignal.unset();

    return ok;
}

///
/// Process all received messages.
/// The origin might be the Hydra driver or the chip / DUT.
///
bool PtapTransport::processRx(
    uint8* apBuffer,
    int aLength)
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;

#ifdef DEBUG_PTAP
    dumpBuffer(apBuffer, aLength, true);
#endif //DEBUG_PTAP

    // Check the message length is sensible.
    if (aLength < 2)
    {
        printf("Received message too short\n");
        dumpBuffer(apBuffer, aLength, true);

        return false;
    }

    // Unpack the header.
    uint16 header;
    unpack2(apBuffer + 0, &header);

    // The message is a request if bit 15 is 0 or a response if bit 15 is 1.
    bool requestMessage = (header & MESSAGE_SENSE) == MESSAGE_REQUEST;
    bool responseMessage = (header & MESSAGE_SENSE) == MESSAGE_RESPONSE;

    // The message type is in bits 14:0.
    MessageType messageType = (MessageType)(header & MESSAGE_TYPE);

    if (
        responseMessage &&
        aLength >= 4)
    {
        //
        // The message is a packet tap response.
        //

        // Unpack the status code.
        unpack2(apBuffer + 2, (int*)&mRxStatusCode);

        if (mRxStatusCode != PTAP_STATUS_SUCCESS)
        {
            MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Response (%04X) with non-success status code (%04X)", messageType, mRxStatusCode);
        }

        // Unpack the rest of the message.
        switch (messageType)
        {
        case PTAP_QUERY_HIFS:
            ok = processQueryHifs(apBuffer, aLength);
            break;

        case PTAP_QUERY_SERVICES:
            ok = processQueryServices(apBuffer, aLength);
            break;

        case PTAP_SERVICE_UNAVAILABLE:
        case PTAP_SERVICE_START:
        case PTAP_SERVICE_MAP:
        case PTAP_QUEUE_PAIR_ATTACH:
        case PTAP_QUEUE_PAIR_DETACH:
            break;

        default:
            printf("Unexpected response message type '%04X'\n", messageType);
            break;
        }

        // Signal that processing the response has finished so the next data can be sent.
        mRxCompleteSignal.set();
    }

    else if (
        requestMessage &&
        (messageType == PTAP_DATA_DOWN || messageType == PTAP_DATA_UP) &&
        aLength >= 8)
    {
        //
        // The message is data from the DUT (to-host).
        //

        // Unpack and validate the service class.
        ServiceClass serviceClass;
        unpack2(apBuffer + 2, (int*)&serviceClass);
        if (serviceClass != SERVICE_CLASS_BLUETOOTH)
        {
            printf("Service class (%04X) is NOT Bluetooth service class\n", serviceClass);
            ok = false;
        }

        // Unpack and validate the service id.
        uint16 serviceId;
        unpack2(apBuffer + 4, &serviceId);
        if (serviceId != mBluetoothServiceInfo.sId)
        {
            printf("Service id (%04X) is NOT Bluetooth service id\n", serviceId);
            ok = false;
        }

        if (ok)
        {
            // Unpack the connection id.
            uint16 conId;
            unpack2(apBuffer + 6, &conId);

            // Dispatch the payload according to connection id.
            uint8* pPayload = apBuffer + 8;
            size_t payloadLength = aLength - 8;

            switch (conId)
            {
            case HCI_CMD:
                onEvent(pPayload, (uint32)payloadLength);
                break;

            case HCI_ACL:
                onACL(pPayload, (uint32)payloadLength);
                break;

            case HCI_SCO:
                onSCO(pPayload, (uint32)payloadLength);
                break;

            case VENDOR:
                // The first octet should be the channel of the PDU.
                onVendor(pPayload[0], pPayload + 1, (uint32)(payloadLength - 1));
                break;

            default:
                printf("Connection id (%04X) is unknown\n", conId);
                break;
            }

             // Pass to watcher.
            Watched::recv(pPayload, payloadLength);
        }
    }

    else
    {
        //
        // The message is unrecognised.
        //

        printf("Received message unrecognised\n");
        dumpBuffer(apBuffer, aLength, true);
        ok = false;
    }

    return ok;
}

///
/// Process a PTAP_QUERY_HIFS response.
///
bool PtapTransport::processQueryHifs(
    uint8* apBuffer,
    int aLength)
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;

    // Clear the cache of host interfaces.
    mHifList.clear();

    // Calculate the number of host interfaces returned.
    unsigned int hifCount = (aLength - HIF_HEADER_LENGTH) / HIF_INFO_LENGTH;

    // Cache each host interface.
    for (unsigned int index = 0; index < hifCount; index++)
    {
        HifInfo hifInfo;

        uint16 offset = HIF_HEADER_LENGTH + index * HIF_INFO_LENGTH;

        unpack2(apBuffer + offset + 0, &hifInfo.hifId);
        unpack2(apBuffer + offset + 2, (int*)&hifInfo.type);
        unpack(apBuffer + offset + 4, hifInfo.name, 32);

        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Host interface found (%04X %04X \"%s\")", hifInfo.hifId, hifInfo.type, hifInfo.name);

        try
        {
            mHifList.push_back(hifInfo);
        }
        catch (...)
        {
            printf("An exception was encountered from std::vector<HifInfo>::push_back()\n");
            ok = false;
        }
    }

    if (hifCount == 0)
    {
        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "No host interfaces found");
    }

    return ok;
}

///
/// Process a PTAP_QUERY_SERVICES response.
///
bool PtapTransport::processQueryServices(
    uint8* apBuffer,
    int aLength)
{
    FUNCTION_DEBUG_SENTRY;

    bool ok = true;

    // Clear the cache of services.
    mServiceList.clear();

    // Calculate the number of services returned.
    unsigned int serviceCount = (aLength - SERVICE_HEADER_LENGTH) / SERVICE_INFO_LENGTH;

    // Cache each service.
    for (unsigned int index = 0; index < serviceCount; index++)
    {
        ServiceInfo serviceInfo;

        uint16 offset = SERVICE_HEADER_LENGTH + index * SERVICE_INFO_LENGTH;

        unpack2(apBuffer + offset + 0, &serviceInfo.sId);
        unpack2(apBuffer + offset + 2, &serviceInfo.curatorId);
        unpack2(apBuffer + offset + 4, (int*)&serviceInfo.serviceClass);
        unpack2(apBuffer + offset + 6, (int*)&serviceInfo.state);
        unpack2(apBuffer + offset + 8, &serviceInfo.hifId);

        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Service found (%04X %04X %04X %04X %04X)", serviceInfo.sId, serviceInfo.curatorId, serviceInfo.serviceClass, serviceInfo.state, serviceInfo.hifId);

        try
        {
            mServiceList.push_back(serviceInfo);
        }
        catch (...)
        {
            printf("An exception was encountered from std::vector<ServiceInfo>::push_back()\n");
            ok = false;
        }
    }

    if (serviceCount == 0)
    {
        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "No services found");
    }

    // Look for the first Curator service found on the requested host interface.
    mCuratorServiceExists = false;
    for (std::vector<ServiceInfo>::iterator it = mServiceList.begin(); it < mServiceList.end(); it++)
    {
        ServiceInfo serviceInfo = *it;

        if (
            serviceInfo.serviceClass == SERVICE_CLASS_CURATOR &&
            serviceInfo.hifId == mRequestedHifId)
        {
            MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Found Curator service (sId:%04X) on requested host interface (hifId:%04X)", serviceInfo.sId, mRequestedHifId);

            mCuratorServiceInfo = serviceInfo;
            mCuratorServiceExists = true;

            break;
        }
    }

    if (!mCuratorServiceExists)
    {
        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Curator service could not be found on requested host interface (hifId:%04X)", mRequestedHifId);
    }

    // Look for the Bluetooth service associated with the Curator service on the requested host interface.
    mBluetoothServiceExists = false;
    for (std::vector<ServiceInfo>::iterator it = mServiceList.begin(); it < mServiceList.end(); it++)
    {
        ServiceInfo serviceInfo = *it;

        if (
            serviceInfo.serviceClass == SERVICE_CLASS_BLUETOOTH &&
            serviceInfo.curatorId == mCuratorServiceInfo.sId)
        {
            MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Found Bluetooth service (sId:%04X) associated with Curator service (sId:%04X)", serviceInfo.sId, serviceInfo.curatorId);

            mBluetoothServiceInfo = serviceInfo;
            mBluetoothServiceExists = true;

            break;
        }
    }

    if (!mBluetoothServiceExists)
    {
        MSG_HANDLER_NOTIFY_DEBUG(DEBUG_ENHANCED, "Bluetooth service could not be found");
    }

    return ok;
}

///
/// Print the buffer contents in hex.
///
void PtapTransport::dumpBuffer(
    uint8* apBuffer,
    size_t aLength,
    bool aRx)
{
    aRx ? printf("Rx:") : printf("Tx:");

    for (unsigned int index = 0; index < aLength; ++index)
    {
        printf(" %02X", apBuffer[index]);
    }

    printf("\n");
}

///////////////////////////////////////////////////////////////////////
// Transport configuration objects.
///////////////////////////////////////////////////////////////////////

///
/// Constructor.
///
PtapConfiguration::PtapConfiguration(
    const char* aComName,
    UARTConfiguration aConf,
    bool aTunnel)
    :
    TransportConfiguration(aTunnel),
    mHifType(PtapTransport::HIF_TYPE_SDIO),
    mComName(aComName),
    mTransportConf(aConf),
    mName("Packet tap name"),
    mSelectedCuratorHifId(atoi(aComName))
{
    FUNCTION_DEBUG_SENTRY;

    printf("Packet tap on host interface %04X requested\n", mSelectedCuratorHifId);
}

///
/// Constructor.
///
PtapConfiguration::PtapConfiguration(
    const wchar_t *aComName,
    UARTConfiguration aConf,
    bool aTunnel)
    :
    TransportConfiguration(aTunnel),
    mHifType(PtapTransport::HIF_TYPE_SDIO),
    mComName(aComName),
    mTransportConf(aConf),
    mName("Packet tap name"),
    mSelectedCuratorHifId(0)
{
    FUNCTION_DEBUG_SENTRY;

    std::wstring inStr = aComName;
    std::wistringstream wstrm;
    wstrm.str(inStr);
    wstrm >> mSelectedCuratorHifId;

    printf("Packet tap on host interface %04X requested\n", mSelectedCuratorHifId);
}

///
/// Clone.
///
TransportConfiguration* PtapConfiguration::clone() const
{
    FUNCTION_DEBUG_SENTRY;

    return new PtapConfiguration(*this);
}

///
/// Creates the requested packet tap transport object depending on platform and parameters.
///
BlueCoreTransportImplementation* PtapConfiguration::make(
    const CallBacks& aRecv) const
{
    FUNCTION_DEBUG_SENTRY;

#ifdef WIN32
    return new PtapTransportWin(*this , aRecv);
#else //WIN32
    return new PtapTransportLinux(*this, aRecv);
#endif //WIN32
}
