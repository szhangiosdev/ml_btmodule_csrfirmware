//******************************************************************************
//
//  ptaptransport.h
//
//  Copyright � Cambridge Silicon Radio Ltd.
//
//  Transport using the packet tap interface of the Hydra driver for Hydra chips.
//
//  Refer to CS-211901 for the Packet Tap Specifiction.
//
//  Note that all fields in the packet tap protocol are little-endian.
//
//******************************************************************************

#ifndef HEADER_PTAPTRANSPORT_H
#define HEADER_PTAPTRANSPORT_H

#ifdef WIN32
#include <windows.h>
#include <tchar.h>
#endif //WIN32

#include "thread/critical_section.h"
#include "thread/signal_box.h"
#include "thread/thread.h"
#include "transportimplementation.h"

///
/// Transport using the packet tap protocol of the Hydra chip driver.
/// Hydra chips are combo chips, i.e. contain more than one service,
/// service being: Bluetooth, Wi-Fi, nfc, uwb, audio, gnss, etc.
/// The Hydra driver provides an abstraction of all types of host interfaces
/// (uart, usb, sdio, etc.) to be used with a chip service.
/// It also provides a way to manage the services in the chip and
/// map them to a particular host interface, e.g. start the bluetooth service
/// and map it to the SDIO host interface.
/// This service management commands will end up to the 'curator', a special 
/// Hydra chip service for managing the chip and its services.
/// Once bluetooth is started and mapped to a host interface, btcli can
/// work as in pre-hydra bluetooth chips.
/// 
/// Packet tap protocol is described in cognidox CS-211901-SP
/// and also: http://wiki/Hydra_drivers/Packet_taps
/// 
/// This class is modeled like the SDIOTransport class but it does not
/// own a 'stack' object. Instead the stack functionality is encapsulated
/// in the same (PtapTransport) class.
/// 
class PtapTransport
    :
    public BlueCoreTransportImplementation,
    public Threadable,
    public Watched
{
public:
    static const uint16 MAX_PTAP_PACKET_SIZE = 8 * 1024;
    static const uint16 QPAIR_TOHOST_BUFFER_COUNT = 8;
    static const uint32 QPAIR_TOHOST_BUFFER_SIZE = 8 * 1024;
    static const uint16 PTAP_SDIO_FUNCTION_NUMBER = 0x02;
    static const unsigned int PTAP_RX_TIMEOUT_MS = 7000;

    enum HifType
        //: uint16 // enum sizing is not supported by the current fc5 compiler, g++ (GCC) 4.1.1 20070105.
    {
        HIF_TYPE_RESERVED = 0x0000,
        HIF_TYPE_SDIO     = 0x0001,
        HIF_TYPE_UART     = 0x0002,
        HIF_TYPE_USB      = 0x0003,
        HIF_TYPE_I2C      = 0x0004,
        HIF_TYPE_PCIE     = 0x0005
    };

    enum ServiceClass
        //: uint16
    {
        SERVICE_CLASS_MISC      = 0x0000,
        SERVICE_CLASS_BLUETOOTH = 0x0100,
        SERVICE_CLASS_WLAN      = 0x0200,
        SERVICE_CLASS_GNSS      = 0x0300,
        SERVICE_CLASS_NFC       = 0x0400,
        SERVICE_CLASS_AUDIO     = 0x0500,
        SERVICE_CLASS_KALIMBA   = 0x0600,
        SERVICE_CLASS_ARM       = 0x0700,
        SERVICE_CLASS_CURATOR   = 0xFF00
    };

    enum MessageType
        //: uint16
    {
        PTAP_DATA_DOWN           = 0x0000,
        PTAP_DATA_UP             = 0x0001,
        PTAP_QUERY_HIFS          = 0x0010,
        PTAP_QUERY_SERVICES      = 0x0011,
        PTAP_QUERY_STATS         = 0x0012,
        PTAP_SERVICE_ADD         = 0x0020,
        PTAP_SERVICE_DEL         = 0x0021,
        PTAP_SERVICE_MAP         = 0x0022,
        PTAP_SERVICE_UNMAP       = 0x0023,
        PTAP_SERVICE_START       = 0x0024,
        PTAP_SERVICE_STOP        = 0x0025,
        PTAP_SERVICE_AVAILABLE   = 0x0026,
        PTAP_SERVICE_UNAVAILABLE = 0x0027,
        PTAP_QUEUE_PAIR_ATTACH   = 0x0030,
        PTAP_QUEUE_PAIR_DETACH   = 0x0031
    };

    enum MessageMask
        //: uint16
    {
        MESSAGE_TYPE  = 0x7FFF,
        MESSAGE_SENSE = 0x8000
    };

    enum MessageSense
        //: uint16
    {
        MESSAGE_REQUEST  = 0x0000,
        MESSAGE_RESPONSE = 0x8000
    };

    enum ServiceState
        //: uint16
    {
        SERVICE_STATE_INACTIVE = 0x0000,
        SERVICE_STATE_STARTED  = 0x0001,
        SERVICE_STATE_MAPPED   = 0x0002
    };

    enum ServiceMask
        //: uint16
    {
        SERVICE_STATE        = 0x007F,
        SERVICE_AVAILABILITY = 0x0080
    };

    enum ServiceAvailability
        //: uint16
    {
        SERVICE_UNAVAILABLE = 0x0000,
        SERVICE_AVAILABLE   = 0x0080
    };

    enum ConnectionId
        //: uint16
    {
        HCI_CMD = 0x0000,
        HCI_ACL = 0x1000,
        HCI_SCO = 0x2000,
        VENDOR  = 0x3000
    };

    enum StatusCode
        //: uint16
    {
        PTAP_STATUS_SUCCESS           = 0x0000,
        PTAP_STATUS_NOT_FOUND         = 0x0001,
        PTAP_STATUS_NO_MEMORY         = 0x0002,
        PTAP_STATUS_INVALID_PARAM     = 0x0003,
        PTAP_STATUS_INVALID_STATE     = 0x0004,
        PTAP_STATUS_NOT_SUPPORTED     = 0x0005,
        PTAP_STATUS_NOT_READY         = 0x0006,
        PTAP_STATUS_UNKNOWN_INTERFACE = 0x0007,
        PTAP_STATUS_TIMEOUT           = 0x0008,
        PTAP_STATUS_DEVICE_BUSY       = 0x0009,
        PTAP_STATUS_DEVICE_ERROR      = 0x000A,
        PTAP_STATUS_NO_DEVICE         = 0x000B,
        PTAP_STATUS_UNKNOWN_ERROR     = 0xFFFF,
    }; 

    typedef struct HifInfo
    {
        uint16 hifId;
        HifType type;
        char name[32];
    } HifInfo;

    typedef struct ServiceInfo
    {
        uint16 sId;
        uint16 curatorId;
        ServiceClass serviceClass;
        ServiceState state;
        uint16 hifId;
    } ServiceInfo;

public:
    PtapTransport(const PtapConfiguration& aConfig, const CallBacks& aRecv);
    virtual ~PtapTransport();

    bool start();
    virtual void stop();
    bool ready(uint32 aTimeout);

    virtual void sendpdu(const PDU& aPacket);

    void setDataWatch(DataWatcher* aW);

    void onTransportFailure();
    void onEvent(uint8* aPdu, uint32 aLength);
    void onACL(uint8* aPdu, uint32 aLength);
    void onSCO(uint8* aPdu, uint32 aLength);
    void onVendor(uint8 aChannel, uint8* aPdu, uint32 aLength);
    static void onPacketSent(bool aDone, void* aParameters);

    int ThreadFunc() = 0;

protected:
    static const int HIF_HEADER_LENGTH = 4;
    static const int HIF_INFO_LENGTH = 36;
    static const int SERVICE_HEADER_LENGTH = 4;
    static const int SERVICE_INFO_LENGTH = 10;

    virtual bool openPacketTapInterface() = 0;
    virtual bool closePacketTapInterface() = 0;

    bool mapBluetoothServiceToHif();
    bool queryBluetoothService();
    HifType getHifType(uint16 aHifId);
    virtual bool attachToQueuePairs();
    virtual bool detachFromQueuePairs();

    void makeQueryHifsPdu(uint8* apBuffer, size_t& aLength);
    void makeQueryServicesPdu(uint8* apBuffer, size_t& aLength);
    void makeServiceUnavailablePdu(uint16 aServiceId, uint8* apBuffer, size_t& aLength);
    void makeServiceStartPdu(uint16 aServiceId, uint8* apBuffer, size_t& aLength);
    void makeServiceMapPdu(uint16 aServiceId, uint8* apBuffer, size_t& aLength);
    void makeServiceUnmapPdu(uint16 aServiceId, uint8* apBuffer, size_t& aLength);
    void makeQueuePairAttachPdu(uint16 aServiceId, uint16 aConId, uint8* apBuffer, size_t& aLength);
    void makeQueuePairDetachPdu(uint16 aServiceId, uint16 aConId, uint8* apBuffer, size_t& aLength);

    bool writeToPtapCheckResp(uint8* apBuffer, size_t aLength);
    virtual size_t writeToPtap(uint8* apBuffer, size_t aLength) = 0;
    bool waitForRx(unsigned int aMillisecs);
    bool processRx(uint8* apBuffer, int aLength);
    bool processQueryHifs(uint8* apBuffer, int aLength);
    bool processQueryServices(uint8* apBuffer, int aLength);
    void dumpBuffer(uint8* apBuffer, size_t aLength, bool aRx);

    void pack1(uint8 aSource, uint8* apDestination);
    void unpack1(const uint8* apSource, uint8* apDestination);
    void pack2(uint16 aSource, uint8* apDestination);
    void unpack2(const uint8* apSource, uint16* apDestination);
    void unpack2(const uint8* apSource, int* apDestination);
    void pack4(uint32 aSource, uint8* apDestination);
    void unpack4(const uint8* apSource, uint32* apDestination);
    void pack(const uint8* apSource, void* apDestination, size_t aCount);
    void unpack(const uint8* apSource, void* apDestination, size_t aCount);

    int mRequestedHifId;
    std::vector<HifInfo> mHifList;
    std::vector<ServiceInfo> mServiceList;
    ServiceInfo mCuratorServiceInfo;
    ServiceInfo mBluetoothServiceInfo;
    HifType mBluetoothServiceHifType;
    bool mCuratorServiceExists;
    bool mBluetoothServiceExists;
    bool mTransportReady;
    StatusCode mRxStatusCode;
    SingleSignal mRxCompleteSignal;
};

#ifdef WIN32
///
/// Windows-specific functionality of the PtapTransport class.
/// Encapsulates what is specific to the Windows Hydra driver.
///
class PtapTransportWin
    :
    public PtapTransport
{
public:
    PtapTransportWin(const PtapConfiguration& aConf, const CallBacks& aCallBacks);
    virtual ~PtapTransportWin();

    int ThreadFunc();

protected:
    virtual bool openPacketTapInterface();
    virtual bool closePacketTapInterface();

    bool findInterface(std::wstring& aDevice, const std::wstring& aInterface);

    virtual size_t writeToPtap(uint8* apBuffer, size_t aLength);

    HANDLE mFile;
    std::wstring mInterface;
    HANDLE mReadStopEvent;
};

#else //WIN32
///
/// Linux-specific functionality of the PtapTransport class.
/// Encapsulates what is specific to the Linux Hydra driver.
///
class PtapTransportLinux
    :
    public PtapTransport
{
public:
    PtapTransportLinux(const PtapConfiguration& aConf, const CallBacks& aCallBacks);
    virtual ~PtapTransportLinux();

    int ThreadFunc();

protected:
    virtual bool openPacketTapInterface();
    virtual bool closePacketTapInterface();

    virtual size_t writeToPtap(uint8* apBuffer, size_t aLength);

    int mPtapDeviceFileDescr;
};

#endif //WIN32

#endif //HEADER_PTAPTRANSPORT_H
