#ifndef NETWORK_FLOW_CONTROL_RECEIVER_H
#define NETWORK_FLOW_CONTROL_RECEIVER_H

#include "thread/thread.h"

#include "defines.h"
#include "flow_control_receiver.h"

class FlowControl;

class NetworkFlowControlReceiver : public Threadable, public FlowControlReceiver
{
public:
    NetworkFlowControlReceiver(short port, FlowControl &fc);
    ~NetworkFlowControlReceiver();
    bool isConnected();
    int ThreadFunc();
private:
    virtual credit_t doUseCredits();
    int sockfd;
    FlowControl &flowControl;
};

#endif

