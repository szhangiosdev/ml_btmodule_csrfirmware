#include <strings.h>
#include <cassert>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "network_flow_control_sender.h"

NetworkFlowControlSender::NetworkFlowControlSender
    (const char *const name, const char *const port) :
        sockfd(-1)
{
    // convert name to usable address
    struct addrinfo *ai = NULL;
    struct addrinfo hints;
    bzero(&hints, sizeof(hints));
    hints.ai_flags = AI_NUMERICSERV | AI_CANONNAME;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    if (getaddrinfo(name, port, &hints, &ai) == 0)
    {
        const int fd(socket(AF_INET, SOCK_DGRAM, 0));
        if (fd >= 0)
        {
            if (connect(fd, ai->ai_addr, sizeof(*ai->ai_addr)) == 0)
            {
                sockfd = fd;
            }
            else
            {
                close(fd);
            }
        }
        freeaddrinfo(ai);
    }
}

bool NetworkFlowControlSender::isConnected()
{
    assert(this);
    return sockfd >= 0;
}


void NetworkFlowControlSender::doAddCredits(credit_t credits)
{
    assert(this);
    credit_t ncredits(htonl(credits));
    int n = send(sockfd, &ncredits, sizeof(ncredits), 0);
}

