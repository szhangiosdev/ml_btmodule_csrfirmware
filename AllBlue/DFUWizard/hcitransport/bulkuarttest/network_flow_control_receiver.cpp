#include <cstdio>
#include <cassert>

#include <strings.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "network_flow_control_receiver.h"
#include "flow_control.h"

NetworkFlowControlReceiver::NetworkFlowControlReceiver(const short port, 
                                                       FlowControl &fc) :
    sockfd(-1),
    flowControl(fc)
{
    const int fd(socket(AF_INET, SOCK_DGRAM, 0));

    if (fd >= 0)
    {
        struct sockaddr_in addr;
        bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        addr.sin_port = htons(port);
        if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0)
        {
            // error occurred binding. Clean up.
            close(fd);
        }
        else
        {
            sockfd = fd;
        }
    }
}

NetworkFlowControlReceiver::~NetworkFlowControlReceiver()
{
    printf("stopping thread");
    Kill();
}

bool NetworkFlowControlReceiver::isConnected()
{
    assert(this);
    return sockfd >= 0;
}

int NetworkFlowControlReceiver::ThreadFunc()
{
    assert(this);
    while (KeepGoing())
    {
        credit_t ncredits;
        credit_t credits;

        char data[1000];

        int n = recv(sockfd, &ncredits, sizeof(ncredits), 0);
        credits = ntohl(ncredits);
        flowControl.addCredits(credits);
    }
    return 0;
}

credit_t NetworkFlowControlReceiver::doUseCredits()
{
    assert(this);
    return flowControl.useCredits();
}

