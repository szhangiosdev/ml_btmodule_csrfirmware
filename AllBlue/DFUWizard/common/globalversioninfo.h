#ifndef STATICVERSIONINFO_H
#define STATICVERSIONINFO_H

#define VERSION_SHORT_COMPANY_NAME        			"Jarre Technologies"
#define VERSION_LONG_COMPANY_NAME         			"Jarre Technologies"
#define VERSION_COPYRIGHT_NO_YEAR					"Copyright (C) " VERSION_YEAR ", " VERSION_LONG_COMPANY_NAME
#define VERSION_COPYRIGHT_START_YEAR(startyear)		"Copyright (C) " startyear "-" VERSION_YEAR ", " VERSION_LONG_COMPANY_NAME
#define VERSION_YEAR                    "2015"
#define VERSION_PRODUCT                 "AeroUpdate"
#define VERSION_SPECIAL_BUILD           ""
#define VERSION_APP_MAJOR           1
#define VERSION_APP_MINOR           3
#define VERSION_APP_REVISION        18
#define VERSION_APP_BUILD           0

/* The string below should not exceed 8 chars */
#define VERSION_APP_FULL_STR		"1.3.18"

#define VERSION_APP_FULL_NUMERIC        VERSION_APP_MAJOR,VERSION_APP_MINOR,VERSION_APP_REVISION,VERSION_APP_BUILD


#endif
