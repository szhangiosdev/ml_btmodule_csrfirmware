#include "stdafx.h"
#include "DFUWizard.h"
#include "PageResults.h"

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

// Vertical gap between repositioned controls
static const int vertical_gap = 5;

IMPLEMENT_DYNCREATE(PageResults, DFUPage)

// Constructor
PageResults::PageResults() : DFUPage(PageResults::IDD)
{
	//{{AFX_DATA_INIT(PageResults)
	valueTitle = _T("");
	//}}AFX_DATA_INIT

	valueDetails = _T("");

	// Window caption not flashing initially
	flashTimer = 0;
}

// Data exchange
void PageResults::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PageResults)
	DDX_Control(pDX, IDC_PAGE_RESULTS_TITLE, staticTitle);
	DDX_Control(pDX, IDC_PAGE_RESULTS_DETAILS_BUTTON, buttonDetails);
	DDX_Control(pDX, IDC_PAGE_RESULTS_EDIT, editResults);
	//}}AFX_DATA_MAP
}

// Message map
BEGIN_MESSAGE_MAP(PageResults, DFUPage)
	//{{AFX_MSG_MAP(PageResults)
	ON_BN_CLICKED(IDC_PAGE_RESULTS_DETAILS_BUTTON, OnDetailsClicked)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// Button management
LRESULT PageResults::GetPageBack()
{
	return disable;
}

// Initialise the page when displayed
BOOL PageResults::OnSetActive() 
{
	// Set the title text
	staticTitle.SetWindowText(valueTitle);

	// Update the main results description
	CString results = valueResults;
	results.Replace(_T("\n"), _T("\r\n"));
	editResults.SetWindowText(results);

	// Show or hide the details button as appropriate
	buttonDetails.ShowWindow(valueDetails.IsEmpty() ? SW_HIDE : SW_SHOWNORMAL);

	// Update the controls
	UpdateData(false);

	// Start flashing the window caption
	flashTimer = SetTimer(1, 500, 0);
	
	// Perform any other initialisation required
	return DFUPage::OnSetActive();
}

// Delayed initialisation
void PageResults::OnSetActiveDelayed()
{
	// Perform any other initialisation required
	DFUPage::OnSetActiveDelayed();

	// Set the focus to either the Details or Finish button
	GotoDlgCtrl(valueDetails.IsEmpty()
	            ? GetSheet()->GetDlgItem(ID_WIZFINISH)
				: &buttonDetails);
}

// Tidy up when the page is hidden
BOOL PageResults::OnKillActive() 
{
	// Stop the animation
	// Stop flashing the window caption
	if (flashTimer) KillTimer(flashTimer);
	
	// Perform any other tidying up required
	return DFUPage::OnKillActive();
}

// Display details when button clicked
void PageResults::OnDetailsClicked() 
{
	// Format the details text across multiple lines
	CString details = valueDetails;
	details.Replace(_T(". "), _T(".\n"));
	int pos;
	while ((pos = details.Find(_T(": "))) != -1)
	{
		details.Delete(pos + 1);
		details.Insert(pos + 1, _T("\n\t"));
		pos = details.Find('\n', pos + 2);
		if ((pos != -1) && (details[pos + 1] != '\n'))
		{
			details.Insert(pos, '\n');
		}
	}

	// Display the details text in a message box
	AfxMessageBox(details, MB_OK | MB_ICONINFORMATION);
}

// Flash the window caption
void PageResults::OnTimer(UINT nIDEvent) 
{
	// Stop flashing the window if focus gained
	CWnd *parent = GetParent();
	if (parent == GetForegroundWindow())
	{
		// Restore the caption state
		parent->FlashWindow(false);

		// Stop the timer
		KillTimer(flashTimer);
		flashTimer = 0;
	}
	else
	{
		// Flash the window caption
		parent->FlashWindow(true);
	}

	// Pass on to the base class
	DFUPage::OnTimer(nIDEvent);
}
