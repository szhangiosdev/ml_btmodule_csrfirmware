#ifndef PAGEINTEO_H
#define PAGEINTRO_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "DFUPage.h"

// PageIntro class
class PageIntro : public DFUPage
{
	DECLARE_DYNCREATE(PageIntro)

public:

	// Constructor
	PageIntro();

	//{{AFX_DATA(PageIntro)
	enum { IDD = IDD_PAGE_INTRO };
	CString	valueVersion;
	CString	valueCopyright;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(PageIntro)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	// Delayed initialisation
	virtual void OnSetActiveDelayed();

	// Exit control
	virtual bool QueryClose(bool finish);
	
	//{{AFX_MSG(PageIntro)
	afx_msg LRESULT OnCallback(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

CString ExtractDfuFile(int resId);

#endif
