#ifndef DFUSHEET_H
#define DFUSHEET_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "PageIntro.h"
#include "PageUSBEnum.h"
#include "PageProgress.h"
#include "PageResults.h"

// DFUSheet class
class DFUSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(DFUSheet)

public:

	// The individual pages
	PageIntro pageIntro;
	PageUSBEnum pageUSBEnum;
	PageProgress pageProgress;
	PageResults pageResults;

	// Constructor
	DFUSheet();

	//{{AFX_VIRTUAL(DFUSheet)
	public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_VIRTUAL

protected:
	CBrush bgBrush;

	//{{AFX_MSG(DFUSheet)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif
