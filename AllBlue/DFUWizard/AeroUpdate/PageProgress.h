#ifndef PAGEPROGRESS_H
#define PAGEPROGRESS_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "dfu\DFUEngine.h"
#include "common/SmartPtr.h"

#include <afxmt.h>
#include <afxtempl.h>

// PageProgress class
class PageProgress : public DFUPage, public DFUEngine::Callback
{
	DECLARE_DYNCREATE(PageProgress)

public:

	// Constructor
	PageProgress();

	// Button management
	virtual LRESULT GetPageBack();
	virtual LRESULT GetPageNext();

	// DFUEngine callback functions
	virtual void Started(DFUEngine *engine, const DFUEngine::Status &status);
	virtual void Progress(DFUEngine *engine, const DFUEngine::Status &status);
	virtual void Complete(DFUEngine *engine, const DFUEngine::Status &status);

	// Callback support functions
	int ProgressTime(DFUEngine::State state);
	void CompleteSuccess(const DFUEngine::Status &status);
	void CompleteSuccessLast(const DFUEngine::Status &status);
	void CompleteFail(const DFUEngine::Status &status);

	//{{AFX_DATA(PageProgress)
	enum { IDD = IDD_PAGE_PROGRESS };
	CStatic	staticComment;
	CStatic	staticProductType;
	CButton	buttonAbort;
	CStatic	staticMain;
	CProgressCtrl progressMain;
	CStatic	staticSub;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(PageProgress)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	virtual BOOL OnQueryCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	// The DFU engine
	DFUEngine dfu;

	CString productName;
	bool isRescueMode;

	// Time taken for different actions
	CTime timeStart;
	CTime timeEnd;
	CTimeSpan timeTotal;

	// Possible callback types
	enum CallbackType
	{
		started,
		progress,
		complete
	};

	// Objects to hold data for callbacks
	struct CallbackData
	{
		// Constructor
		CallbackData(CallbackType type, DFUEngine *engine,
		     const DFUEngine::Status &status);

		// The data
		CallbackType type;
		DFUEngine *engine;
		DFUEngine::Status status;
	};

	// List of outstanding callbacks
	CList<SmartPtr<CallbackData>, const SmartPtr<CallbackData> &> listData;
	mutable CCriticalSection lockListData;

		// Generic callback request
	void CallbackRequest(CallbackType type, DFUEngine *engine,
	                     const DFUEngine::Status &status);

	// Delayed initialisation
	virtual void OnSetActiveDelayed();

	//{{AFX_MSG(PageProgress)
	afx_msg void OnAbortClicked();
	afx_msg LRESULT OnCallback(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}

#endif
