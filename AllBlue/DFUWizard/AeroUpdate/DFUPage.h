#ifndef DFUPAGE_H
#define DFUPAGE_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "DFUSheet.h"

// Declare the class to avoid recursive includes
class DFUSheet;

// DFUPage class
class DFUPage : public CPropertyPage
{
	DECLARE_DYNCREATE(DFUPage)

public:

	// Special page identifiers
	enum
	{
		automatic = 0,
		disable = -1,
		finish = -2,
		disablefinish = -3
	};

	// Constructors
	DFUPage();
	DFUPage(UINT id);

	// Obtain a pointer to the parent property sheet
	DFUSheet *GetSheet() const;

	// Button management
	virtual void SetButtons();
	virtual LRESULT GetPageBack();
	virtual LRESULT GetPageNext();

	// Page navigation
	void SetBack();
	void SetNext();

	// String matching
	static CString FindUnique(const CStringList &list, const CString &value,
	                          bool matchEmpty = true);

	//{{AFX_DATA(DFUPage)
	CStatic	staticTitle;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(DFUPage)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnQueryCancel();
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnWizardFinish();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	CBrush bgBrush;

	void Init();

	// Delayed initialisation
	virtual void OnSetActiveDelayed();

	// Exit control
	virtual bool QueryClose(bool finish);

	//{{AFX_MSG(DFUPage)
	afx_msg LRESULT OnSetActiveDelayedMsg(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif
