#include "stdafx.h"
#include "DFUWizard.h"
#include "PageUSBEnum.h"
#include "dfu\DFUEngine.h"

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(PageUSBEnum, DFUPage)

// Constructor
PageUSBEnum::PageUSBEnum() : DFUPage(PageUSBEnum::IDD)
{
	//{{AFX_DATA_INIT(PageUSBEnum)
	//}}AFX_DATA_INIT
}

// Destructor
PageUSBEnum::~PageUSBEnum()
{
}

// Data exchange
void PageUSBEnum::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PageUSBEnum)
	DDX_Control(pDX, IDC_PAGE_USBENUM_TITLE, staticTitle);
	//}}AFX_DATA_MAP
}

// Message map
BEGIN_MESSAGE_MAP(PageUSBEnum, DFUPage)
	//{{AFX_MSG_MAP(PageUSBEnum)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// Delayed initialisation
void PageUSBEnum::OnSetActiveDelayed()
{
	// Perform any other initialisation required
	DFUPage::OnSetActiveDelayed();

	// Show the hourglass
	CWaitCursor wait;

	// Enumerate the USB devices
	DFUSheet *sheet = GetSheet();
	CStringListX list;
	int devices = DFUEngine::GetUSBDeviceNames(list);
	Convert(list, listUSB);

	if (devices == 0)
	{
		AfxMessageBox(IDP_ERROR_USB_NONE, MB_OK | MB_ICONEXCLAMATION);
		sheet->SetActivePage(&sheet->pageIntro);
	}
	else if (devices == 1)
	{
		deviceName = listUSB.GetHead();
		sheet->SetActivePage(&sheet->pageProgress);
	}
	else
	{
		AfxMessageBox(IDP_ERROR_MULTIPLE_USB, MB_OK | MB_ICONEXCLAMATION);
		sheet->SetActivePage(&sheet->pageIntro);
	}
}

// Button management
LRESULT PageUSBEnum::GetPageBack()
{
	return disable;
}

LRESULT PageUSBEnum::GetPageNext()
{
	return disable;
}
