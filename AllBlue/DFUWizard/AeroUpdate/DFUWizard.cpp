
#include "stdafx.h"
#include "Shldisp.h"
#include "DFUWizard.h"
#include "versioninfo\VersionInfo.h"
#include "dfu\DFUEngine.h"

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

// Program exit codes
static const int rc_success = 0;
static const int rc_fail = 1;
static const int rc_cancel = 2;

// The application all sits inside this object
DFUWizardApp theApp;

// Message map
BEGIN_MESSAGE_MAP(DFUWizardApp, CWinApp)
	//{{AFX_MSG_MAP(DFUWizardApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

// Constructor
DFUWizardApp::DFUWizardApp()
{
#ifdef _DEBUG
	// Enable debugging of memory management
	int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF | _CRTDBG_LEAK_CHECK_DF;
	tmpFlag &= ~_CRTDBG_CHECK_CRT_DF;
	_CrtSetDbgFlag(tmpFlag);
	//_CrtSetBreakAlloc(alocnum);
#endif
#ifndef NO_HTML_HELP
	EnableHtmlHelp();
#endif
}

// Initialisation
BOOL DFUWizardApp::InitInstance()
{
	// Enable use of Active X controls
	AfxEnableControlContainer();

#if WINVER < 0x0500 
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
#endif

	// Initialise the DLL
	DFUEngine::InitDLL();

	// Use the registry for persistent settings
	SetRegistryKey(IDS_REGISTRY_COMPANY);

    // Change to look for the chm file in a "help" subdirectory
    CString sAppPath = m_pszHelpFilePath;
    int iPos = sAppPath.ReverseFind( '\\' );
    CString newHelpFile = sAppPath.Mid(0, iPos) + "\\help" + sAppPath.Right(sAppPath.GetLength()-iPos);
    free((void*)m_pszHelpFilePath);
    m_pszHelpFilePath=_tcsdup(newHelpFile);

	// Successfully initialised
	return true;
}


// The body of the application
int DFUWizardApp::Run() 
{
	// Obtain the application version number
#ifdef UNICODE
	VersionInfo<std::wstring> versionInfo;
#else
	VersionInfo<std::string> versionInfo;
#endif
	CString version = versionInfo.GetProductVersion().c_str();

	// Restore any previous settings
	DFUSheet wizard;

	// Set the version number and copyright messages
	CString textVersion = versionInfo.GetFileVersion().c_str();
	CString textBuild = versionInfo.GetSpecialBuild().c_str();
	CString textCopyright = versionInfo.GetLegalCopyright().c_str();
	if (textBuild.IsEmpty()) wizard.pageIntro.valueVersion.Format(IDS_VERSION_FORMAT, (LPCTSTR) textVersion);
	else wizard.pageIntro.valueVersion.Format(IDS_VERSION_BUILD_FORMAT, (LPCTSTR) textVersion, (LPCTSTR) textBuild);
	wizard.pageIntro.valueCopyright = textCopyright;

	// Enable simulated connections for demonstration builds only
	// wizard.simulate = true; // textBuild == _T("Demonstration");

	// Run the wizard
	int rc;
	if (wizard.DoModal() == ID_WIZFINISH)
	{
		// The wizard completed normally so save the settings
		rc = rc_success;
	}
	else
	{
		// The wizard was cancelled
		rc = rc_cancel;
	}

	// Value to be returned by WinMain
	return rc;
}
