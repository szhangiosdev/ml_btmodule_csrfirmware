#ifndef PAGERESULTS_H
#define PAGERESULTS_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "DFUPage.h"

// PageResults class
class PageResults : public DFUPage
{
	DECLARE_DYNCREATE(PageResults)

public:

	// The title, results and optional details
	CString valueTitle;
	CString valueResults;
	CString valueDetails;

	// Constructor
	PageResults();

	// Button management
	virtual LRESULT GetPageBack();

	//{{AFX_DATA(PageResults)
	enum { IDD = IDD_PAGE_RESULTS };
	CEdit editResults;
	CButton	buttonDetails;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(PageResults)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	// Timer to flash window caption
	UINT flashTimer;

	// Delayed initialisation
	virtual void OnSetActiveDelayed();

	//{{AFX_MSG(PageResults)
	afx_msg void OnDetailsClicked();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif
