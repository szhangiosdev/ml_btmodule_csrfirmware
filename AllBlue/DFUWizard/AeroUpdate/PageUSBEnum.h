#ifndef PAGEUSBENUM_H
#define PAGEUSBENUM_H

#if _MSC_VER > 1000
#pragma once
#endif

// PageUSBEnum class
class PageUSBEnum : public DFUPage
{
	DECLARE_DYNCREATE(PageUSBEnum)

public:

	// List of suitable USB devices
	CStringList listUSB;
	CString deviceName;

	// Constructor
	PageUSBEnum();

	// Destructor
	~PageUSBEnum();

	// Button management
	virtual LRESULT GetPageBack();
	virtual LRESULT GetPageNext();

	//{{AFX_DATA(PageUSBEnum)
	enum { IDD = IDD_PAGE_USBENUM };
	//}}AFX_DATA

	//{{AFX_VIRTUAL(PageUSBEnum)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	// Delayed initialisation
	virtual void OnSetActiveDelayed();

	//{{AFX_MSG(PageUSBEnum)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}

#endif
