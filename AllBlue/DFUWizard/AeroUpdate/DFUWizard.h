#ifndef DFUWIZARD_H
#define DFUWIZARD_H

#if _MSC_VER > 1000
#pragma once
#endif

// Include project header files
#include "resource.h"
#include "DFUSheet.h"

// DFUWizardApp class
class DFUWizardApp : public CWinApp
{
public:

	// Constructor
	DFUWizardApp();

	//{{AFX_VIRTUAL(DFUWizardApp)
	public:
	virtual BOOL InitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(DFUWizardApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}

#endif
