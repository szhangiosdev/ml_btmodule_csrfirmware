#include "stdafx.h"
#include "DFUWizard.h"
#include "PageProgress.h"
#include "SHLWapi.h"

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

// Relative timings for the different stages
static const int timeReconfigure = 8;
static const int timeDownload = 51;
static const int timeManifest = 6;

#define UM_CALLBACK (WM_USER + 2)

IMPLEMENT_DYNCREATE(PageProgress, DFUPage)

// Constructor
PageProgress::PageProgress() : DFUPage(PageProgress::IDD)
{
	//{{AFX_DATA_INIT(PageProgress)
	//}}AFX_DATA_INIT
}

// Data exchange
void PageProgress::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PageProgress)
	DDX_Control(pDX, IDC_PAGE_PROGRESS_COMMENT_STATIC, staticComment);
	DDX_Control(pDX, IDC_PAGE_PROGRESS_PRODUCT_TYPE, staticProductType);
	DDX_Control(pDX, IDC_PAGE_PROGRESS_ABORT_BUTTON, buttonAbort);
	DDX_Control(pDX, IDC_PAGE_PROGRESS_MAIN_PROGRESS, progressMain);
	DDX_Control(pDX, IDC_PAGE_PROGRESS_TITLE, staticTitle);
	//}}AFX_DATA_MAP
}

// Message map
BEGIN_MESSAGE_MAP(PageProgress, DFUPage)
	//{{AFX_MSG_MAP(PageProgress)
	ON_BN_CLICKED(IDC_PAGE_PROGRESS_ABORT_BUTTON, OnAbortClicked)
	ON_MESSAGE(UM_CALLBACK, OnCallback)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// Initialise the page when displayed
BOOL PageProgress::OnSetActive() 
{
	productName = _T("?");
	isRescueMode = false;
	staticProductType.SetWindowText(_T(""));

	// Reset the timers
	timeTotal = CTimeSpan(0, 0, 0, 0);

	// Configure the DFU engine
	dfu.CreateUSB(GetSheet()->pageUSBEnum.deviceName);

	dfu.SetCallback(this);
	
	// Perform any other initialisation required
	return DFUPage::OnSetActive();
}

// Tidy up when the page is hidden
BOOL PageProgress::OnKillActive() 
{
	// Display the hourglass
	CWaitCursor wait;

	// Terminate the DFU engine
	dfu.SetCallback();
	dfu.Destroy();

	// Perform any other tidying up required
	return DFUPage::OnKillActive();
}

// Delayed initialisation
void PageProgress::OnSetActiveDelayed()
{
	// Perform any other initialisation required
	DFUPage::OnSetActiveDelayed();

	// Start the procedure
	if (!dfu.StartReconfigure(false, false, _T("")))
	{
		// Display the results page
		GetSheet()->pageResults.valueTitle.Format(IDS_RESULTS_TITLE_FAIL);
		CString &results = GetSheet()->pageResults.valueResults;
		results.Format(IDS_RESULTS_MSG_FAIL_INTERNAL);
		CString recover;
		recover.Format(IDS_RESULTS_MSG_RECOVER_FAIL);
		if (!recover.IsEmpty()) results += "\n\n" + recover;
		GetSheet()->pageResults.valueDetails.Empty();
		GetSheet()->SetActivePage(&GetSheet()->pageResults);
	}
}

// Button management
LRESULT PageProgress::GetPageBack()
{
	return disable;
}

LRESULT PageProgress::GetPageNext()
{
	return disable;
}

// Handle clicks on the Abort button
void PageProgress::OnAbortClicked() 
{
	// Check if the user really wants to abort
	int result = AfxMessageBox(IDP_ABORT_PROGRESS_QUERY, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);

	// Ignore result if operation completed
	if (GetSheet()->GetActivePage() != this) return;

	// Abort the operation if requested
	if (result == IDYES)
	{
		// Disable the abort button
		buttonAbort.EnableWindow(false);

		// Set the title of the results page
		GetSheet()->pageResults.valueTitle.Format(IDS_RESULTS_TITLE_ABORT);

		// Add a description of the operations performed
		CString &results = GetSheet()->pageResults.valueResults;
		DFUEngine::Status status;
		dfu.GetStatus(status);
		switch (status.state)
		{
		case DFUEngine::reconfigure:
			results.Format(IDS_RESULTS_MSG_FAIL_RECONFIGURE);
			break;

		case DFUEngine::upload:
			results.Format(IDS_RESULTS_MSG_FAIL_UPLOAD);
			break;

		case DFUEngine::download:
			results.Format(IDS_RESULTS_MSG_FAIL_DOWNLOAD);
			break;

		case DFUEngine::manifest:
			results.Format(IDS_RESULTS_MSG_FAIL_MANIFEST);
			break;

		default:
			results.Format(IDS_RESULTS_MSG_FAIL_UNKNOWN);
			break;
		}

		// Add the recovery information
		CString recover;
		recover.Format(IDS_RESULTS_MSG_RECOVER_ABORT);
		if (!recover.IsEmpty()) results += "\n\n" + recover;

		// No details available
		GetSheet()->pageResults.valueDetails.Format(IDS_RESULTS_MSG_ABORT);

		// Display the results page
		GetSheet()->SetActivePage(&GetSheet()->pageResults);
	}
}

// Callback functions
void PageProgress::Started(DFUEngine *engine, const DFUEngine::Status &status)
{
	CallbackRequest(started, engine, status);
}

void PageProgress::Progress(DFUEngine *engine, const DFUEngine::Status &status)
{
	// Suppress progress callbacks if other callbacks pending
	lockListData.Lock();
	bool empty = listData.IsEmpty() != 0;
	lockListData.Unlock();
	if (empty) CallbackRequest(progress, engine, status);
}

void PageProgress::Complete(DFUEngine *engine, const DFUEngine::Status &status)
{
	CallbackRequest(complete, engine, status);
}

PageProgress::CallbackData::CallbackData(CallbackType type, DFUEngine *engine,
                          const DFUEngine::Status &status)
{
	this->type = type;
	this->engine = engine;
	this->status = status;
}

// Callback implementation
void PageProgress::CallbackRequest(CallbackType type,
                                   DFUEngine *engine,
                                   const DFUEngine::Status &status)
{
	// Construct an object with the callback details
	lockListData.Lock();
	{
		SmartPtr<CallbackData> data(new CallbackData(type, engine, status));
		listData.AddTail(data);
	}
	lockListData.Unlock();

	// Request a callback
	PostMessage(UM_CALLBACK, 0, 0);
}

int PageProgress::ProgressTime(DFUEngine::State state)
{
	int time = 0;
	switch (state)
	{
	case DFUEngine::manifest:
		time += timeManifest;

	case DFUEngine::download:
		time += timeDownload;

	case DFUEngine::upload:

	case DFUEngine::reconfigure:
		time += timeReconfigure;
		break;
	}
	return time;
}


static int ResIdFromDeviceId(unsigned id)
{
	switch (id)
	{
	case 0x0258: // no product ID, ADK 3.0
	case 0x0547: // no product ID, ADK 3.5
	case 0x1001:
		return IDR_AEROSKULL_XS_1_DFU;
	case 0x1002:
		return IDR_AEROTWIST_DFU;
	case 0x1003:
		return IDR_AEROSKULL_HD_DFU;
	case 0x1004:
		return IDR_AEROBULL_DFU;
	case 0x1006:
		return IDR_AEROSKULL_XS_2_DFU;
	case 0x1fff: // when a DFU upgrade is interrupted, next time the product code returned will be 0x1fff
		return IDR_RESCUE_DFU;
	default:
		return 0;
	}
}


static CString ProductIdFromResId(int id)
{
	switch (id)
	{
	case IDR_AEROSKULL_XS_1_DFU:
		return _T("AeroSkull XS (I)");
	case IDR_AEROTWIST_DFU:
		return _T("AeroTwist");
	case IDR_AEROSKULL_HD_DFU:
		return _T("Aeroskull HD");
	case IDR_AEROBULL_DFU:
		return _T("Aerobull");
	case IDR_AEROSKULL_XS_2_DFU:
		return _T("AeroSkull XS (II)");
	case IDR_RESCUE_DFU:
		return _T("Rescue Mode Image");
	default:
		return _T("Unknown"); // shouldn't happen
	}
}


void PageProgress::CompleteSuccess(const DFUEngine::Status &status)
{
	bool ok = true;

	// Start the next operation
	CTimeSpan timeSpan = timeEnd - timeStart;
	switch (status.state)
	{
	case DFUEngine::reconfigure:
		{
			unsigned id = dfu.GetBcdDevice() & 0x1fff; // strip off the feature flags and leave only the prodID
			isRescueMode = id == 0x1fff;
			int resId = ResIdFromDeviceId(id);
			ok = resId != 0;
			if (ok)
			{
				CString fileName = ExtractDfuFile(resId);
				productName = ProductIdFromResId(resId);
				staticProductType.SetWindowText(CString(_T("Device: ")) + productName);
				ok = dfu.StartDownload(fileName);
			}
			else
				GetSheet()->pageResults.valueDetails.Format(IDS_RESULT_FAIL_UNKNOWN_DEVICE, id);
		}
		break;

	case DFUEngine::upload:
		// ok = dfu.StartDownload(fileDownload);
		break;

	case DFUEngine::download:
		ok = dfu.StartManifest();
		break;

	default:
		// Otherwise successfully completed
		CompleteSuccessLast(status);
		break;
	}

	// Handle failure to start the next operation
	if (!ok)
	{
		// Display the results page
		GetSheet()->pageResults.valueTitle.Format(IDS_RESULTS_TITLE_FAIL);
		CString &results = GetSheet()->pageResults.valueResults;
		results.Format(IDS_RESULTS_MSG_FAIL_INTERNAL);
		CString recover;
		recover.Format(IDS_RESULTS_MSG_RECOVER_FAIL);
		if (!recover.IsEmpty()) results += "\n\n" + recover;
		// GetSheet()->pageResults.valueDetails.Empty();
		GetSheet()->SetActivePage(&GetSheet()->pageResults);
	}
}

void PageProgress::CompleteSuccessLast(const DFUEngine::Status &status)
{
	// Display the results page
	GetSheet()->pageResults.valueTitle.Format(IDS_RESULTS_TITLE_SUCCESS);
	CString &results = GetSheet()->pageResults.valueResults;
	results.Format(IDS_RESULTS_MSG_SUCCESS_DOWNLOAD);
	GetSheet()->pageResults.valueDetails.Empty();
	if (isRescueMode)
		GetSheet()->SetActivePage(&GetSheet()->pageIntro);
	else
		GetSheet()->SetActivePage(&GetSheet()->pageResults);
}

void PageProgress::CompleteFail(const DFUEngine::Status &status)
{
	// Construct a description of the problem
	GetSheet()->pageResults.valueTitle.Format(IDS_RESULTS_TITLE_FAIL);

	// Add a description of the operations performed
	CString &results = GetSheet()->pageResults.valueResults;
	switch (status.state)
	{
	case DFUEngine::reconfigure:
		results.Format(IDS_RESULTS_MSG_FAIL_RECONFIGURE);
		break;

	case DFUEngine::upload:
		results.Format(IDS_RESULTS_MSG_FAIL_UPLOAD);
		break;

	case DFUEngine::download:
		results.Format(IDS_RESULTS_MSG_FAIL_DOWNLOAD);
		break;

	case DFUEngine::manifest:
		results.Format(IDS_RESULTS_MSG_FAIL_MANIFEST);
		break;

	default:
		results.Format(IDS_RESULTS_MSG_FAIL_UNKNOWN);
		break;
	}

	// Add the recovery information
	CString recover;
	recover.Format(IDS_RESULTS_MSG_RECOVER_FAIL);
	if (!recover.IsEmpty()) results += "\n\n" + recover;

	// Set the details
	GetSheet()->pageResults.valueDetails = status.result();

	// Display the results page
	GetSheet()->SetActivePage(&GetSheet()->pageResults);
}

// on callback message.
LRESULT PageProgress::OnCallback(WPARAM wParam, LPARAM lParam)
{
	// Process the next callback
	lockListData.Lock();
	SmartPtr<CallbackData> data;
	if (!listData.IsEmpty()) data = listData.RemoveHead();
	lockListData.Unlock();
	if (!data) return 0;

	// Call the callback function if valid
	switch (data->type)
	{
	case started:
		{
			// Record the start time
			timeStart = CTime::GetCurrentTime();

			// Set the progress bars
			Progress(data->engine, data->status);

			// Display a description of the operation
			CString description;
			switch (data->status.state)
			{
			case DFUEngine::reconfigure:
				description.Format(IDS_PROGRESS_OPERATION_RECONFIGURE);
				break;

			case DFUEngine::upload:
				description.Format(IDS_PROGRESS_OPERATION_UPLOAD);
				break;

			case DFUEngine::download:
				description.Format(IDS_PROGRESS_OPERATION_DOWNLOAD, productName);
				break;

			case DFUEngine::manifest:
				description.Format(IDS_PROGRESS_OPERATION_MANIFEST);
				break;
			}
			PathSetDlgItemPath(m_hWnd, IDC_PAGE_PROGRESS_SUB_STATIC, description);
		}
		break;
	case progress:
		{
			// Set the progress bar for the whole procedure
			int start = ProgressTime((DFUEngine::State) (data->status.state - 1));
			int end = ProgressTime(data->status.state);
			int total = ProgressTime(DFUEngine::manifest);
			progressMain.SetPos((start * 100 + (end - start) * data->status.percent) / total);

			// Set the comment
			staticComment.SetWindowText(data->status.activity());
		}
		break;
	case complete:
		{
			// Record the end time
			timeEnd = CTime::GetCurrentTime();
			timeTotal += timeEnd - timeStart;

			// Special case if failed to upload due to corrupt firmware
			bool ignoreResult = false;
			if ((data->status.state == DFUEngine::upload)
				&& ((data->status.result.GetCode() == DFUEngine::fail_dfu_firmware)
					|| (data->status.result.GetCode() == DFUEngine::fail_no_upload)))
			{
				// Check if the user wants to skip the upload and download anyway
				int result = AfxMessageBox(data->status.result.GetCode() == DFUEngine::fail_dfu_firmware
										   ? IDP_SKIP_UPLOAD_CORRUPT_QUERY
										   : IDP_SKIP_UPLOAD_UNSUPPORTED_QUERY,
										   MB_YESNO | MB_ICONQUESTION);
				
				// Continue with the download if required
				if (result == IDYES)
				{
					ignoreResult = true;
				}
			}

			// Set the progress bars
			if (!ignoreResult) Progress(data->engine, data->status);

			// Action depends on whether the operation was successful
			if (data->status.result || ignoreResult) CompleteSuccess(data->status);
			else CompleteFail(data->status);
		}
		break;
	}

	return 0;
}

// Exit control
BOOL PageProgress::OnQueryCancel() 
{
	// Check if the user really wants to quit
	int result = AfxMessageBox(IDP_EXIT_PROGRESS_QUERY, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);

	// Ignore result if operation completed
	if (GetSheet()->GetActivePage() != this) return false;

	// Cancel the close if requested
	if (result == IDNO) return false;

	// Disable the abort button
	buttonAbort.EnableWindow(false);

	// Terminate the DFU engine
	dfu.SetCallback();
	dfu.Destroy();

	// Display a final prompt to the user
	AfxMessageBox(IDP_EXIT_PROGRESS_PROMPT);
	
	// Perform any default behaviour (skipping the DFUPage query)
	return CPropertyPage::OnQueryCancel();
}
