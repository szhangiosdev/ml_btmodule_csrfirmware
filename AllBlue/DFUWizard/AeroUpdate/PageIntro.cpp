
#include "stdafx.h"
#include "DFUWizard.h"
#include "PageIntro.h"

#include <Setupapi.h>

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

#define HW_ID "USB\\VID_0A12&PID_0001"
#define DFU_FILE_NAME "Aero.dfu"
#define USB_DRIVER_ZIP_FILE_NAME "USBDriver.zip"
// The below should be the same as the main directory within the zip file
#define USB_DRIVER_FOLDER_NAME "USBDriver"
#define INF_FILE_NAME "CSRBlueCoreUSB.inf"


#define UM_CALLBACK (WM_USER + 3)


static void Fail(const TCHAR* msg)
{
	AfxMessageBox(msg);
	exit(1);
}


static CString GetTempDir()
{
	CString result;
	{
		TCHAR buffer[MAX_PATH];
		if (SUCCEEDED(SHGetFolderPath(0, CSIDL_APPDATA | CSIDL_FLAG_CREATE, 0, 0, buffer)))
		{
			// Use the application data directory within the user's profile
			CString company;
			company.Format(IDS_REGISTRY_COMPANY);
			result.Format(_T("%s\\%s"), buffer, (LPCTSTR) company);
			CreateDirectory(result, 0);
			CString application;
			application.Format(AFX_IDS_APP_TITLE);
			result += _T("\\") + application;
			CreateDirectory(result, 0);
		}
		else
			Fail(_T("Couldn't create the temporary directory. Exiting the application."));
	}
	return result;
}


static CString ExtractResourceToFile(int resId, const TCHAR* fileName)
{
	CString result = GetTempDir() + _T("\\") + fileName;
	// Write the RCDATA resource containing the DFU file into the external temp file
	{
		CFile file;
		if (file.Open(result, CFile::modeCreate|CFile:: modeReadWrite))
		{
			HRSRC rc = ::FindResource(NULL, MAKEINTRESOURCE(resId), RT_RCDATA);
			size_t size = ::SizeofResource(NULL, rc);
			HGLOBAL rcData = ::LoadResource(NULL, rc);
			void* data = ::LockResource(rcData);
			if (data && size)
				file.Write(data, size);
			file.Close();
		}
		else
			Fail(_T("Couldn't create the temporary file. Exiting the application."));
	}
	return result;
}


CString ExtractDfuFile(int resId)
{
	return ExtractResourceToFile(resId, _T(DFU_FILE_NAME));
}


static void MakeWritable(CONST CString& filename)
{
	DWORD dwAttrs = ::GetFileAttributes(filename);
	if (dwAttrs == INVALID_FILE_ATTRIBUTES)
		return;
	if (dwAttrs & FILE_ATTRIBUTE_READONLY)
		::SetFileAttributes(filename, dwAttrs & (~FILE_ATTRIBUTE_READONLY));
}


static bool DeleteDirectory(const CString& dir)
{
	CFileFind ff;
	if (!ff.FindFile(dir))
		return true;

	CString file;
	BOOL more = ff.FindFile(dir + _T("\\*.*"));

	while (more)
	{
		more = ff.FindNextFile();
		if (ff.IsDirectory())
		{
			if (!ff.IsDots())
				if (!DeleteDirectory(ff.GetFilePath()))
					return false;
		}
		else
		{
			file = ff.GetFilePath();
			MakeWritable(file);
			if (!DeleteFile(file))
				return false;
		}
	}

	// RemoveDirectory fails without this one!  CFileFind locks file system resources.
	ff.Close();

	if(!RemoveDirectory(dir))
		return false;

	return true;
}


static bool UnzipToFolder(BSTR lpZipFile, BSTR lpFolder)
{
	IShellDispatch *pISD;

	Folder  *pZippedFile = NULL;
	Folder  *pDestination = NULL;

	long FilesCount = 0;
	IDispatch* pItem = NULL;
	FolderItems *pFilesInside = NULL;

	VARIANT Options, OutFolder, InZipFile, Item;
	CoInitialize( NULL);
	__try
	{
		if (CoCreateInstance(CLSID_Shell, NULL, CLSCTX_INPROC_SERVER, IID_IShellDispatch, (void **)&pISD) != S_OK)
			return false;

		InZipFile.vt = VT_BSTR;
		InZipFile.bstrVal = lpZipFile;
		pISD->NameSpace(InZipFile, &pZippedFile);
		if (!pZippedFile)
			return false;

		OutFolder.vt = VT_BSTR;
		OutFolder.bstrVal = lpFolder;
		pISD->NameSpace(OutFolder, &pDestination);
		if (!pDestination)
			return false;

		pZippedFile->Items(&pFilesInside);
		if (!pFilesInside)
			return false;

		pFilesInside->get_Count(&FilesCount);
		if (FilesCount < 1)
			return false;
		pFilesInside->QueryInterface(IID_IDispatch,(void**)&pItem);

		Item.vt = VT_DISPATCH;
		Item.pdispVal = pItem;
		Options.vt = VT_I4;
		Options.lVal = 1024 | 512 | 16 | 4; // http://msdn.microsoft.com/en-us/library/bb787866(VS.85).aspx
		bool retval = pDestination->CopyHere(Item, Options) == S_OK;

		return retval;

	}__finally    
	{
		if (pItem) pItem->Release();
		if (pFilesInside) pFilesInside->Release();
		if (pDestination) pDestination->Release();
		if (pZippedFile) pZippedFile->Release();
		if (pISD) pISD->Release();

		CoUninitialize();
	}
}


static void UnpackDrivers()
{
	CString tempDir = GetTempDir();
	CString zipFileName = ExtractResourceToFile(IDR_USB_DRIVER, _T(USB_DRIVER_ZIP_FILE_NAME));
	CString dir = tempDir + _T("\\") + USB_DRIVER_FOLDER_NAME;
	if (!DeleteDirectory(dir))
		Fail(_T("Failed to delete temporary directory"));
	if (!UnzipToFolder(CComBSTR(zipFileName), CComBSTR(tempDir)))
		Fail(_T("Couldn't unpack the drivers. Exiting the application."));
}


static BOOL IsWow64()
{
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
	LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress(GetModuleHandle(TEXT("kernel32")),"IsWow64Process");
	if (!fnIsWow64Process)
		return false;
    BOOL bIsWow64 = FALSE;
    if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
        Fail(_T("IsWow64Process() failed whereas it shouldn't. This is Windows, what can we do?"));
    return bIsWow64;
}


static void InstallDriver()
{
	typedef BOOL (WINAPI *UpdateDriverForPlugAndPlayDevicesProto)(HWND hwndParent,
		LPCTSTR HardwareId, LPCTSTR FullInfPath, DWORD InstallFlags, PBOOL bRebootRequired);
#define INSTALLFLAG_FORCE 0x1
#define INSTALLFLAG_READONLY 0x2
#define INSTALLFLAG_NONINTERACTIVE 0x4
#ifdef _UNICODE
#  define UPDATEDRIVERFORPLUGANDPLAYDEVICES "UpdateDriverForPlugAndPlayDevicesW"
#else
#  define UPDATEDRIVERFORPLUGANDPLAYDEVICES "UpdateDriverForPlugAndPlayDevicesA"
#endif

	CString dir = GetTempDir() + _T("\\") + USB_DRIVER_FOLDER_NAME + _T("\\") +
		(IsWow64() ? _T("win64") : _T("win32"));
	CString inf = dir + _T("\\") + _T(INF_FILE_NAME);

	BOOL result = SetupCopyOEMInf(inf, NULL, SPOST_PATH, SP_COPY_NEWER, NULL, 0, NULL, NULL);
	if (!result)
	{
		DWORD e = GetLastError(); 
		LPTSTR buf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, e, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&buf, 0, NULL);
		Fail(buf);
		LocalFree(buf); // never reached
		return;
	}

    UpdateDriverForPlugAndPlayDevicesProto UpdateFn;
    HMODULE newdevMod = LoadLibrary(TEXT("newdev.dll"));
    if (!newdevMod)
	{
		Fail(_T("Could not activate the driver (newdev.dll not found)"));
		return;
	}
    UpdateFn = (UpdateDriverForPlugAndPlayDevicesProto)GetProcAddress(newdevMod, UPDATEDRIVERFORPLUGANDPLAYDEVICES);
    if (!UpdateFn)
    {
		Fail(_T("Could not activate the driver (function UpdateDriverForPlugAndPlayDevices() not found in newdev.dll)"));
		return;
    }
	BOOL bRebootReqTemp = FALSE;
	if (!UpdateFn(NULL, _T(HW_ID), inf, INSTALLFLAG_FORCE, &bRebootReqTemp))
	{
		DWORD err = GetLastError();
		if (err == ERROR_NO_SUCH_DEVINST)
			; // Fail(_T("Device not connected, exiting."));
		else if (err == ERROR_IN_WOW64)
			; // TODO: do 64-bit installation
		else
			Fail(_T("Driver installation failed, exiting."));
	}
}


IMPLEMENT_DYNCREATE(PageIntro, DFUPage)

// Constructor
PageIntro::PageIntro() : DFUPage(PageIntro::IDD)
{
	//{{AFX_DATA_INIT(PageIntro)
	valueVersion = _T("");
	valueCopyright = _T("");
	//}}AFX_DATA_INIT
}

// Data exchange
void PageIntro::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PageIntro)
	DDX_Control(pDX, IDC_PAGE_INTRO_TITLE, staticTitle);
	DDX_Text(pDX, IDC_PAGE_INTRO_VERSION_STATIC, valueVersion);
	DDX_Text(pDX, IDC_PAGE_INTRO_COPYRIGHT_STATIC, valueCopyright);
	//}}AFX_DATA_MAP
}

// Message map
BEGIN_MESSAGE_MAP(PageIntro, DFUPage)
	//{{AFX_MSG_MAP(PageIntro)
	ON_MESSAGE(UM_CALLBACK, OnCallback)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static BOOL driversInstalled = false; // no need to reinstall when the user returns to the Intro page again


LRESULT PageIntro::OnCallback(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case 0:
		// The DFU file is extracted in PageProgress.cpp when the product ID is known
		UnpackDrivers();
		PostMessage(UM_CALLBACK, wParam + 1);
		break;

	case 1:
		InstallDriver();
		PostMessage(UM_CALLBACK, wParam + 1);
		break;

	case 2:
		driversInstalled = true;
		GetSheet()->SetWizardButtons(PSWIZB_NEXT);
		break;
	}

	return 0;
}


// Delayed initialisation
void PageIntro::OnSetActiveDelayed()
{
	// Perform any other initialisation required
	DFUPage::OnSetActiveDelayed();

	if (!driversInstalled)
	{
		GetSheet()->SetWizardButtons(0);
		PostMessage(UM_CALLBACK, 0);
	}
}

// Exit control
bool PageIntro::QueryClose(bool finish)
{
	return true;
}
