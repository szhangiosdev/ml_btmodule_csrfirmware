#include "stdafx.h"
#include "DFUWizard.h"
#include "DFUSheet.h"

// Visual C++ debugging code
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(DFUSheet, CPropertySheet)

// Constructor
DFUSheet::DFUSheet()
{
	m_psh.dwFlags &= ~PSH_HASHELP;

	// Add all of the pages to the property sheet
	AddPage(&pageIntro);
	AddPage(&pageUSBEnum);
	AddPage(&pageProgress);
	AddPage(&pageResults);

	// Change the property sheet to a wizard
	SetWizardMode();
} 

// Message map
BEGIN_MESSAGE_MAP(DFUSheet, CPropertySheet)
	//{{AFX_MSG_MAP(DFUSheet)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL DFUSheet::OnInitDialog() 
{
	bgBrush.CreateSolidBrush(RGB(255, 255, 255));
	return CPropertySheet::OnInitDialog();
}

HBRUSH DFUSheet::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	return bgBrush;
}

