DFU build steps and things to becareful:


1. Make sure all the 3 decoders are built with the correct settings: compile mode change to TWS (or TWS_wired for SBC).
The 3 bin built out will be copied here:
C:\ADK3.5\AllBlue\Firmware\image\aac_decoder
C:\ADK3.5\AllBlue\Firmware\image\aptx_decoder
C:\ADK3.5\AllBlue\Firmware\image\sbc_decoder

2. Jingles are 2 groups: -20dB for HD and -12dB for XS.
2.1 replace
C:\ADK3.5\AllBlue\Firmware\image\headers
C:\ADK3.5\AllBlue\Firmware\image\prompts
C:\ADK3.5\AllBlue\Firmware\image\refname
by -20dB, then run build_all_HD.bat to build

2.1 replace
C:\ADK3.5\AllBlue\Firmware\image\headers
C:\ADK3.5\AllBlue\Firmware\image\prompts
C:\ADK3.5\AllBlue\Firmware\image\refname
by -12dB, then run build_all_XS.bat to build