@call setenv.bat

@set product=%1

@if [%product%]==[] goto endprog

@set public_key=%adkpath%\AllBlue\DFU\keys.public.key
@set private_key=%adkpath%\AllBlue\DFU\keys.private.key
@set fw_psr=%adkpath%\AllBlue\%product%_stack.psr
@set vm_psr=%adkpath%\AllBlue\%product%_app.psr
@set src_dir=%adkpath%\AllBlue\Firmware

:: Build the code
@cd %src_dir%
@%adkpath%\xide\bin\xipbuild.exe -n %product% allblue.xip
@if %errorlevel% neq 0 goto endprog
@%adkpath%\tools\bin\make image.fs BLUELAB=%adkpath%\tools -f allblue.%product%.mak
@if %errorlevel% neq 0 goto endprog
@%adkpath%\tools\bin\copyfile allblue.app image\vm.app

@cd %adkpath%\AllBlue
@if not exist build mkdir build
@cd build
@if not exist %product% mkdir %product%
@cd %product%
@if %errorlevel% neq 0 goto endprog

:: Remove previous products
@del loader_signed* 2>nul
@del stack_signed* 2>nul
@del image.fs 2>nul
@del app_signed* 2>nul
@del image_signed* 2>nul
@del merge_signed* 2>nul
@del vm_signed* 2>nul
@del fw_signed* 2>nul
@del %product%.dfu 2>nul

:: Sign the stack image
@%dfutoolspath%\dfukeyinsert -o loader_signed -l %adkpath%\firmware\assisted\unified\gordon\loader_unsigned.xdv -ks %public_key%
@%dfutoolspath%\dfusign -o stack_signed -s %adkpath%\firmware\assisted\unified\gordon\stack_unsigned.xpv -ks %private_key%

:: Create the (unsigned) FS image
@%adkpath%\tools\bin\packfile %src_dir%\image image.fs

:: Build the signed app FS image
@%dfutoolspath%\dfusign -o image_signed -h image.fs -ka %private_key%

:: Build merge.xpv/xdv (stack + app)
@%adkpath%\tools\bin\vmbuilder -size 16064 merge_signed.xpv stack_signed.xpv image_signed.fs

:: Sign PSKEYs to be included in the DFU image
@%dfutoolspath%\dfusign -o vm_signed -pa %vm_psr% -ka %private_key%
@%dfutoolspath%\dfusign -o fw_signed -ps %fw_psr% -ks %private_key%

:: Generate the DFU file
@%dfutoolspath%\dfubuild -pedantic -f %product%.dfu -uv 0x0a12 -up 0x0001 -ui "AllBlue" ^
	-s stack_signed.xpv -d stack_signed.xdv ^
	-h image_signed.fs -p3 . fw_signed.stack.psr vm_signed.app.psr

@if errorlevel 1 (
	echo.
	echo *** ERROR: DFU file not built
	echo.
	del %product%.dfu 2>nul
	goto endprog
)

@del /Q %adkpath%\AllBlue\DFUWizard\build\x86win32\bin\release
@del /Q %adkpath%\AllBlue\DFUWizard\build\x86win32\intermediate\AeroUpdate\release

:endprog
@cd %adkpath%\AllBlue\DFU
@exit /B
